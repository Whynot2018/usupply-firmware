#pragma once

#include "Meta.hpp"
#include "Pin.hpp"
#include "RCC.hpp"
#include "Types.hpp"
#include "Math.hpp"
#include "Register.hpp"
#include "RegistersDebug.hpp"

#include <cstdint>

namespace Peripherals::TimerGeneral
{
	enum class Request : std::uint32_t
	{
		Trigger,
		CaptureCompare1,
		CaptureCompare2, 
		CaptureCompare3, 
		CaptureCompare4, 
		Update
	};
	
	template <std::uint32_t Channel>
	constexpr bool IsTimerAPB1 =  DebugGeneral::IsTimerAPB2<Channel>;
	
	template <std::uint32_t Channel>
	constexpr bool IsTimerAPB2 =  DebugGeneral::IsTimerAPB2<Channel>;

	template <std::uint32_t Channel>
	constexpr bool IsBasicTimer = General::IsOneOf(Channel, 6u, 7u);
	
	template <std::uint32_t Channel>
	constexpr bool IsGeneralPurposeTimer = General::IsOneOf(Channel, 2u, 3u, 14u, 15u, 16u, 17u);
	
	template <std::uint32_t Channel>
	constexpr bool IsAdvancedControlTimer = General::IsOneOf(Channel, 1u);
	
	template <std::uint32_t Channel>
	constexpr bool IsTimerChannel = DebugGeneral::IsTimerChannel<Channel>;
			
	struct Prescales
	{
		std::uint32_t const PSC;
		std::uint32_t const ARR;
	};
	
	static constexpr auto CalculatePrescale(std::uint32_t pTimerClock, std::uint32_t pFrequency)
	{
		std::uint32_t 	prescaling 		= 0x1u;
		std::uint32_t 	auto_reload 	= 0xffffu;
	
		while (true)
		{
			auto const timer_clock = ( pTimerClock + prescaling / 2 ) / prescaling;
			auto const division = ( timer_clock + pFrequency / 2 ) / pFrequency;
			if (division <= 0xffff)
			{
				auto_reload = division;
				break;
			}
			++prescaling;
		}
		return Prescales{ prescaling - 1, auto_reload - 1 };
	}

	template <std::uint32_t Channel>
	constexpr Peripheral GetPeripheral() 
	{
		switch (Channel)
		{
		case 1: return Peripheral::ClockTIM1;
		case 2: return Peripheral::ClockTIM2;
		case 3: return Peripheral::ClockTIM3;
		case 6: return Peripheral::ClockTIM6;
		case 7: return Peripheral::ClockTIM7;
		case 14: return Peripheral::ClockTIM14;
		case 15: return Peripheral::ClockTIM15;
		case 16: return Peripheral::ClockTIM16;
		case 17: return Peripheral::ClockTIM17;
		};
	}
	using System::InterruptSource;

	template <unsigned Channel>
	InterruptSource constexpr TimerChannelInterrupt()
	{
#ifdef STM32F070x6
		switch (Channel)
		{
		case 1:		return InterruptSource::eTIM1_CC;
		case 3:		return InterruptSource::eTIM3;
		case 14:	return InterruptSource::eTIM14;
		case 16: 	return InterruptSource::eTIM16;
		case 17: 	return InterruptSource::eTIM17;
		};
#endif
#ifdef STM32F072
		switch (Channel)
		{
		case 1:		return InterruptSource::eTIM1_CC;
		case 2:		return InterruptSource::eTIM2;
		case 3:		return InterruptSource::eTIM3;
		case 6:		return InterruptSource::eTIM6_DAC;
		case 7:		return InterruptSource::eTIM7;
		case 14:	return InterruptSource::eTIM14;
		case 15: 	return InterruptSource::eTIM15;
		case 16: 	return InterruptSource::eTIM16;
		case 17: 	return InterruptSource::eTIM17;
		};
#endif
	}
	
	
	template<unsigned Channel> bool constexpr CC4OFExists(){	return General::IsOneOf(Channel, 1, 2, 3);				}
	template<unsigned Channel> bool constexpr CC3OFExists(){	return General::IsOneOf(Channel, 1, 2, 3);				}
	template<unsigned Channel> bool constexpr CC2OFExists(){	return General::IsOneOf(Channel, 1, 2, 3);				}
	template<unsigned Channel> bool constexpr CC1OFExists(){	return General::IsOneOf(Channel, 1, 2, 3, 14, 16, 17);	}
	template<unsigned Channel> bool constexpr BIFExists  (){	return General::IsOneOf(Channel, 1, 16, 17);			}
	template<unsigned Channel> bool constexpr TIFExists  (){	return General::IsOneOf(Channel, 1, 2, 3);				}
	template<unsigned Channel> bool constexpr COMIFExists(){	return General::IsOneOf(Channel, 1, 16, 17);			}
	template<unsigned Channel> bool constexpr CC4IFExists(){	return General::IsOneOf(Channel, 1, 2, 3);				}
	template<unsigned Channel> bool constexpr CC3IFExists(){	return General::IsOneOf(Channel, 1, 2, 3);				}
	template<unsigned Channel> bool constexpr CC2IFExists(){	return General::IsOneOf(Channel, 1, 2, 3);				}
	template<unsigned Channel> bool constexpr CC1IFExists(){	return General::IsOneOf(Channel, 1, 2, 3, 14, 16, 17);	}
	template<unsigned Channel> bool constexpr UIFExists  (){	return true;											}
	
	template <unsigned Channel>
	constexpr auto * TimerRegister()
	{
		if constexpr (Channel == 1u)	return TIM1;
		if constexpr (Channel == 2u)	return TIM2;
		if constexpr (Channel == 3u)	return TIM3;
		if constexpr (Channel == 6u)	return TIM6;
		if constexpr (Channel == 7u)	return TIM7;
		if constexpr (Channel == 14u)	return TIM14;
		if constexpr (Channel == 15u)	return TIM15;
		if constexpr (Channel == 16u)	return TIM16;
		if constexpr (Channel == 17u)	return TIM17;
	}
	
	constexpr auto TimerRegisterBase(unsigned Channel)
	{
		switch(Channel)
		{
			case (1u)	 :return TIM1_BASE;
			case (2u)	 :return TIM2_BASE;
			case (3u)	 :return TIM3_BASE;
			case (6u)	 :return TIM6_BASE;
			case (7u)	 :return TIM7_BASE;
			case (14u)	 :return TIM14_BASE;
			case (15u)	 :return TIM15_BASE;
			case (16u)	 :return TIM16_BASE;
			case (17u)	 :return TIM17_BASE;
		};
	}
	
	template <unsigned Channel>
	constexpr auto InterruptSource()
	{
#ifdef STM32F070x6
		switch (Channel)
		{
		case 1:		return InterruptSource::eTIM1_CC;
		case 3:		return InterruptSource::eTIM3;
		case 14:	return InterruptSource::eTIM14;
		case 16: 	return InterruptSource::eTIM16;
		case 17: 	return InterruptSource::eTIM17;
		};
#endif
#ifdef STM32F072
		switch (Channel)
		{
		case 1:		return InterruptSource::eTIM1_CC;
		case 2:		return InterruptSource::eTIM2;
		case 3:		return InterruptSource::eTIM3;
		case 6:		return InterruptSource::eTIM6_DAC;
		case 7:		return InterruptSource::eTIM7;
		case 14:	return InterruptSource::eTIM14;
		case 15: 	return InterruptSource::eTIM15;
		case 16: 	return InterruptSource::eTIM16;
		case 17: 	return InterruptSource::eTIM17;
		};
#endif
	}

	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, CR1)>
	struct CR1 : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		//Counter enable register
		auto CEN () { return base_t::template Actual<TIM_CR1_CEN >(); }
		
		//Update registers
		auto UDIS() { return base_t::template Actual<TIM_CR1_UDIS>(); }
		auto URS () { return base_t::template Actual<TIM_CR1_URS >(); }
		
		//Counter mode registers
		auto ARPE() { return base_t::template Actual<TIM_CR1_ARPE>(); }
		auto CMS () { return base_t::template Actual<TIM_CR1_CMS >(); }
		auto DIR () { return base_t::template Actual<TIM_CR1_DIR >(); }
		auto OPM () { return base_t::template Actual<TIM_CR1_OPM >(); }
		
		//Filter based registers
		auto CKD () { return base_t::template Actual<TIM_CR1_CKD >(); }
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, CR2)>
	struct CR2 : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto TI1S() { return base_t::template Actual<TIM_CR2_TI1S>(); }
		auto MMS () { return base_t::template Actual<TIM_CR2_MMS >(); }
		auto CCDS() { return base_t::template Actual<TIM_CR2_CCDS>(); }
		auto CCPC() { return base_t::template Actual<TIM_CR2_CCPC>(); }
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, SMCR)>
	struct SMCR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		//Trigger based registers
		auto TS  () { return base_t::template Actual<TIM_SMCR_TS  >(); }
		auto ETP () { return base_t::template Actual<TIM_SMCR_ETP >(); }
		auto ETF () { return base_t::template Actual<TIM_SMCR_ETF >(); }
		auto ETPS() { return base_t::template Actual<TIM_SMCR_ETPS>(); }
		
		//External clock enable
		auto ECE () { return base_t::template Actual<TIM_SMCR_ECE >(); }
		
		auto MSM () { return base_t::template Actual<TIM_SMCR_MSM >(); }
		auto OCCS() { return base_t::template Actual<TIM_SMCR_OCCS>(); }
		auto SMS () { return base_t::template Actual<TIM_SMCR_SMS >(); }
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, DIER)>
	struct DIER : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		//DMA bits in field
		auto TDE  () { return base_t::template Actual<TIM_DIER_TDE  >(); }
		auto CC4DE() { return base_t::template Actual<TIM_DIER_CC4DE>(); }
		auto CC3DE() { return base_t::template Actual<TIM_DIER_CC3DE>(); }
		auto CC2DE() { return base_t::template Actual<TIM_DIER_CC2DE>(); }
		auto CC1DE() { return base_t::template Actual<TIM_DIER_CC1DE>(); }
		auto UDE  () { return base_t::template Actual<TIM_DIER_UDE  >(); }
		
		//Interrupt bits in field
		auto TIE  () { return base_t::template Actual<TIM_DIER_TIE  >(); }
		auto CC4IE() { return base_t::template Actual<TIM_DIER_CC4IE>(); }
		auto CC3IE() { return base_t::template Actual<TIM_DIER_CC3IE>(); }
		auto CC2IE() { return base_t::template Actual<TIM_DIER_CC2IE>(); }
		auto CC1IE() { return base_t::template Actual<TIM_DIER_CC1IE>(); }
		auto UIE  () { return base_t::template Actual<TIM_DIER_UIE  >(); }
		
		//Helper functions
		template <Request ... Values>
		void DMAEnable( bool const Active )
		{
			if constexpr (General::IsOneOf(Request::CaptureCompare1, Values...))
				CC1DE() = Active;
			if constexpr (General::IsOneOf(Request::CaptureCompare2, Values...))
				CC2DE() = Active;
			if constexpr (General::IsOneOf(Request::CaptureCompare3, Values...))
				CC3DE() = Active;
			if constexpr (General::IsOneOf(Request::CaptureCompare4, Values...))
				CC4DE() = Active;
			if constexpr (General::IsOneOf(Request::Trigger, Values...))
				TDE() = Active;
			if constexpr (General::IsOneOf(Request::Update, Values...))
				UDE() = Active;
		}
		template <Request ... Values>
		void EnableInterrupt( bool const Active )
		{
			if constexpr (General::IsOneOf(Request::CaptureCompare1, Values...))
				CC1IE() = Active;
			if constexpr (General::IsOneOf(Request::CaptureCompare2, Values...))
				CC2IE() = Active;
			if constexpr (General::IsOneOf(Request::CaptureCompare3, Values...))
				CC3IE() = Active;
			if constexpr (General::IsOneOf(Request::CaptureCompare4, Values...))
				CC4IE() = Active;
			if constexpr (General::IsOneOf(Request::Trigger, Values...))
				TIE() = Active;
			if constexpr (General::IsOneOf(Request::Update, Values...))
				UIE() = Active;
		}
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, SR)>
	struct SR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto CC4OF() { return base_t::template Actual<TIM_SR_CC4OF>(); }
		auto CC3OF() { return base_t::template Actual<TIM_SR_CC3OF>(); }
		auto CC2OF() { return base_t::template Actual<TIM_SR_CC2OF>(); }
		auto CC1OF() { return base_t::template Actual<TIM_SR_CC1OF>(); }
		
		auto BIF  () { return base_t::template Actual<TIM_SR_BIF	 >(); }
		auto TIF  () { return base_t::template Actual<TIM_SR_TIF  >(); }
		
		auto CC4IF() { return base_t::template Actual<TIM_SR_CC4IF>(); }
		auto CC3IF() { return base_t::template Actual<TIM_SR_CC3IF>(); }
		auto CC2IF() { return base_t::template Actual<TIM_SR_CC2IF>(); }
		auto CC1IF() { return base_t::template Actual<TIM_SR_CC1IF>(); }
		auto UIF  () { return base_t::template Actual<TIM_SR_UIF  >(); }
		auto COMIF() { return base_t::template Actual<TIM_SR_COMIF>(); }
		
		void Clear()
		{
			base_t::operator =(0u);
		}
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, EGR)>
	struct EGR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto TG  () { return base_t::template Actual<TIM_EGR_TG  >(); }
		auto CC4G() { return base_t::template Actual<TIM_EGR_CC4G>(); }
		auto CC3G() { return base_t::template Actual<TIM_EGR_CC3G>(); }
		auto CC2G() { return base_t::template Actual<TIM_EGR_CC2G>(); }
		auto CC1G() { return base_t::template Actual<TIM_EGR_CC1G>(); }
		auto UG  () { return base_t::template Actual<TIM_EGR_UG  >(); }
	};

	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, CCMR1)>
	struct CCMR1 : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		//Capture compare selection registers
		auto CC2S 	() { return base_t::template Actual<TIM_CCMR1_CC2S >(); 		}
		auto CC1S 	() { return base_t::template Actual<TIM_CCMR1_CC1S >(); 		}
		
		//Output compare registers
		auto OC2CE	() { return base_t::template Actual<TIM_CCMR1_OC2CE>(); 		}
		auto OC2M 	() { return base_t::template Actual<TIM_CCMR1_OC2M >(); 		}
		auto OC2PE	() { return base_t::template Actual<TIM_CCMR1_OC2PE>(); 		}
		auto OC2FE	() { return base_t::template Actual<TIM_CCMR1_OC2FE>(); 		}
		auto OC1CE	() { return base_t::template Actual<TIM_CCMR1_OC1CE>(); 		}
		auto OC1M 	() { return base_t::template Actual<TIM_CCMR1_OC1M >(); 		}
		auto OC1PE	() { return base_t::template Actual<TIM_CCMR1_OC1PE>(); 		}
		auto OC1FE	() { return base_t::template Actual<TIM_CCMR1_OC1FE>(); 		}
		
		//Input compare registers
		auto IC2F 	() { return base_t::template Actual<TIM_CCMR1_IC2F >();	 	}
		auto IC1F 	() { return base_t::template Actual<TIM_CCMR1_IC1F >();	 	}
		auto IC2PSC	() { return base_t::template Actual<TIM_CCMR1_IC2PSC  >();	}
		auto IC1PSC	() { return base_t::template Actual<TIM_CCMR1_IC1PSC  >();	}
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, CCER)>
	struct CCER : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto CC4NP() { return base_t::template Actual<TIM_CCER_CC4NP>(); }
		auto CC4P () { return base_t::template Actual<TIM_CCER_CC4P >(); }
		auto CC4E () { return base_t::template Actual<TIM_CCER_CC4E >(); }
		auto CC3NP() { return base_t::template Actual<TIM_CCER_CC3NP>(); }
		auto CC3P () { return base_t::template Actual<TIM_CCER_CC3P >(); }
		auto CC3E () { return base_t::template Actual<TIM_CCER_CC3E >(); }
		auto CC2NP() { return base_t::template Actual<TIM_CCER_CC2NP>(); }
		auto CC2P () { return base_t::template Actual<TIM_CCER_CC2P >(); }
		auto CC2E () { return base_t::template Actual<TIM_CCER_CC2E >(); }
		auto CC1NP() { return base_t::template Actual<TIM_CCER_CC1NP>(); }
		auto CC1P () { return base_t::template Actual<TIM_CCER_CC1P >(); }
		auto CC1E () { return base_t::template Actual<TIM_CCER_CC1E >(); }
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, CNT)>
	struct CNT : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto & operator = (std::uint32_t const & pInput)
		{
			if constexpr (Channel == 2) base_t::operator =(pInput);
			else base_t::template Actual<TIM_CNT_CNT>() = pInput;
			return *this;
		}
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, PSC)>
	struct PSC : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		static constexpr auto Maximum = 0xffffu;
		
		auto & operator = (std::uint32_t const & pInput)
		{
			base_t::template Actual<TIM_PSC_PSC>() = pInput;
			return *this;
		}
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, ARR)>
	class ARR : General::u32_reg<A>
	{
	public:
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
	private:
		static constexpr std::uint32_t GetMax() noexcept
		{
			if (Channel == 2u) return 0xffffffffu;
			return 0xffffu;
		}
	public:
		static constexpr auto Maximum = GetMax();
		
		auto & operator = (std::uint32_t const & pInput)
		{
			if constexpr (Channel == 2) base_t::operator = (pInput);
			else base_t::template Actual<TIM_ARR_ARR>() = pInput;
			return *this;
		}
		
		
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, CCR1)>
	struct CCR1 : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto & operator = (std::uint32_t const & pInput)
		{
			if constexpr (Channel == 2) base_t::operator =(pInput);
			else base_t::template Actual<TIM_CCR1_CCR1>() = pInput;
			return *this;
		}
	};

	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, CCR2)>
	struct CCR2 : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto & operator = (std::uint32_t const & pInput)
		{
			if constexpr (Channel == 2) base_t::operator =(pInput);
			else base_t::template Actual<TIM_CCR2_CCR2>() = pInput;
			return *this;
		}
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, CCR3)>
	struct CCR3 : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto & operator = (std::uint32_t const & pInput)
		{
			if constexpr (Channel == 2) base_t::operator =(pInput);
			else base_t::template Actual<TIM_CCR3_CCR3>() = pInput;
			return *this;
		}
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, CCR4)>
	struct CCR4 : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto & operator = (std::uint32_t const & pInput)
		{
			if constexpr (Channel == 2) base_t::operator =(pInput);
			else base_t::template Actual<TIM_CCR4_CCR4>() = pInput;
			return *this;
		}
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, DCR)>
	struct DCR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto DBL() { return base_t::template Actual<TIM_DCR_DBL>(); }
		auto DBA() { return base_t::template Actual<TIM_DCR_DBA>(); }
	};
	
	template <unsigned Channel, std::size_t A = TimerRegisterBase(Channel) + offsetof(TIM_TypeDef, DMAR)>
	struct DMAR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto DMAB() { return base_t::template Actual<TIM_DMAR_DMAB>(); }
	};
};