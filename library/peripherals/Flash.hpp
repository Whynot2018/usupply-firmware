#pragma once

#include "RegistersFlash.hpp"
#include "Math.hpp"
#include <utility>

namespace Peripherals
{
	//These are device specific, and should be template defined
	static constexpr std::size_t FlashStart	= FLASH_BASE;
	static constexpr std::size_t FlashEnd 	= FLASH_BASE + 0x20000;
	static constexpr std::size_t PageSize 	= 0x800u;
	static constexpr std::size_t FlashSize 	= FlashEnd - FlashStart;
	/**
	 * @brief All flash addresses are indexed from the first flash address
	 * 
	 * Addresses therefore in function parameters should start with 0 
	 * and end with the data capacity of the flash
	 */
	struct FlashLocker
	{
		/**
		 * @brief Unlocks flash on construction.
		 */
		FlashLocker() noexcept
		{
			::FLASH_Unlock();
		}
		/**
		 * @brief Locks the flash on destruction.
		 */
		~FlashLocker()
		{
			::FLASH_Lock();
		}
	};
	/**
	 * @brief A manager class of the Flash module.
	 * 
	 */
	class Flash : FlashLocker
	{
	private:
		/**
		 * @brief A helper class for the intermediate variables for a flash transaction.
		 * 
		 * @note This is a metaprogramming tool, not used for runtime calculations.
		 * 
		 * @tparam Address The address to read or write from.
		 * @tparam Size The size of the transaction.
		 */
		template <std::size_t Address, std::size_t Size>
		struct Details
		{
			static constexpr std::size_t
				StartIndex 			= Address - FlashStart,
				StartPage 			= General::FloorDivide(StartIndex, PageSize),
				StartPageOffset 	= StartIndex % PageSize,
				StartPageLength 	= General::Minimum(PageSize - StartPageOffset, Size) % PageSize,
				FullPage 			= (StartPageLength > 0u) ? StartPage + 1u : StartPage,
				FullPages 			= General::FloorDivide(Size - StartPageLength, PageSize),
				FullPagesLength 	= (FullPages * PageSize),
				EndPageLength 		= Size - FullPagesLength - StartPageLength,
				EndPage				= FullPage + FullPages;
		};
		/**
		 * @brief Blocks while the flash peripheral is busy.
		 * 
		 */
		static void WhileBusy() noexcept
		{
			using namespace FlashGeneral;
			while (SR{}.BSY());
		}
		/**
		 * @brief RAII helper for enabling and disabling programablility of the flash.
		 */
		struct ProgramMode
		{
			ProgramMode() noexcept	{ FlashGeneral::CR{}.PG() = true; 	}
			~ProgramMode() 			{ FlashGeneral::CR{}.PG() = false; 	}
		};
		/**
		 * @brief RAII helper for enabling and disabling mass erase mode of the flash.
		 */
		struct MassEraseMode
		{
			MassEraseMode() noexcept{ FlashGeneral::CR{}.MER() = true; }
			~MassEraseMode() 		{ FlashGeneral::CR{}.MER() = false; }
		};
		/**
		 * @brief RAII helper for enabling and disabling page erase mode of the flash.
		 */
		struct PageEraseMode
		{
			PageEraseMode() noexcept{ FlashGeneral::CR{}.PER() = true; }
			~PageEraseMode()		{ FlashGeneral::CR{}.PER() = false; }
		};
		/**
		 * @brief Tests whether or not an address is a valid flash write address.
		 * 
		 * @tparam T The type that is being read/written
		 * @tparam Address The address for the transaction.
		 * @return true The address is valid.
		 * @return false The address is invalid.
		 */
		template<typename T, std::size_t Address>
		static constexpr bool ValidAddress() noexcept
		{
			return 	( General::Between( Address, FlashStart, FlashEnd ) ) &&
					( General::Between( Address + sizeof(T), FlashStart, FlashEnd ) );
		}
		/**
		 * @brief Issue a end of flash operation.
		 * 
		 * @return true The operation completed.
		 * @return false The operation failed.
		 */
		static bool EndOperation() noexcept
		{
			using namespace FlashGeneral;
			WhileBusy();
			if (! SR{}.EOP() ) return false;
			SR{}.EOP() = true;
			return true;
		}
		/**
		 * @brief Read a 16 bit value.
		 * 
		 * @tparam Address The address of the read.
		 * @return std::uint16_t The value read.
		 */
		template <std::size_t Address>
		static std::uint16_t Read16() noexcept
		{
			using namespace FlashGeneral;
			using type = std::uint16_t;
			static_assert(ValidAddress<type, Address>(), "The flash address tried to access to is beyond the devices range.");
			return (*(volatile uint16_t*)(Address));
		}
		/**
		 * @brief Writes a 16 bit value.
		 * 
		 * @param input The value to write to the address.
		 * @param address The address to write to.
		 */
		static void Write16( std::uint16_t input, volatile std::size_t address ) noexcept
		{
			// Wait for operations to complete
			WhileBusy();

			// Write to register, does this work, or do i need FLASH_AR
			(*(volatile uint16_t*)(address)) = input;
		}
		static constexpr auto page_data_size = PageSize / 2u;
		using page_data_t = std::array< volatile std::uint16_t, page_data_size>;
		/**
		 * @brief A manager class for flash pages.
		 */
		struct Page
		{
			std::size_t const Address = 0u;
			/**
			 * @brief Construct a new Page object
			 * 
			 * @param input The page index.
			 */
			Page( std::size_t const & input ) noexcept :
				Address{ FlashStart + input * PageSize }
			{}
			/**
			 * @brief Erase a single page of data.
			 * 
			 * @return true Erase succeeded.
			 * @return false Erase failed.
			 */
			bool Erase() const noexcept
			{
				using namespace FlashGeneral;
				
				WhileBusy();
				
				//Start page erase mode
				volatile PageEraseMode init;
				
				//Write to the address register
				AR{} = Address;
				
				//Start the page erase
				CR{}.STRT() = true;
				
				return EndOperation();
			}
			/**
			 * @brief Read an entire page.
			 * 
			 * @return page_data_t The page data.
			 */
			page_data_t Read() const noexcept
			{
				page_data_t output;
				for ( auto i = 0u; i < output.size(); ++i )
					output[ i ] = *(volatile uint16_t*)( Address + (i * 2u) );
				return output;
			}
			/**
			 * @brief Writes a whole page into memory.
			 * 
			 * @warning Erase must be performed before any write.
			 * 
			 * @param input The page data to write.
			 * @return true Write succeeded.
			 * @return false Write failed.
			 */
			bool Write( page_data_t const & input ) const noexcept
			{
				//Start program mode
				volatile ProgramMode init;

				for ( auto i = 0u; i < input.size(); ++i )
					Write16( input[i], Address + (i * 2u) );

				return EndOperation();
			}
			/**
			 * @brief Write data into the page.
			 * 
			 * @tparam T the data type to write.
			 * @param input The data to write.
			 * @return true Write succeeded.
			 * @return false Write failed.
			 */
			template <typename T>
			bool Write( T const & input ) const noexcept
			{
				std::size_t i = 0;
				auto const & data = (std::array<std::uint16_t, sizeof(std::decay_t<T>)> const &)input;
		
				//Start program mode
				volatile ProgramMode init;

				//Start of the page
				for ( ; i < data.size(); ++i )
					Write16( data[i], Address + (i * 2u) );

				//Remaining sections of the page
				for ( ; i < page_data_size; ++i)
					Write16( 0, Address + (i * 2u) );

				return EndOperation();
			}
			/**
			 * @brief Erases a page then writes new data into the page.
			 * 
			 * @param input The data to write into the page.
			 * @return true Write or erase succeeded.
			 * @return false Write or erase failed.
			 */
			bool EraseWrite( page_data_t const & input ) const noexcept
			{
				// Erase the flash memory
				if ( !Erase() ) return false;
				
				// Write buffer to memory
				return Write( input );
			}
			/**
			 * @brief Perform a copy then modifies the copy, then erases the flash then re-writes the copy.
			 * 
			 * @tparam Offset The offset in the array to start the write.
			 * @tparam Length The length of the write.
			 * @param input The data to write into the page.
			 * @return true The write succeeded.
			 * @return false The write failed.
			 */
			template <std::size_t Offset, std::size_t Length>
			bool CopyModifyEraseWrite(std::array<std::uint16_t, Length> const & input) const noexcept
			{
				static_assert((Offset % 2u) 	== 0, "The offset must be aligned 2-byte boundarys.");
				static_assert((Offset + Length) <= PageSize, "Offset + Length is larger than PageSize.");
				
				//Read flash memory from page
				auto buffer{ Read() };
				
				//Copy data into buffer
				for ( auto i = 0u; i < Length; ++i ) 
					buffer[Offset / 2u + i] = input[i];
				
				//Write buffer to memory
				return EraseWrite(buffer);
			}
			/**
			 * @brief This is a blind erase write, the remaining sections of the page will be assigned 0
			 * 
			 * This performs the following steps:
			 * @li Erase
			 * @li Write
			 * 
			 * @tparam T The type of tha data to write.
			 * @param data The data that is written.
			 * @return true The write succeeded.
			 * @return false The write failed.
			 */
			template <typename T>
			bool EraseWrite( T const & data ) const noexcept
			{
				//Erase the flash memory
				if ( !Erase() ) return false;
				
				//Write buffer to memory
				return Write( data );
			}
		};
	public:
		/**
		 * @brief Construct a new Flash object
		 */
		Flash() = default;
		/**
		 * @brief A datatype that can represent half a DMA transaction.
		 * 
		 * @tparam Length The length of the whole buffer size.
		 */
		template <std::size_t Length>
		using half_buffer = std::array<std::uint16_t, Length / 2u>;
		/**
		 * @brief Writes a block of data into flash.
		 * 
		 * @tparam Address The address to write.
		 * @tparam T The type to write.
		 * @param input The data to write.
		 * @return true Write succeeded.
		 * @return false Write failed.
		 */
		template <std::size_t Address, typename T>
		bool Write( T const & input ) const noexcept
		{
			static_assert( (Address % 2u) == 0u, "WriteEven must be aligned to a even boundary.");
			static_assert( ValidAddress< T, Address >(), "The address you tryed to write is beyond the devices range." );
			
			using details_t = Details<Address, sizeof(std::decay_t<T>)>;
			
			auto constexpr has_start_page 	= ( details_t::StartPageLength 	> 0u 	);
			auto constexpr has_full_pages 	= ( details_t::FullPagesLength	> 0u 	);
			auto constexpr has_end_page 	= ( details_t::EndPageLength 	> 0u 	);
			
			auto const & all_data = (std::array<std::uint8_t, sizeof(T)>&)input;
			
			std::size_t offset = 0u;
			if constexpr ( has_start_page )
			{
				auto constexpr start_page		= ( details_t::StartPage );
				auto constexpr start_offset		= ( details_t::StartPageOffset );
				auto constexpr start_length 	= ( details_t::StartPageLength );
			
				//Read, Modify, Erase, Write
				auto const & data = ( half_buffer<start_length>& ) all_data[ offset ];
				if (!Page( start_page ).CopyModifyEraseWrite<start_offset>( data ))
					return false;
				
				offset += start_length;
			}
			if constexpr ( has_full_pages )
			{
				auto constexpr full_page			= ( details_t::FullPage );
				auto constexpr full_pages 			= ( details_t::FullPages );
				auto constexpr full_pages_length	= ( details_t::FullPagesLength );
				
				//Erase, Write
				for ( auto page = full_page; page < (full_page + full_pages); ++page )
				{
					auto const & data = ( page_data_t& ) all_data[ offset ];
					if ( !Page( page ).EraseWrite( data ) )
						return false;
					
					offset += PageSize;
				}
			}
			if constexpr ( has_end_page )
			{
				auto constexpr end_page 			= ( details_t::EndPage );
				auto constexpr end_length			= ( details_t::EndPageLength );
				
				//Read, Modify, Erase, Write
				auto const & data = ( half_buffer<end_length>& ) all_data[ offset ];
				if (!Page( end_page ).CopyModifyEraseWrite<0u>( data ))
					return false;
			}
			
			return false;
		}
		/**
		 * @brief Overrides a page with data, remaining space in page is left erased.
		 * 
		 * @tparam Address The address to write.
		 * @tparam T The type to write.
		 * @param input The data to write.
		 * @return true The write succeeded.
		 * @return false The write failed.
		 */
		template <std::size_t Address, typename T>
		bool OverridePage( T const & input ) noexcept
		{
			using details_t = Details<Address, sizeof(std::decay_t<T>)>;
			static_assert( (Address % 2u) == 0u, "WriteEven must be aligned to a even boundary.");
			static_assert( ValidAddress< T, Address >(), "The address you tryed to write is beyond the devices range." );

			auto constexpr has_start_page 	= ( details_t::StartPageLength 	> 0u 	);
			auto constexpr has_full_pages 	= ( details_t::FullPagesLength	> 0u 	);
			auto constexpr has_end_page 	= ( details_t::EndPageLength 	> 0u 	);

			//Must only occupy a single page
			static_assert( (has_start_page + has_full_pages + has_end_page) == 1, "The address you tryed to write is beyond the devices range." );

			//Lambda return the working page to override
			auto const page = []
			{
				if constexpr ( has_start_page )
					return Page( details_t::StartPage );
				if constexpr ( has_full_pages )
					return Page( details_t::FullPage );
				if constexpr ( has_end_page )
					return Page( details_t::EndPage );
			}();

			//Override the page
			return page.EraseWrite( input );
		}
		/**
		 * @brief Reads 16 bits from member at an address. 
		 * 
		 * @tparam Address The address to read.
		 * @return std::uint16_t The data.
		 */
		template <std::size_t Address>
		std::uint16_t Read() noexcept
		{
			return Read16<Address>();
		}
	};
}