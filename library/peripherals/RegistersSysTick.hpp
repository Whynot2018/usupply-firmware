#pragma once

#include "Meta.hpp"
#include "Register.hpp"
#include <cstdint>

namespace Peripherals::SysTickGeneral
{
	struct CTRL : General::u32_reg<SysTick_BASE + offsetof(SysTick_Type, CTRL)>
    {
        using base_t = General::u32_reg<SysTick_BASE + offsetof(SysTick_Type, CTRL)>;
        using base_t::base_t;
        //
        auto ENABLE()   { return base_t::template Actual<SysTick_CTRL_ENABLE_Msk>(); }
        auto TICKINT()  { return base_t::template Actual<SysTick_CTRL_TICKINT_Msk>(); }
        auto CLKSOURCE(){ return base_t::template Actual<SysTick_CTRL_CLKSOURCE_Msk>(); }
        bool COUNTFLAG(){ return base_t::template Actual<SysTick_CTRL_COUNTFLAG_Msk>(); }
        //
    };

	struct RELOAD : General::u32_reg<SysTick_BASE + offsetof(SysTick_Type, LOAD)>
    {
        using base_t = General::u32_reg<SysTick_BASE + offsetof(SysTick_Type, LOAD)>;
        using base_t::base_t;
        //
        RELOAD & operator = (std::uint32_t value) 
        {
            base_t::template Actual<SysTick_LOAD_RELOAD_Msk>() = value;
            return *this; 
        }
        operator std::uint32_t () const
        {
            return base_t::template Actual<SysTick_LOAD_RELOAD_Msk>();
        }
    };

    struct CURRENT : General::u32_reg<SysTick_BASE + offsetof(SysTick_Type, VAL)>
    {
        using base_t = General::u32_reg<SysTick_BASE + offsetof(SysTick_Type, VAL)>;
        using base_t::base_t;
        
        void Clear()
        {
            //Any write causes a clear
            base_t::template Actual<SysTick_LOAD_RELOAD_Msk>() = 0;
        }
        std::uint32_t Read() const
        {
            return base_t::template Actual<SysTick_VAL_CURRENT_Msk>();
        }
        operator std::uint32_t () const
        {
            return Read();
        }
    };

    struct CALIB : General::u32_reg<SysTick_BASE + offsetof(SysTick_Type, CALIB)>
    {
        using base_t = General::u32_reg<SysTick_BASE + offsetof(SysTick_Type, CALIB)>;
        using base_t::base_t;

        auto NOREF(){ return base_t::template Actual<SysTick_CALIB_NOREF_Msk>(); }
        auto SKEW() { return base_t::template Actual<SysTick_CALIB_SKEW_Msk>();  }
        auto TENMS(){ return base_t::template Actual<SysTick_CALIB_TENMS_Msk>(); }
    };
}