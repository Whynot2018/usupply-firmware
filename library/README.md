**uSupply Library**

This repo is located at:
https://gitlab.com/Sepps/usupply-library.git

Push with:
git subtree push --prefix library usupply-library master

Pull with:
git subtree pull --prefix library usupply-library master

This is where the latest and greatest of the uSupply library is developed.

The firmware is written in **c++17**.
The project is a cmake project, compiled with **GCC**. Developed primarily with visual studio code for portability (its on all platforms).

If you have feature requests log them into issues.
If you would like to contribute, simply submit patches, merge requests and issues.

The uSupply can be purchased from:
www.eevblog.com/store/usupply