#pragma once
#include "ArrayConvert.hpp"
#include "FIFO.hpp"
#include "Integer.hpp"
#include "Math.hpp"
#include <type_traits>

namespace General
{
	/**
	 * @brief A helper class which simply stores some information that assists in integer conversions.
	 * 
	 * @tparam T The integer type.
	 */
	template <typename T>
	struct BaseInfo
	{
		T const Power;
		T const Maximum;
	};
	/**
	 * @brief Returns maximum number of digits minus 1 necessary to store 
	 * a number in a particular base for a particular integer.
	 * 
	 * For example:
	 * When base = 10
	 * 
	 * uint64_t max
	 * 18 446 744 073 709 551 615 = 19
	 * 20 digits
	 * 
	 * uint32_t max
	 * 4 294 967 295 = 9
	 * 10 digits
	 * 
	 * uint16_t max
	 * 65535 = 4
	 * 5 digits
	 * 
	 * uint8_t max
	 * 255 = 2
	 * 3 digits
	 * 
	 * @tparam T The type to test.
	 * @param base The base system.
	 * @return T The number of digits - 1 nessary to express the maximum value in a given integer type.
	 */
	template <typename T>
	constexpr auto LOP( T base ) noexcept -> T
	{
		T const multiple = base;
		T       power{ 1 }, scaling{ base };
		//
		while ( ( ( scaling *= multiple ) / multiple ) == base )
		{
			base = scaling;
			++power;
		}
		//
		return power;
	}
	/**
	 * @brief Gets a lookup table of the numeric limits of each digits of an integer for each integer printable with a given integer type.
	 * 
	 * @tparam T The integer type to test.
	 * @tparam Base The base system to derive the table from.
	 * @tparam I An integer sequence that an be used to generate the particular digit limits.
	 * @return std::array<T, sizeof...( I )> The lookup table.
	 */
	template <typename T, std::size_t Base, std::size_t... I>
	constexpr auto DigitsLookup( std::index_sequence<I...> ) noexcept -> std::array<T, sizeof...( I )>
	{
		return {Power<T, Base>( I )...};
	}
	/**
	 * @brief Generates a lookup table for all possible digits printable in a given base with a given integer type.
	 * 
	 * @tparam T The integer type to test.
	 * @tparam Base The base system to use.
	 * @return std::array<T, N> The complete lookup table.
	 */
	template <typename T, T Base>
	constexpr auto DigitsLookup() noexcept
	{
		return DigitsLookup<T, Base>( std::make_index_sequence<LOP<T>( Base ) + (T)1>() );
	}
	/**
	 * @brief Converts an unsigned integer to a character array.
	 * 
	 * @tparam N The capacity of the output storage.
	 * @tparam T The type of the integer.
	 * @param input An unsigned integer to convert.
	 * @param output The output array.
	 * @param offset The index to start outputting into the output array.
	 * @param padding Any blank characters that are printed will use this character.
	 * @return std::size_t The number of printed characters.
	 */
	template <std::size_t N, typename T = std::size_t>
	std::size_t utoa( T input, std::array<char, N> & output, std::size_t const offset = 0u, char const padding = ' ' ) noexcept
	{
		static constexpr T	zero = (T)0;
		static constexpr T	ten = (T)10;
		std::size_t length = offset;
		if ( offset < N )
		{
			if ( input == zero )
				output[ length++ ] = '0';
			else
			{
				auto index = offset;
				while ( input > zero )
				{
					ShiftRight( output, offset, padding );
					output[ index ] = FromNumber( input % ten );
					input /= ten;
					++length;
				}
			}
		}
		return length - offset;
	}
	/**
	 * @brief Converts an unsigned integer to a FIFO.
	 * 
	 * @tparam N The capacity of the FIFO.
	 * @tparam T The type of the integer to convert.
	 * @tparam Base The base system to use in the conversion.
	 * @param input The integer to convert.
	 * @param output The fifo to push data into.
	 * @return std::size_t The number of characters inserted into the FIFo.
	 */
	template <std::size_t N, typename T = std::size_t, T Base = 10u>
	std::size_t utoa( T input, Containers::FIFO<char, N> &output ) noexcept
	{
		constexpr auto Digits{General::DigitsLookup<T, Base>()};
		T              length = 0;
		T              index  = 0;
		bool           pushed = false;
		while ( index < Digits.size() )
		{
			auto const current_index = Digits.size() - index - 1u;
			T const    base{Digits[ current_index ]};
			T          sampr{0u}, value{0u};
			T          count{0};

			while ( ( sampr += base ) <= input )
			{
				value += base;
				++count;
			}
			++index;

			input -= value;
			if ( count != 0 || pushed )
			{
				pushed |= true;
				if ( output.Push( FromNumber( count ) ) )
					++length;
				else
					return index;
			}
		}
		if ( length == 0 )
			if ( output.Push( '0' ) )
				++length;
		return length;
	}
	/**
	 * @brief Converts a signed integer to to an array of characters.
	 * 
	 * This class does support unsigned integers. To use this with unsigned integers use the is_negative parameter to 
	 * indicate that a negative number is present.
	 * 
	 * @tparam N The capacity of the output array.
	 * @tparam T The type of the input integer.
	 * @param input The integer.
	 * @param output The output array.
	 * @param offset The index to start printing characters into the output.
	 * @param padding Blank characters will be inserted as this character.
	 * @param is_negative If this is true, the integer will be printed as negative regardless of the sign in input.
	 * @return std::size_t The number of inserted characters into the array.
	 */
	template <std::size_t N, typename T = int>
	std::size_t itoa( T input, std::array<char, N> &output, std::size_t const offset = 0u, char const padding = ' ', bool is_negative = false ) noexcept
	{
		using uint_t    = General::UnsignedInt_t<sizeof( T ) * 8u>;
		std::size_t length = 0u;

		if ( offset >= N )
			return length;

		if ( input < (T)0 )
		{
			input = -input;
			is_negative |= true;
		}
		if ( is_negative )
		{
			output[ offset ] = '-';
			++length;
		}

		length += utoa<N, uint_t>( input, output, offset + length, padding );
		return length;
	}
	/**
	 * @brief Converts a signed integer into a FIFO of characters.
	 * 
	 * This class does support unsigned integers. To use this with unsigned integers use the is_negative parameter to 
	 * indicate that a negative number is present.
	 * 
	 * @tparam N The capacity of the output FIFO.
	 * @tparam T The type of the input integer.
	 * @param input The integer.
	 * @param output The output FIFO.
	 * @param is_negative If this is true, the integer will be printed as negative regardless of the sign in input.
	 * @return std::size_t The number of inserted characters into the FIFO.
	 */
	template <std::size_t N, typename T = int>
	std::size_t itoa( T input, Containers::FIFO<char, N> &output, bool is_negative = false ) noexcept
	{
		using uint_t    = General::UnsignedInt_t<sizeof( T ) * 8u>;
		std::size_t length = 0u;

		if ( output.Full() )
			return length;

		if ( input < (T)0 )
		{
			input = -input;
			is_negative |= true;
		}
		if ( is_negative )
		{
			if (output.Push( '-' ))
				++length;
			else 
				return length;
		}

		length += utoa<N, uint_t>(
		    input,
		    output );
		return length;
	}
	/**
	 * @brief Converts an unsigned integer and inserts it into an output array.
	 * 
	 * @tparam fixed_length The maximum index in the output array to access.
	 * @tparam N The capacity of the output array.
	 * @tparam T The integer type.
	 * @param input The integer.
	 * @param output The output array.
	 * @param offset The index to start printing characters into the output.
	 * @param padding Any blank printed characters will be printed as this character.
	 * @return std::size_t The number of characters that were printed.
	 */
	template <std::size_t fixed_length, std::size_t N, typename T = std::size_t>
	std::size_t fixed_utoa( T input, std::array<char, N> &output, std::size_t offset = 0, char padding = ' ' ) noexcept
	{
		if ( offset >= N ) return 0;
		//
		using container = std::array<char, fixed_length>;
		auto & out = (container &)output[ offset ];
		std::size_t l = utoa<fixed_length, T>( input, out );
		while ( l++ < fixed_length ) ShiftRight<char, N, fixed_length>( output, offset, padding );
		//
		return fixed_length;
	}
	/**
	 * @brief Converts a signed integer to a array.
	 * 
	 * @tparam fixed_length The uppper bounds for the access to the output array (nothing printed beyond this point)
	 * @tparam N The capacity of the outptu array.
	 * @tparam T The integer type.
	 * @param input The signed integer.
	 * @param output The output array.
	 * @param offset The index to start printing into the output array.
	 * @param padding Any blank printed characters will be printed as this character.
	 * @return std::size_t The number of printed characters.
	 */
	template <std::size_t fixed_length, std::size_t N, typename T = int>
	std::size_t fixed_itoa( T input, std::array<char, N> &output, std::size_t offset = 0, char padding = ' ' ) noexcept
	{
		if ( offset >= N ) return 0;
		//
		using container = std::array<char, fixed_length>;
		auto &out    = (container &)output;
		std::size_t l = itoa( input, out, offset );
		while ( l++ < fixed_length ) ShiftRight<char, N, fixed_length>( output, offset, padding );
		//
		return fixed_length;
	}
	/**
	 * @brief Converts an array of character to an unsigned integer.
	 * 
	 * @tparam N The capacity of the output array.
	 * @tparam T The output for the parsing of the string parsing.
	 * @param input The input array.
	 * @param output The output unsigned integer.
	 * @param offset The index to start parsing the input.
	 * @return std::size_t The number of parsed characters.
	 */
	template <std::size_t N, typename T>
	constexpr std::size_t atou( std::array<char, N> const &input, T & output, std::size_t offset = 0 ) noexcept
	{
		T        before = (T)0;
		std::size_t length = 0u;

		for ( auto i = offset; i < input.size(); ++i )
		{
			auto const item = input[ i ];

			if ( IsNumber( item ) )
			{
				auto const value = ToNumber( item );
				before *= (T)10;
				before += value;
			}
			else
				break;
			++length;
		}
		output = before;
		return length;
	}
	/**
	 * @brief Converts a array of characters to a signed integer.
	 * 
	 * @tparam N The capcity of the input array.
	 * @tparam T The type to output into.
	 * @param input The input array.
	 * @param output The output value.
	 * @param offset The index to start parsing in the input array.
	 * @return std::size_t The number of parsed characters.
	 */
	template <std::size_t N, typename T>
	constexpr std::size_t atoi( std::array<char, N> const &input, T &output, std::size_t offset = 0 ) noexcept
	{
		bool     	negative = false;
		std::size_t length   = 0u;
		//
		for ( auto i = offset; i < input.size(); ++i )
		{
			auto const item = input[ i ];
			if ( IsSign( item ) )
			{
				negative ^= ( item == '-' );
				++length;
				continue;
			}
			General::UnsignedVersion_t<T> temporary = 0;
			length += atou( input, temporary, i );
			output = (T)temporary;
			if ( negative ) output = -output;
			break;
		}
		return length;
	}
}