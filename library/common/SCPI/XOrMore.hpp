#pragma once

#include "BasicBlock.hpp"
#include "Folding.hpp"
#include "Meta.hpp"
#include <array>

namespace Parser
{
	/**
	 * @brief A parser that checks that X or more instances of a parser exist sequentially.
	 * 
	 * @tparam L The length of the section to check.
	 * @tparam X The number of times the section should be present.
	 */
	template <std::size_t L, std::size_t X = 0u>
	class XOrMore : BasicBlock<L>
	{
	public:
		using base_t = BasicBlock<L>;
	private:
		template <std::size_t N, std::size_t ... Indexes>
		constexpr bool IgnoreOnce( std::array<char, N> const & pInput, std::size_t index, std::index_sequence<Indexes...> ) const noexcept
		{
			return General::IsAnyOf(pInput[ index ], base_t::m_Data[Indexes] ... );
		}
		using IS = std::make_index_sequence<L>;
	public:
		using result_t = typename base_t::result_t;
		/**
		 * @brief Construct a new XOrMore class.
		 * 
		 * @tparam Args The input argument types.
		 */
		template <typename ... Args>
		constexpr XOrMore(Args && ... pArgs) noexcept : base_t{ FWD(pArgs) ... } {}
		/**
		 * @brief Parse the string are determine whether it exists X or more times in the input array.
		 * 
		 * @tparam N The length of the input array.
		 * @param pInput The input array.
		 * @param index The index to start parsing.
		 * @param pOffset The offset from the index to process.
		 * @return ParseResult Ses ParseResult docs.
		 */
		template <std::size_t N>
		constexpr result_t operator()( std::array<char, N> const & pInput, std::size_t index = 0u, std::size_t pOffset = 0u ) const noexcept
		{
			auto		i{ index + pOffset };
			auto const	copy { i };

			while ( i < N )
			{
				if (IgnoreOnce(pInput, i, IS())) ++i;
				else break;
			}
			auto const length = i - copy;

			if constexpr (X == 0u)
				return { length };
			else
			{
				if (length >= X) return { length };
				else			 return { };
			}
		}
	};
	/**
	 * @brief Deduction guide for the XOrMore class.
	 * 
	 * @warning Drop the null termination, might be buggy if someone forwards an lvalue... someone please fix that possibility
	 * 
	 * @tparam N The length of the input c style array.
	 */
	template < std::size_t N >
	XOrMore(const char(&)[N])->XOrMore<N - 1u>;

	//Convience functions
	/**
	 * @brief A version of X or more where X = 1.
	 * 
	 * @tparam N The length of the input string.
	 * @return XOrMore<N - 1u, 1u> 
	 */
	template < std::size_t N > constexpr auto OneOrMore	(const char(&pArg)[N]) noexcept -> XOrMore<N - 1u, 1u> { return { pArg }; }
	/**
	 * @brief A version of X or more where X = 0.
	 * 
	 * @tparam N The length of the input string.
	 * @return XOrMore<N - 1u, 1u> 
	 */
	template < std::size_t N > constexpr auto ZeroOrMore(const char(&pArg)[N]) noexcept -> XOrMore<N - 1u, 0u> { return { pArg }; }
}