#pragma once
#include "Convert.hpp"
#include "ParseResult.hpp"
#include <type_traits>

namespace Parser
{
	/**
	 * @brief A passable type for the parser functions, this function can be passed as a type parameter in Commands.
	 * 
	 * @tparam T The type of the parameters.
	 */
	template <typename T>
	struct Param
	{
		static_assert(std::is_default_constructible_v<T>, "T must be default constructable otherwise a temporary cannot be used.");
		/**
		 * @brief Default construction is the only way to construct a Param.
		 */
		constexpr Param() = default;
		/**
		 * @brief Parse the input array and load the value of the string into the parameter if possible.
		 * 
		 * @tparam N The length of the input array.
		 * @param input The input array.
		 * @param index The index to start parsing.
		 * @param offset The offset from the start to process.
		 * @return ValueParseResult<T> The value parsed by the function, or a null result (failed to parse) see ValueParseResult docs.
		 */
		template <std::size_t N>
		constexpr ValueParseResult<T> operator()( std::array<char, N> const & input, std::size_t index = 0, std::size_t offset = 0 ) const noexcept
		{
			std::size_t length = 0;
			T temporary = {};

			auto constexpr is_bool_literal = std::is_same_v<bool, std::decay_t<T>>;

			//Switch is a hack to avoid very nested compile time expressions
			do
			{
				if constexpr (std::is_floating_point_v<T>)
				{
					length = General::atof<N, T>( input, temporary, index + offset );
					break;
				}
				if constexpr (std::is_unsigned_v<T> && !is_bool_literal)
				{
					length = General::atou<N, T>( input, temporary, index + offset );
					break;
				}
				if constexpr (std::is_signed_v<T> && !is_bool_literal)
				{
					length = General::atoi<N, T>( input, temporary, index + offset );
					break;
				}
				//For Boolean class definitions eg. OnOff, OkCancel ...
				if constexpr (General::is_boolean_v<T>)
				{
					length = temporary.FromString( input, (index + offset) );
					break;
				}
				if constexpr (is_bool_literal)
				{
					length = General::atob(input, temporary, General::MakeArray("true"), General::MakeArray("false"), (index + offset));
					if (length == 0)  length = General::atob(input, temporary, General::MakeArray("1"), General::MakeArray("0"), (index + offset));
					break;
				}
			} while(false);
			//
			if (length > 0u)	return ValueParseResult<T>(std::move(temporary), length);
			else				return ValueParseResult<T>{};
		}

		using result_t = ValueParseResult<T>;
	};
	template <typename T>
	struct IsParamHelper : std::conditional_t<General::is_boolean_v<T>, std::true_type, std::false_type> {};
	template <>	struct IsParamHelper<bool>					: std::true_type{};
	template <>	struct IsParamHelper<char>					: std::true_type{};
	template <>	struct IsParamHelper<short>					: std::true_type{};
	template <>	struct IsParamHelper<int>					: std::true_type{};
	template <>	struct IsParamHelper<long>					: std::true_type{};
	template <>	struct IsParamHelper<long long>				: std::true_type{};
	template <>	struct IsParamHelper<unsigned char>			: std::true_type{};
	template <>	struct IsParamHelper<unsigned short>		: std::true_type{};
	template <>	struct IsParamHelper<unsigned int>			: std::true_type{};
	template <>	struct IsParamHelper<unsigned long>			: std::true_type{};
	template <>	struct IsParamHelper<unsigned long long>	: std::true_type{};
	template <>	struct IsParamHelper<float>					: std::true_type{};
	template <>	struct IsParamHelper<double>				: std::true_type{};
	template <>	struct IsParamHelper<long double>			: std::true_type{};
	/**
	 * @brief Check whether a type is a Param type. 
	 * 
	 * @tparam T The type to check.
	 */
	template <typename T>
	constexpr auto IsParam_v = IsParamHelper<std::decay_t<T>>::value;
}