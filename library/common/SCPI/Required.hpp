#pragma once

#include "BasicBlock.hpp"
#include "Folding.hpp"
#include <array>

namespace Parser
{
	/**
	 * @brief A required block of text for parsing.
	 * 
	 * @tparam L The length of the required block of text. 
	 */
	template <std::size_t L>
	struct Required : BasicBlock<L>
	{
		using base_t = BasicBlock<L>;
		using base_t::base_t;
		using result_t = ParseResult;
		/**
		 * @brief Parse 
		 * 
		 * @tparam N The length of the input array.
		 * @param input The input array.
		 * @param index The index to start processing.
		 * @param offset The offset from the start to process.
		 * @return ParseResult See ParseResult docs.
		 */
		template <std::size_t N>
		constexpr ParseResult operator()( std::array<char, N> const & input, std::size_t index = 0, std::size_t offset = 0 ) const noexcept
		{
			if constexpr (L > 1)
				return Folding::Match(input.data(), N, base_t::m_Data.data(), L, index + offset);
			
			if constexpr (L == 1)
				if ( General::Equivalent( base_t::Data()[0], input[index + offset] ) )
					return { std::size_t(1) };
				
			return { false };
		}
	};
	/**
	 * @brief A deduction guide from a C-style string.
	 * 
	 * @warning The null terminaor will be stripped from the string, relying on the null terminator
	 *  is undefined behaviour for this library.
	 * @tparam N The length of the string.
	 */
	template < std::size_t N >
	Required(const char(&)[N])->Required<N - 1u>;
	/**
	 * @brief Check if a type is the required type.
	 * 
	 * @tparam T The type to check.
	 */
	template <typename T>
	constexpr bool IsRequired_v = false;
	template <unsigned L>
	constexpr bool IsRequired_v<Required<L>> = true;
}