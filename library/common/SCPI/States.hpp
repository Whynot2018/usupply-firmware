#pragma once
#include "ParseResult.hpp"
#include <tuple>
#include <type_traits>
#include <cstdint>

namespace Parser
{
	/**
	 * @brief A parser that allows ONE of a few differnt types to be parsed successfully.
	 * 
	 * The first type from the group of arguments that parses correctly is handled as the result.
	 * If you have multiple types with the same prefix, make sure you order them in a suitable manner.
	 * 
	 * @tparam Args The items that will be parsed (only one needs to be true for a successful parse)
	 */
	template <typename ... Args>
	class States : std::tuple<Args...>
	{
	private:
		using base_t = std::tuple<Args...>;
		/**
		 * @brief The number of items.
		 * 
		 */
		static constexpr auto count = sizeof...(Args);
		
		template <std::size_t ... I, std::size_t N>
		inline constexpr auto Run(std::index_sequence<I...>, std::array<char, N> const & input, std::size_t index ) const noexcept
		{
			std::size_t Length = index;
			(
				([&]( auto & base ) -> bool
				{
					if (auto result = base(input, index, 0))
					{
						index += result.Length();
						return true;
					}
					else 
					{
						return false;
					}
				}((std::get<I>((base_t const &)(*this)))))
				or 
				... 
			);
			return index - Length; //Returns the new position of index
		}
	public:
		using result_t = ParseResult;
		/**
		 * @brief Constuct a new States object.
		 * 
		 * @param args The parsers that will be checked.
		 */
		constexpr States(Args ... args) noexcept : 
			base_t( std::forward<Args>(args) ... )
		{}
		/**
		 * @brief Allow copy construction.
		 * 
		 */
		constexpr States(States const &) noexcept = default;
		/**
		 * @brief Parse the input array and determine whether any of the parsers match.
		 * 
		 * @tparam N The length of the input array.
		 * @param input The input array.
		 * @param index The index to start processing.
		 * @param offset The offset from the start to process.
		 * @return ParseResult See ParseResult docs. 
		 */
		template <std::size_t N>
		constexpr ParseResult operator () ( std::array<char, N> const & input, std::size_t index = 0u, std::size_t offset = 0u ) const noexcept
		{
			//Length and index are unsigned, Index is garenteed to be greater or equal to index 
			//because only + takes place and no negative numbers exist in that domain
			auto Length = Run(std::make_index_sequence<count>{}, input, index + offset);
			return ( Length ) ? result_t( Length ) : result_t(false);
		}
	};
	/**
	 * @brief A deduction guide that performs a selective decay of the arguments.
	 * 
	 * @tparam Args The items in the states class.
	 */
	template <typename ... Args>
	States(Args && ...)->States<General::decay_rvalue_t<Args>...>;
}