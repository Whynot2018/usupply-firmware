#pragma once
#include <cstdint>
#include <cstddef>
/**
 * @brief Forward a value
 * 
 */
#define FWD(x) std::forward<decltype(x)>(x)
/**
 * @brief Returns a unique uint8_t number for the application.
 * 
 * @warning If you use this more than 255 times, this WILL fail.
 * 
 * @return uint8_t The unique ID. 
 */
constexpr uint8_t UniqueID() noexcept
{
	return __COUNTER__;
}
//
#ifndef ALWAYS_INLINE
#define ALWAYS_INLINE inline __attribute__((always_inline))
#endif
//
#ifndef NEVER_INLINE
#define NEVER_INLINE __attribute__((noinline))
#endif
/**
 * @brief Defines a hardware breakpoint.
 */
#define Breakpoint() __asm__("BKPT")
/**
 * @brief Defines a manual hardawre breakpoint by looping over a volatile.
 * 
 * You can manually set the value of keep_looping_while_one in the debugger to continue.
 */
#define Breakpoints() { volatile int keep_looping_while_one = 1; while(keep_looping_while_one); }
/**
 * @brief Converts a parameter to a C-style string.
 */
#define STRINGIZE(x) #x
/**
 * @brief Suppresses.
 */
#define UNUSED(x) ((void)x)
/**
 * @brief Number of states to store in the debugging structures Monitor and LMonitor.
 */
#define STATE_COUNT 5
#if STATE_COUNT > 0
/**
 * @brief A class used to log a string.
 */
struct
{
	const char * states[STATE_COUNT];
	size_t states_index = 0;
} 
volatile inline StringMonitor;
//
#define LINE_POINT_STRING(line, file) file ":" STRINGIZE(line) 
/**
 * @brief Records the line and file in the StringMonitor
 */
#define LINE_POINT() do { if (StringMonitor.states_index < STATE_COUNT) StringMonitor.states[StringMonitor.states_index++] = LINE_POINT_STRING(__LINE__, __FILE__); } while(0)
/**
 * @brief Records a message in the StringMonitor.
 */
#define MSG_POINT(msg) []{ if (StringMonitor.states_index < STATE_COUNT) StringMonitor.states[StringMonitor.states_index++] = msg; return true; }()
/**
 * @brief Records a function name in the StringMonitor.
 */
#define FUNC_POINT() []{ if (StringMonitor.states_index < STATE_COUNT) StringMonitor.states[StringMonitor.states_index++] = __func__; }() 
/**
 * @brief A class used to store debuggin information.
 * 
 * This class stores a pointer to a string and a number.
 */
struct
{
	struct
	{
		const char * 	label;
		uint32_t 		number;
	} states[STATE_COUNT];
	size_t states_index = 0;
	/**
	 * @brief Pushes a single number and a string pointer onto the LogMonitor stack.
	 * 
	 * This command fails silently once the log is full.
	 * 
	 * @warning This does not request a critical section.
	 * @param number The number to push.
	 * @param label The label to push.
	 */
	inline void PushState(size_t number, const char * label) volatile noexcept 
	{
		if ( states_index < STATE_COUNT )
		{
			auto & state = states[ states_index++ ];
			state.number = number;
			state.label = label;
		}
	}
}
static volatile inline LogMonitor;
/**
 * @brief Records a number and the Line and file that the macro was called from into the LogMonitor.
 */
#define LOG_POINT(endpoint) LogMonitor.PushState(endpoint, LINE_POINT_STRING(__LINE__, __FILE__))
#endif