#pragma once
#include "Macros.hpp"
#include "MaskedField.hpp"
#include <cstdint>
#include <type_traits>
#include <new>

namespace General
{
	/**
	 * @brief A handle to a bitfield for a particular register at a defined address.
	 * 
	 * The recommended pattern for using this is through inheritance.
	 * This class has a size of zero, but members of classes always have a non-zero size.
	 * Inherited members benifit from empty base optimization and therefore are optimal.
	 * 
	 * @tparam T The register type.
	 * @tparam RegisterAddress The registers address.
	 * @tparam MaskField the mask that represents the bitfield.
	 * @tparam ZeroAlways the mask that ensures some register bits are ALWAYS zero.
	 * @tparam SetAlways The bit that indicates that some register bits should always be one.
	 * @tparam IsToggle This bit indiates that the bitfield is a toggle bit (only togglable)
	 */
	template <typename T, std::size_t RegisterAddress, T MaskField, T ZeroAlways = ~(T)0, T SetAlways = 0, bool IsToggle = false>
	class Bitfield
	{
	private:
		struct Range
		{
			T Start 	= (T)0;
			T Length 	= (T)0;
		};
		//
		//
		ALWAYS_INLINE static constexpr Range GetMaskRange() noexcept
		{
			T mask  	= MaskField;
			T start 	= 0u;
			T length 	= 0u;

			while ( ( mask & 1 ) == 0 )
			{
				++start;
				mask >>= 1;
			}
			while ( ( mask & 1 ) == 1 )
			{
				++length;
				mask >>= 1;
			}

			return {start, length};
		}
		//
		//
		static auto		constexpr	MaskRange  = GetMaskRange();
		static T		constexpr	MaskStart  = MaskRange.Start;
		static T		constexpr	MaskLength = MaskRange.Length;
		//
		//
		static constexpr T Mask  = ( MaskField) & ZeroAlways;
		static constexpr T NMask = (~MaskField) & ZeroAlways;
		//
		//
		inline static T volatile * const Addr = reinterpret_cast<T volatile * const>( RegisterAddress );
		//
		//
		ALWAYS_INLINE static auto ApplyMask( T input ) noexcept
		{
			return ((input << MaskStart) & Mask);
		}
		ALWAYS_INLINE static auto ApplyNMask( T input ) noexcept
		{
			return ((input << MaskStart) & NMask);
		}
	public:
		/**
		 * @brief Clears the bitfield
		 */
		ALWAYS_INLINE void Clear() noexcept
		{
			(*Addr) &= (NMask | SetAlways);
		}
		/**
		 * @brief Sets the value in the bitfield to the input value.
		 * 
		 * @param input The input.
		 */
		ALWAYS_INLINE void Set( T input ) noexcept
		{
			(*Addr) = (*Addr & NMask) | 
			(
				[=](T masked)
				{
					if constexpr ( IsToggle )
					{
						return (*Addr & ZeroAlways) ^ masked;
					}
					else
					{
						return masked;
					}
				} ( ApplyMask( input ) )
			) | SetAlways;
		}
		/**
		 * @brief Assign the masked value to the register.
		 * 
		 * @warning this function may have unintended side effects in the register.
		 * 
		 * This does not read-modify-write
		 * 	that pattern can be an isssue with some registers.
		 * 
		 * @param value 
		 * @return RawSet 
		 */
		ALWAYS_INLINE void RawSet( T value ) noexcept
		{
			( *Addr ) = ApplyMask( value );
		}
		/**
		 * @brief Returns the value stored in the bitfield.
		 * 
		 * @return T The value of the bitfield.
		 */
		ALWAYS_INLINE T Get() const noexcept
		{
			return ((*Addr) & MaskField) >> MaskStart;
		}
		/**
		 * @brief Implicitly converts the stored value.
		 * 
		 * @return T The stored value in the bitfield.
		 */
		ALWAYS_INLINE operator T() const noexcept
		{
			return Get();
		}
		/**
		 * @brief Assigns a value to the bitfield.
		 * 
		 * @param input The value to assign to the bitfield.
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE Bitfield & operator = ( T input ) noexcept
		{
			Set( input );
			return *this;
		}
		/**
		 * @brief Bitwise ors the bitfield with the input.
		 * 
		 * @param input The input.
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE Bitfield & operator|=( T input ) noexcept
		{
			(*Addr) |= (ApplyMask(input) | SetAlways);
			return *this;
		}
		/**
		 * @brief Performs a bitwise and with the register.
		 * 
		 * @param input The input.
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE Bitfield & operator&=( T input ) noexcept
		{
			(*Addr) &= (ApplyMask(input) | NMask | SetAlways);
			return *this;
		}
	};
	/**
	 * @brief This is a handle for a raw register.
	 * 
	 * This class has no data members and therefore construction is free.
	 * 
	 * @tparam T The type of the register (ie uint32_t)
	 * @tparam RegisterAddress The address of the register
	 */
	template <typename T, std::size_t RegisterAddress>
	struct RawRegister
	{
		/**
		 * @brief The only way to construct a RawRegister is through default construction
		 * 
		 */
		constexpr RawRegister() = default;
		/**
		 * @brief A pointer to the register
		 */
		inline static T volatile * const Addr = reinterpret_cast<T volatile * const>(RegisterAddress);
		/**
		 * @brief Assign a value to the register
		 * 
		 * @param input The value to assign to the register
		 */
		ALWAYS_INLINE void Set( T input ) noexcept
		{
			(*Addr) = input;
		}
		/**
		 * @brief Returns a reference to this.
		 * 
		 * @return A reference to this class.
		 */
		ALWAYS_INLINE RawRegister & Access() noexcept
		{
			return *this;
		}
		/**
		 * @brief Assigns a value to the register.
		 * 
		 * @param input The value to assign
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator = ( T input ) noexcept
		{
			(*Addr) = input;
			return *this;
		}
		/**
		 * @brief Performs a modulo operation with the register.
		 * 
		 * @param input 
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator %= (T const & input) noexcept
		{
			(*Addr) %= input;
			return *this;
		}
		/**
		 * @brief Performs a XOR operation with the register.
		 * 
		 * @param input 
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator ^= (T const & input) noexcept
		{
			(*Addr) ^= input;
			return *this;
		}
		/**
		 * @brief Performs a multiplcation operation with the register.
		 * 
		 * @param input 
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator *= (T const & input) noexcept
		{
			(*Addr) *= input;
			return *this;
		}
		/**
		 * @brief Performs a division operation with the register.
		 * 
		 * @param input 
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator /= (T const & input) noexcept
		{
			(*Addr) /= input;
			return *this;
		}
		/**
		 * @brief Performs a addition operation with the register.
		 * 
		 * @param input 
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator += (T const & input) noexcept
		{
			(*Addr) += input;
			return *this;
		}
		/**
		 * @brief Performs a subtraction operation with the register.
		 * 
		 * @param input 
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator -= (T const & input) noexcept
		{
			(*Addr) -= input;
			return *this;
		}
		/**
		 * @brief Performs an or operation with the register.
		 * 
		 * @param input 
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator |= (T const & input) noexcept
		{
			(*Addr) |= input;
			return *this;
		}
		/**
		 * @brief Performs an and operation with the register.
		 * 
		 * @param input 
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator &= (T const & input) noexcept
		{
			(*Addr) &= input;
			return *this;
		}
		/**
		 * @brief Performs a left bitshft operation with the register.
		 * 
		 * @param input 
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator <<= (T const & input) noexcept
		{
			(*Addr) <<= input;
			return *this;
		}
		/**
		 * @brief Performs a right bitshift operation with the register.
		 * 
		 * @param input 
		 * @return Bitfield & for chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator >>= (T const & input) noexcept
		{
			(*Addr) >>= input;
			return *this;
		}
		/**
		 * @brief Gets the bitwise not of the value in the register.
		 * 
		 * @return T The bitwise not of the value in the register
		 */
		ALWAYS_INLINE T operator ~ () const noexcept
		{
			return ~(*Addr);
		}
		/**
		 * @brief Get the negative value of the register
		 * 
		 * @return T -Get()
		 */
		ALWAYS_INLINE T operator - () const noexcept
		{
			return -(*Addr);
		}
		/**
		 * @brief Performs the + operation on the register
		 * 
		 * @return T +Get()
		 */
		ALWAYS_INLINE T operator + () const noexcept
		{
			return +(*Addr);
		}
		/**
		 * @brief Performs XOR on the value in the register
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE T operator ^ (T const & input) const noexcept
		{
			return (*Addr) ^ input;
		}
		/**
		 * @brief Performs modulo on the value in the register
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE T operator % (T const & input) const noexcept
		{
			return (*Addr) % input;
		}
		/**
		 * @brief Performs AND on the value in the register
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE T operator & (T const & input) const noexcept
		{
			return (*Addr) & input;
		}
		/**
		 * @brief Performs OR on the value in the register
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE T operator | (T const & input) const noexcept
		{
			return (*Addr) & input;
		}
		/**
		 * @brief Performs + on the value in the register
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE T operator + (T const & input) const noexcept
		{
			return (*Addr) + input;
		}
		/**
		 * @brief Performs - on the value in the register
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE T operator - (T const & input) const noexcept
		{
			return (*Addr) - input;
		}
		/**
		 * @brief Performs * on the value in the register
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE T operator * (T const & input) const noexcept
		{
			return (*Addr) * input;
		}
		/**
		 * @brief Performs / on the value in the register
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE T operator / (T const & input) const noexcept
		{
			return (*Addr) / input;
		}
		/**
		 * @brief Performs left bitshift on the value in the register
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE T operator <<	(T const & input) const noexcept
		{
			return (*Addr) << input;
		}
		/**
		 * @brief Performs right bitshift on the value in the register
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE T operator >>	(T const & input) const noexcept
		{
			return (*Addr) >> input;
		}
	
		//Operator overloads : Logical
		/**
		 * @brief Performs logical not on the value in the register
		 * 
		 * @return true The regster is zero.
		 * @return false The register is not zero.
		 */
		ALWAYS_INLINE bool operator ! () const noexcept
		{
			return ! (*Addr);
		}
		/**
		 * @brief Performs logical and on the value in the register
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE bool operator && (T const & input) const noexcept
		{
			return (*Addr) && input;
		}
		/**
		 * @brief Performs logical or on the value in the register
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE bool operator || (T const & input) const noexcept
		{
			return (*Addr) || input;
		}
		
		//Operator overloads : Comparison
		/**
		 * @brief Test whether the value in the register is equal to input.
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE bool operator == (T const & input) const noexcept
		{
			return (*Addr) == input;
		}
		/**
		 * @brief Test whether the value in the register is not equal to input.
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE bool operator != (T const & input) const noexcept
		{
			return (*Addr) != input;
		}
		/**
		 * @brief Test whether the value in the register less than to input.
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE bool operator < (T const & input) const noexcept
		{
			return (*Addr) < input;
		}
		/**
		 * @brief Test whether the value in the register greater than to input.
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE bool operator > (T const & input) const noexcept
		{
			return (*Addr) > input;
		}
		/**
		 * @brief Test whether the value in the register less than or equal to input.
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE bool operator <= (T const & input) const noexcept
		{
			return (*Addr) <= input;
		}
		/**
		 * @brief Test whether the value in the register is greater than or equal to input.
		 * 
		 * @param input RHS of the operation.
		 * @return Result of the operation.
		 */
		ALWAYS_INLINE bool operator >= (T const & input) const noexcept
		{
			return (*Addr) >= input;
		}

		//Operator overloads : Member access
		/**
		 * @brief Decrement the value in the register.
		 * 
		 * @return RawRegister & For chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator--() noexcept
		{
			--(*Addr);
			return *this;
		}
		/**
		 * @brief Increment the value in the register.
		 * 
		 * @return RawRegister & For chained operations.
		 */
		ALWAYS_INLINE RawRegister & operator++() noexcept
		{
			++(*Addr);
			return *this;
		}
		/**
		 * @brief Increment the value in the register.
		 * 
		 * @return T The value before the increment.
		 */
		ALWAYS_INLINE T operator++(int) noexcept
		{
			auto out = (*Addr);
			++(*Addr);
			return out;
		}
		/**
		 * @brief Decrement the value in the register.
		 * 
		 * @return T The value before the decrement.
		 */
		ALWAYS_INLINE T operator--(int) noexcept
		{
			auto out = (*Addr);
			--(*Addr);
			return out;
		}
		/**
		 * @brief Get the value in the register
		 * 
		 * @return T The value in the register
		 */
		ALWAYS_INLINE constexpr T Get() const noexcept
		{
			return (*Addr);
		}
		/**
		 * @brief Get a bitfield from within the register
		 * 
		 * @tparam Mask The mask to apply to the register
		 * @tparam ZeroAlways The bits to ensure are always zero.
		 * @tparam SetAlways The bits to ensure are always set.
		 * @tparam IsToggle Whether or not the bitfield values are toggle only fields.
		 * @return Bitfield<T, RegisterAddress, Mask, ZeroAlways, SetAlways, IsToggle> The requested bitfield
		 */
		template <T Mask, T ZeroAlways = ~(T)0, T SetAlways = 0, bool IsToggle = false>
		ALWAYS_INLINE Bitfield<T, RegisterAddress, Mask, ZeroAlways, SetAlways, IsToggle> Actual() const noexcept
		{
			return {};
		}
	};
	/**
	 * @brief A 32bit register.
	 * 
	 * @tparam Address The address of the register
	 */
	template <std::size_t Address>
	using u32_reg = General::RawRegister<std::uint32_t, Address>;
	/**
	 * @brief A 16 bit register
	 * 
	 * @tparam Address The address of the register
	 */
	template <std::size_t Address>
	using u16_reg = General::RawRegister<std::uint16_t, Address>;
}