#pragma once
#include "MenuItem.hpp"
#include "Meta.hpp"
#include "Math.hpp"
#include <type_traits>

namespace Menu
{
	/**
	 * @brief A kernal that enables numerical inputs (numbers) in a RawInput.
	 * 
	 * @tparam Selectable_v Whether or not the number can be selected.
	 */
	template <bool Selectable_v = false>
	struct NumberMatchKernal
	{
		/**
		 * @brief Check whether the keycode is relevant to the number input.
		 * 
		 * @param code The keycode to check.
		 * @return true A relevant input was detected.
		 * @return false Not a relevant input.
		 */
		static constexpr bool Match( KeyCodes code ) noexcept
		{
			return IsKeycodeNumber( code );
		}
		/**
		 * @brief Translates a keycode to a decimal number input.
		 * 
		 * 
		 * @note This function always succeeds, so be careful to use Match first.
		 * @param code The keycode to convert.
		 * @return std::uint8_t The resulting number, or zero if the keycode is not supported.
		 */
		static constexpr std::uint8_t Translate( KeyCodes code ) noexcept
		{
			auto const keycode   = (std::uint32_t)code;
			const bool is_num    = General::Between( keycode, (std::uint8_t)KeyCodes::Num0, (std::uint8_t)KeyCodes::Num9 );
			const bool is_numpad = General::Between( keycode, (std::uint8_t)KeyCodes::Numpad0, (std::uint8_t)KeyCodes::Numpad9 );
			if (is_num) 
				return keycode - (std::uint8_t)KeyCodes::Num0;
			if (is_numpad)
				return keycode - (std::uint8_t)KeyCodes::Numpad0;
			return 0;
		}
		/**
		 * @brief Whether or not the number input can be selected.
		 */
		static constexpr bool Selectable = Selectable_v;
	};
	/**
	 * @brief Check whether a specific key was pressed.
	 * 
	 * @tparam Code The code to check against.
	 * @tparam Selectable_v Whether the input is selectable.
	 */
	template <KeyCodes Code, bool Selectable_v = false>
	struct KeyMatchKernal
	{
		/**
		 * @brief Check whether the key was pressed.
		 * 
		 * @param code The code to check against.
		 * @return bool Code == code
		 */
		static constexpr bool Match( KeyCodes code ) noexcept
		{
			return Code == code;
		}
		/**
		 * @brief Returns the input, this function is for interface consistancy.
		 * 
		 * @param code The code to return.
		 * @return KeyCodes code. 
		 */
		static constexpr KeyCodes Translate( KeyCodes code ) noexcept
		{
			return code;
		}
		/**
		 * @brief Whether the input is selectable.
		 */
		static constexpr bool Selectable = Selectable_v;
	};
	/**
	 * @brief Converts one kernal to one that renders nothing but accepts the keycodes.
	 * 
	 * @tparam BufferSize The render target length.
	 * @tparam Match_t The kernal to check with.
	 */
	template <unsigned BufferSize, typename Match_t>
	struct BlankHotkey : public BlankMenuItem<BufferSize>
	{
		/**
		 * @brief Construct a new Blank Hotkey object
		 * 
		 * @param kernal Used in deductino guide to determine the render target length.
		 * @param match Used for deduction guide, for determination of the Matcher.
		 */
		BlankHotkey( General::SizeT<BufferSize> kernal, Match_t match ) noexcept {}
		/**
		 * @brief Runs Match_t::Match, see docs for the underlying type of Match_t.
		 * 
		 * @param code The keycode to check.
		 * @return true Match occured.
		 * @return false Match didn't occur.
		 */
		static constexpr bool Match( KeyCodes code ) noexcept
		{
			return Match_t::Match( code );
		}
		/**
		 * @brief Runs Match_t::Match, see docs for the underlying type of Match_t.
		 * 
		 * @param code The keycode to check.
		 * @return true Match occured.
		 * @return false Match didn't occur.
		 */
		static constexpr auto Translate( KeyCodes code ) noexcept
		{
			return Match_t::Translate( code );
		}
		/**
		 * @brief Does nothing, used for interface consistancy.
		 */
		void Execute(KeyCodes) const noexcept {};
		/**
		 * @brief BlankHotKeys are not executable.
		 */
		static constexpr bool Executable = false;
		/**
		 * @brief BlankHotKeys are not selectable.
		 */
		static constexpr bool Selectable = false;
	};
	/**
	 * @brief Used to create menus with input sensitve contexts.
	 * 
	 * For example:
	 * 
	 * If you were to load some presets from memory and the way to load would be:
	 * 1. Hold presets
	 * 2. Press the number to load (0 to 9)
	 * 
	 * This would be achieved like this:
	 * 
	 * BasicHotkey
	 * (
	 *      KeyMatchKernal<KeyCodes::Presets, true>{}, //The true here means selectable.
	 *      Shortcut
	 * 		(
	 *  	    s, 								// This passes in the information about the render target.
	 * 			Label(s, MakeArray("LoAd")), 	// The value to render after the hotkey is detected.
	 *  		ActionHotkey					// This type runs a callback when an its kernal detects a match.
	 * 			(
	 * 				BlankHotkey( s, NumberMatchKernal{} ),	// After presets the next valid input is a number
	 * 				[&](auto v)
	 * 				{
	 * 					// This is run when the number is pressed, 
	 * 					// 	the parameter v is the number pressed.
	 *				}
	 *			)
	 * 		)
	 * )
	 * 
	 * @tparam T_t The type of the underlying kernal.
	 * @tparam Match_t The type of the matcher.
	 */
	template <typename T_t, typename Match_t>
	struct BasicHotkey : public T_t
	{
		/**
		 * @brief Construct a new Basic Hotkey object
		 * 
		 * @param item A rvalue reference to a T_t object.
		 */
		BasicHotkey( T_t&& item ) noexcept: T_t( std::move( item ) ) {}
		/**
		 * @brief Construct a new Basic Hotkey object
		 * 
		 * @tparam Match_t The type of the matcher.
		 * @param item A rvalue reference to a T_t object.
		 */
		BasicHotkey( Match_t, T_t&& item ) noexcept: BasicHotkey( std::move( item ) ) {}
		/**
		 * @brief Use Match_t's matcher, see its docs for its behaviour.
		 * 
		 * @param code The keycode to test.
		 * @return true Match occured.
		 * @return false No match occured.
		 */
		static constexpr bool Match( KeyCodes code ) noexcept
		{
			return Match_t::Match( code );
		}
		/**
		 * @brief Translate keycode using the Match_t's translate function, see its docs for its behaviour.
		 * 
		 * @param code The code to check.
		 * @return auto The type returned by Match_t::Translate(code).
		 */
		static constexpr auto Translate( KeyCodes code ) noexcept
		{
			return Match_t::Translate( code );
		}
		/**
		 * @brief Does nothing, used for interface consistancy.
		 */
		void Execute(KeyCodes) const noexcept {};
		/**
		 * @brief This is not an executable hotkey type.
		 */
		static constexpr bool Executable = false;
		/**
		 * @brief The selectability behaviour is inherited from Match_t's behaviour.
		 */
		static constexpr bool Selectable = Match_t::Selectable;
	};
	/**
	 * @brief A hotkey that is executable (is run when a match occurs) and runs a callback when matched.
	 * 
	 * @tparam Parent 
	 * @tparam Action 
	 */
	template <typename Parent, typename Action>
	class ActionHotkey : public Parent
	{
	private:
		Action m_Action;
	public:
		/**
		 * @brief Construct a new Action Hotkey object
		 * 
		 * @param parent The parent.
		 * @param action The action to run when the hotkey is detected.
		 */
		ActionHotkey( Parent&& parent, Action&& action ) noexcept:
			Parent( std::move( parent ) ),
			m_Action( std::move( action ) ) 
		{}
		/**
		 * @brief Translate keycode using the Parent's translate function, see its docs for its behaviour.
		 * 
		 * @param code The code to check.
		 * @return auto The type returned by Parent::Translate(code).
		 */
		static constexpr auto Translate( KeyCodes code ) noexcept
		{
			return Parent::Translate( code );
		}
		/**
		 * @brief This is an executable hotkey, it will run when this is called.
		 * 
		 * @param input The input keycode.
		 * @return auto The return type of the callback.
		 */
		auto Execute( KeyCodes input ) const noexcept
		{
			return m_Action( Translate( input ) );
		}
		/**
		 * @brief The ActionHotKey is always executable.
		 */
		static constexpr bool Executable = true;
		/**
		 * @brief The selectability behaviour is inherited from Parent.
		 */
		static constexpr bool Selectable = Parent::Selectable;
	};

	//Deduction guides
	template < unsigned BufferSize, typename Match_t>
	BlankHotkey( General::SizeT<BufferSize>, Match_t )->BlankHotkey<BufferSize, Match_t>;

	template <typename Match_t, typename T_t>
	BasicHotkey( Match_t, T_t )->BasicHotkey<T_t, Match_t>;

	template <typename Parent, typename Action>
	ActionHotkey( Parent, Action )->ActionHotkey<Parent, Action>;
}