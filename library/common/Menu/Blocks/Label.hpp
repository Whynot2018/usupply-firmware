#pragma once

#include "MenuItem.hpp"

namespace Menu
{
	/**
	 * @brief A menu item that is just a string.
	 * 
	 * @tparam BufferSize The render target length.
	 * @tparam N The number of characters in the label.
	 */
	template <unsigned BufferSize, unsigned N>
	class Label : public RawMenuItem<BufferSize>
	{
	public:
		using base_t   = RawMenuItem<BufferSize>;
		using buffer_t = typename base_t::buffer_t;
		static auto constexpr min_N = ( N > BufferSize ) ? BufferSize : N;
		using array_t               = std::array<char, N>;
		using min_array_t           = std::array<char, min_N>;
	private:
		array_t const m_Label;
	public:
		/**
		 * @brief Construct a new Label object
		 * 
		 * @param label The array that has the labels characters.
		 */
		Label( array_t const& label ) noexcept :
		    m_Label{ label }
		{}
		/**
		 * @brief Construct a new Label object
		 * 
		 * @param args The arguments used to initalise std::array of characters.
		 * @param label The array that has the labels characters.
		 */
		Label( General::SizeT<BufferSize>, array_t const& label ) noexcept :
		    Label{ label }
		{}
		/**
		 * @brief Renders the label into the render target array.
		 * 
		 * @param output The render target array.
		 * @param use_cursor Never used.
		 * @return unsigned The number of characters rendered.
		 */
		virtual unsigned Render( buffer_t& output, bool use_cursor ) noexcept
		{
			UNUSED(use_cursor);
			(min_array_t&)output = (min_array_t&)m_Label;
			return min_N;
		}
	};

	template <unsigned BufferSize, unsigned N>
	Label( General::SizeT<BufferSize>, std::array<char, N> const& pInput ) -> Label<BufferSize, N>;
}