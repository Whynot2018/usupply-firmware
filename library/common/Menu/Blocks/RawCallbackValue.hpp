#pragma once

#include "Boolean.hpp"
#include "MenuItem.hpp"
#include "Time.hpp"

namespace Menu
{
	/**
	 * @brief A raw value menu item that displays a value returned from a callback.
	 * 
	 * @tparam BufferSize The number of characters in the render target.
	 * @tparam T The type of the input.
	 */
	template <unsigned BufferSize, typename T>
	class RawCallbackValue : public RawMenuItem<BufferSize>
	{
	private:
		using buffer_t = typename RawMenuItem<BufferSize>::buffer_t;
		T m_Callback;
	public:
		/**
		 * @brief Construct a new Raw Callback Value object
		 * 
		 * @param callback The callback that is used to retrieve the value to be rendered.
		 */
		RawCallbackValue( T && callback ) : 
			m_Callback( std::forward<T>( callback ) ) 
		{}
		/**
		 * @brief Construct a new Raw Callback Value object
		 * 
		 * @tparam SizeT<BufferSize> Used for deduction of the classes buffer size.
		 * @param callback The callback that is used to retrieve the value to be rendered.
		 */
		RawCallbackValue( General::SizeT<BufferSize>, T && callback ) : 
			RawCallbackValue( std::forward<T>( callback ) ) 
		{}
		/**
		 * @brief Renders the value returned by the callback into the render target.
		 * 
		 * @param output The render target.
		 * @param use_cursor Whether or not to use cursors (Unused in this function).
		 * @return unsigned The number of characters inserted in the render target.
		 */
		virtual unsigned Render( buffer_t & output, bool use_cursor ) noexcept
		{
			UNUSED(use_cursor);
			//
			auto const value = m_Callback();
			using type = std::decay_t<decltype(value)>;

			if constexpr ( std::is_floating_point_v<type> )
				return General::fixed_ftoa<BufferSize>( value, output );

			if constexpr ( std::is_unsigned_v<type> )
				return General::fixed_utoa<BufferSize>( value, output );

			if constexpr ( std::is_integral_v<type> )
				return General::fixed_itoa<BufferSize>( value, output );

			if constexpr ( std::is_same_v<std::decay_t<type>, bool> )
				return General::fixed_btoa<BufferSize>( value, output );

			if constexpr ( General::is_boolean_v<type> )
				return value.ToString( output );

			if constexpr ( General::IsTime_v< std::decay_t<decltype(value)> > )
				return value.ToString( output );

			return 0u;
		}
	};

	template <unsigned BufferSize, typename T>
	RawCallbackValue(General::SizeT<BufferSize>, T) -> RawCallbackValue<BufferSize, T>;
}