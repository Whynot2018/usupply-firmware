#pragma once
#include "Label.hpp"
#include "MenuItem.hpp"

namespace Menu
{
	/**
	 * @brief A menu item that when selected runs a callback.
	 * 
	 * @tparam BufferSize The render target buffer size.
	 * @tparam N The length of the actions label string.
	 * @tparam action_function_t The callback function type.
	 */
	template <unsigned BufferSize, unsigned N, typename action_function_t>
	class Action : public RawMenuItem<BufferSize>
	{
	private:
		using base_t   = RawMenuItem<BufferSize>;
		using buffer_t = typename base_t::buffer_t;
		/**
		 * @brief The label displayed on the screen.
		 */
		Label<BufferSize, N> m_Label;
		/**
		 * @brief The function to call when the action is triggered
		 */
		action_function_t    m_Function;
	public:
		/**
		 * @brief Construct a new Action object
		 * 
		 * @param label The character array that containers the label string (not null terminated).
		 * @param function The function to call when the action is triggered.
		 */
		Action( std::array<char, N> label, action_function_t && function ) noexcept :
		    m_Label( label ),
		    m_Function( std::move(function) )
		{}
		/**
		 * @brief Construct a new Action object
		 * 
		 * This function is used for class template type deduction, it simply calls the other consturctor.
		 * 
		 * @tparam Args The arguments to initialise the Action class, see other constructor.
		 * @param args The arguments to forward to the standard constructor.
		 */
		template <typename... Args>
		Action( General::SizeT<BufferSize>, Args && ... args ) noexcept :
		    Action( std::forward<Args>( args )... )
		{}
		/**
		 * @brief Actions are triggered by the enter keycode. 
		 * 
		 * This triggers the callback if enter is pressed.
		 * 
		 * @param input The keycode to process.
		 * @return NavigateReturn Whether the KeyCode is handled or not.
		 */
		virtual NavigateReturn Navigate( KeyCodes input ) noexcept
		{
			if ( input == KeyCodes::Enter )
			{
				m_Function();
				return Handled;
			}
			return Unhandled;
		}
		/**
		 * @brief Renders the label of the action item into the output buffer.
		 * 
		 * @param output The render target.
		 * @param use_cursor Whether or not a cursor is allowed. 
		 * @return unsigned The number of characters rendered.
		 */
		virtual unsigned Render( buffer_t & output, bool use_cursor ) noexcept
		{
			return m_Label.Render( output, use_cursor );
		}
	};

	template <unsigned BufferSize, unsigned N, typename action_function_t>
	Action( General::SizeT<BufferSize>, std::array<char, N>, action_function_t )->Action<BufferSize, N, action_function_t>;
}