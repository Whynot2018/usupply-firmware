#pragma once

#include "Hotkey.hpp"
#include "Label.hpp"
#include <tuple>

namespace Menu
{
	/**
	 * @brief This is a menu item that once a trigger is detected a certain menu item is displayed.
	 * 
	 * @tparam BufferSize The render target length (number of characters).
	 * @tparam N The size of the default label.
	 * @tparam Items The types of the items in the shortcut.
	 */
	template <unsigned BufferSize, unsigned N, typename... Items>
	class Shortcut : public SelectableMenuItem<BufferSize>
	{
	private:
		using base_t  = SelectableMenuItem<BufferSize>;
		using tuple_t = std::tuple<Items...>;
		using buffer_t = typename base_t::buffer_t;

		Label<BufferSize, N> 	m_Label;
		tuple_t  				m_Items;
		unsigned 				m_Index = N;

		template <unsigned I>
		decltype( auto ) Get() noexcept
		{
			return std::get<I>( m_Items );
		}

		template <unsigned I = sizeof...( Items ) - 1>
		RawMenuItem<BufferSize> & AtIndex( unsigned Index ) noexcept
		{
			if constexpr ( I == 0 )
				return Get<I>();
			else
				return ( Index == I ) ? Get<I>() : AtIndex<I - 1>( Index );
		}
		RawMenuItem<BufferSize> & Item() noexcept { return AtIndex( m_Index ); }

	public:
		/**
		 * @brief Construct a new Shortcut object
		 * 
		 * @param label The menu item label class used as the default menu item that is rendered (when no shortcut has been pressed.) 
		 * @param items The different menu items in the menu.
		 */
		Shortcut( Label<BufferSize, N> && label, Items &&... items ) noexcept:
			m_Label( std::move( label ) ),
			m_Items{ std::move( items )...} 
		{}
		/**
		 * @brief Construct a new Shortcut object
		 * 
		 * This is only used for template class argument deduction.
		 * See docs for other constructor, this one calls the other construction.
		 * 
		 * @param label The menu item label class used as the default menu item that is rendered (when no shortcut has been pressed.) 
		 * @param items The different menu items in the menu.
		 */
		Shortcut( General::SizeT<BufferSize>, Label<BufferSize, N> && label, Items &&... items ) noexcept:
			Shortcut( std::move(label), std::move( items )... )
		{}
		/**
		 * @brief Performs navigation when the control is selected.
		 * 
		 * @param input The keycode to process.
		 * @return NavigateReturn The result of the navigation, see NavigateReturn docs.
		 */
		virtual NavigateReturn SelectedNavigate( KeyCodes input ) noexcept
		{
			if ( auto v = Item().Navigate( input ); (v != Unhandled) )
				return v;
			else if ( IsCancel( input ) )
			{
				base_t::Deselect();
				return Finished;
			}
			return Unhandled;
		}
		/**
		 * @brief Performs navigation when the control is not selected.
		 * 
		 * @param input The keycode to process.
		 * @return NavigateReturn The result of the navigation, see NavigateReturn docs.
		 */
		virtual NavigateReturn DeselectedNavigate( KeyCodes input ) noexcept
		{
			NavigateReturn Match = Unhandled;
			General::ForEachTuple( m_Items, [&]( auto & item, unsigned index )
			{
				using type = std::decay_t<decltype(item)>;
				//
				if ( item.Match( input ) )
				{
					if constexpr (type::Executable)
					{
						(index == index); //Suppress unused warning.
						item.Execute(input);
					}
					if constexpr (type::Selectable)
					{
						m_Index = index;
						Match = Handled;
						base_t::Select();
					}
					else Match = Finished;
				}
			});
			return Match;
		}
		/**
		 * @brief The rendering function when the control is selected.
		 * 
		 * @param output The render target.
		 * @param use_cursor Whether or not to use cursors.
		 * @return unsigned The number of characters rendered.
		 */
		virtual unsigned SelectedRender( buffer_t & output, bool use_cursor ) noexcept
		{
			return Item().Render( output, use_cursor );
		}
		/**
		 * @brief The rendering function when the control is not selected.
		 * 
		 * @param output The render target.
		 * @param use_cursor Whether or not to use cursors.
		 * @return unsigned The number of characters rendered.
		 */
		virtual unsigned DeselectedRender( buffer_t & output, bool use_cursor ) noexcept
		{
			return m_Label.Render( output, use_cursor );
		}
	};
	/**
	 * @brief A template deduction guide for the ShortCut class.
	 * 
	 * @tparam BufferSize Render target length (number of characters).
	 * @tparam N The length of the label.
	 * @tparam Args The types of the items in the shortcut.
	 */
	template <unsigned BufferSize, unsigned N, typename... Args>
	Shortcut( General::SizeT<BufferSize>, Label<BufferSize, N>, Args&&... )->Shortcut<BufferSize, N, std::decay_t<Args>...>;
}