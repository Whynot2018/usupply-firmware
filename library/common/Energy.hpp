#pragma once

#include "Math.hpp"

namespace General
{
	/**
	 * @brief Manages the integration of power and calculates energy.
	 * 
	 * @tparam T The underlying numerical type used by this class.
	 */
	template <typename T>
	class Energy
	{
	private:
		T m_Count = 0;
		T m_Accumulation = 0;
		T m_Value = 0, m_Wh = 0;
	public:
		/**
		 * @brief Clears count and accumulate
		 */
		void Reset() noexcept
		{
			m_Count = 0, m_Accumulation = 0, m_Value = 0, m_Wh = 0; 	
		}		
		/**
		 * @brief Samples a power value, accumulates it
		 * 
		 * @param power The power to sample and update the structure.
		 */
		void Sample(T power) noexcept
		{
			m_Accumulation += power;
			++m_Count;
		}
		/**
		 * @brief Updates the values in the energy object with a time slice.
		 * 
		 * @param duration time passed since last call of the update function
		 */
		void Update(T duration) noexcept
		{
			m_Value += ( (T)m_Accumulation / (T)m_Count ) * (T)duration;
			m_Wh = m_Value / (T)3600;
			m_Count = 0;
			m_Accumulation = 0;
		}
		/**
		 * @brief Returns the accumulated power as per the last update
		 * 
		 * @return T The joules accumulated since last reset or the inital construction.
		 */
		T Joules() const noexcept
		{
			return m_Value;
		}
		/**
		 * @brief Gets the watt hours.
		 * 
		 * @return T the number of watt hours the device has used.
		 */
		T WattHours() const noexcept
		{
			return m_Wh;
		}
	};
}