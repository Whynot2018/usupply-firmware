#pragma once

namespace General
{
	/**
	 * @brief A class that manages interpolation and extrapolation of a line.
	 */
	class Line
	{
	protected:
		float const m, b;
	public:
		constexpr Line & operator=(Line &&) = default;
		constexpr Line & operator=(Line const &) = default;
		constexpr Line(Line &&) noexcept = default;
		constexpr Line(Line const &) noexcept = default;
		/**
		 * @brief Construct a line from two pairs of points.
		 * 
		 * @param x1 (x1, ) The x component of the first point.
		 * @param y1 (, y1) The y component of the first point.
		 * @param x2 (x2, ) The x component of the second point.
		 * @param y2 (, y2) The y component of the second point.
		 */
		constexpr Line( float x1, float y1, float x2, float y2 ) noexcept :
		    m{ ( y2 - y1 ) / ( x2 - x1 ) },
		    b{ y1 - m * x1 }
		{}
		/**
		 * @brief Takes an x value and returns the y value.
		 * 
		 * @param x the x value of the line.
		 * @return float the result of the interpolated or extrapolated line. 
		 */
		constexpr float operator()( float x ) const noexcept
		{
			return m * x + b;
		}
	};
}
