#pragma once
#include "Macros.hpp"
#include <array>
#include <cstddef>
#include <optional>
#include <iterator>
#include <functional>
#include <algorithm>
#include <atomic>

namespace Containers
{
	/**
	 * @brief A generic "view" into contigious data.
	 * 
	 * @tparam T The type of the elements.
	 * @tparam ReadOnly Whether or not the view is read only.
	 */
	template <typename T, bool ReadOnly = true>
	class DataView
	{
	public:
		//
		// types
		using value_type                = T;
		using pointer                   = T *;
		using const_pointer             = const T *;
		using reference                 = T &;
		using const_reference           = const T &;
		using iterator                  = T *;
		using const_iterator            = T const *;
		using const_reverse_iterator    = std::reverse_iterator<const_iterator>;
		using reverse_iterator          = const_reverse_iterator;
		using size_type                 = size_t;
		using difference_type           = ptrdiff_t;
		static constexpr size_type npos = size_type(-1);
		//
		template<std::size_t N>
		using carray_t = T[N];
	private:
		using T_RW = std::conditional_t<ReadOnly, T const, T>;
		//
		T_RW *		m_Data{ nullptr };
		size_type	m_Length{ 0u };
	public:
		/**
		 * @brief Construct a null dataview. 
		 */
		constexpr DataView() = default;
		/**
		 * @brief Copy a dataview into another.
		 * 
		 */
		constexpr DataView(DataView const &) noexcept = default;
		/**
		 * @brief Assign a dataview to another.
		 * 
		 * @return DataView& 
		 */
		DataView & operator=(DataView const &) noexcept = default;
		/**
		 * @brief Construct a dataview.
		 * 
		 * @param data A pointer to contigious data without padding.
		 * @param length The number of contigious items that proceed the first data item (including the first item).
		 */
		ALWAYS_INLINE constexpr DataView( T_RW * data, size_type length ) noexcept : 
			m_Data  { data },
			m_Length{ length }
		{}
		/**
		 * @brief construct a dataview from a container that supports std::data and std::size
		 * 
		 * @tparam A The container type.
		 * @tparam C The container type with the correct const as per the ReadOnly parameter.
		 * @param data The container.
		 */
		template <typename A, typename C = std::conditional_t<ReadOnly, A const, A>>
		ALWAYS_INLINE constexpr DataView(C & data) noexcept : 
			m_Data  { std::data( data ) }, 
			m_Length{ std::size( data ) }
		{}
		/**
		 * @brief The begin iterator.
		 * 
		 * @return iterator The begin iterator of the fifo.
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		constexpr iterator begin() noexcept
		{
			return m_Data;
		}
		/**
		 * @brief The begin iterator.
		 * 
		 * @return iterator The begin iterator of the fifo.
		 */
		constexpr const_iterator begin() const noexcept
		{
			return m_Data;
		}
		/**
		 * @brief The end iterator.
		 * 
		 * @return iterator The end iterator of the fifo.
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		constexpr iterator end() noexcept
		{
			return m_Data + m_Length;
		}
		/**
		 * @brief The end iterator.
		 * 
		 * @return const_iterator The end iterator of the fifo.
		 */
		constexpr const_iterator end() const noexcept
		{
			return m_Data + m_Length;
		}
		/**
		 * @brief The begin iterator.
		 * 
		 * @return iterator The begin iterator of the fifo.
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		constexpr iterator cbegin() noexcept
		{
			return m_Data;
		}
		/**
		 * @brief The begin iterator.
		 * 
		 * @return iterator The begin iterator of the fifo.
		 */
		constexpr const_iterator cbegin() const noexcept
		{
			return m_Data;
		}
		/**
		 * @brief The end iterator.
		 * 
		 * @return const_iterator The end iterator of the fifo.
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		constexpr iterator cend() noexcept
		{
			return m_Data + m_Length;
		}
		/**
		 * @brief The end iterator.
		 * 
		 * @return const_iterator The end iterator of the fifo.
		 */
		constexpr const_iterator cend() const noexcept
		{
			return m_Data + m_Length;
		}
		/**
		 * @brief Get the reverse begin iterator.
		 * 
		 * @return reverse_iterator for rbegin.
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		constexpr reverse_iterator rbegin() noexcept
		{
			return { end() };
		}
		/**
		 * @brief Get the reverse iterator for end.
		 * 
		 * @return reverse_iterator for rend. 
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		constexpr reverse_iterator rend() noexcept
		{
			return { begin() };
		}
		/**
		 * @brief Get the reverse iterator for the begin.
		 * 
		 * @return const_reverse_iterator The reverse iterator.
		 */
		constexpr const_reverse_iterator rbegin() const noexcept
		{
			return { end() };
		}
		/**
		 * @brief Get the reverse iterator for end.
		 * 
		 * @return reverse_iterator for rend. 
		 */
		constexpr const_reverse_iterator rend() const noexcept
		{
			return { begin() };
		}
		/**
		 * @brief Get the reverse iterator for the begin.
		 * 
		 * @return const_reverse_iterator The reverse iterator.
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		constexpr reverse_iterator crbegin() noexcept
		{
			return { end() };
		}
		/**
		 * @brief Get the reverse iterator for end.
		 * 
		 * @return reverse_iterator for rend. 
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		constexpr reverse_iterator crend() noexcept
		{
			return { begin() };
		}
		/**
		 * @brief Get the reverse iterator for the begin.
		 * 
		 * @return const_reverse_iterator The reverse iterator.
		 */
		constexpr const_reverse_iterator crbegin() const noexcept
		{
			return { end() };
		}
		/**
		 * @brief Get the reverse iterator for end.
		 * 
		 * @return reverse_iterator for rend. 
		 */
		constexpr const_reverse_iterator crend() const noexcept
		{
			return { begin() };
		}
		/**
		 * @brief Get the number of items that are contained in the dataview.
		 * 
		 * @return size_type The number of items that the dataview is viewing. 
		 */
		ALWAYS_INLINE constexpr size_type Size() const noexcept
		{
			return m_Length;
		}
		/**
		 * @brief Gets whether or not the dataview contains any items.
		 * 
		 * @return true The dataview is empty.
		 * @return false The dataview contains items.
		 */
		ALWAYS_INLINE constexpr bool Empty() const noexcept
		{
			return ( Size() == 0 ) or ( Data() == nullptr );
		}
		/**
		 * @brief Gets the item at an index in the data.
		 * 
		 * @param index Get the data at the specified index (relative to the start of the dataview, not the underlying container.)
		 * @return reference The item. 
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		ALWAYS_INLINE constexpr reference operator[](size_type index) noexcept
		{
			return *(m_Data + index);
		}
		/**
		 * @brief Gets the item at an index in the data.
		 * 
		 * @param index Get the data at the specified index (relative to the start of the dataview, not the underlying container.)
		 * @return const_reference The item. 
		 */
		ALWAYS_INLINE constexpr const_reference operator[](size_type index) const noexcept
		{
			return *(m_Data + index);
		}
		/**
		 * @brief Gets the item at an index in the data.
		 * 
		 * @param index Get the data at the specified index (relative to the start of the dataview, not the underlying container.)
		 * @return reference The item. 
		 */
		ALWAYS_INLINE constexpr const_reference At(size_type index) const noexcept
		{
			return *(m_Data + std::min(index, m_Length - 1));
		}
		/**
		 * @brief Gets the item at an index in the data.
		 * 
		 * @param index Get the data at the specified index (relative to the start of the dataview, not the underlying container.)
		 * @return const_reference The item. 
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		ALWAYS_INLINE constexpr reference At(size_type index) noexcept
		{
			return *(m_Data + std::min(index, m_Length - 1));
		}
		/**
		 * @brief Gets the first item in the dataview.
		 * 
		 * @return const_reference The first item in the dataview.
		 */
		ALWAYS_INLINE constexpr const_reference Front() const noexcept
		{
			return *m_Data;
		}
		/**
		 * @brief Gets the first item in the dataview.
		 * 
		 * @return reference The first item in the dataview.
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		ALWAYS_INLINE constexpr reference Front() noexcept
		{
			return *m_Data;
		}
		/**
		 * @brief Gets the first item in the dataview.
		 * 
		 * @return const_reference The first item in the dataview.
		 */
		ALWAYS_INLINE constexpr const_reference Back() const noexcept
		{
			return *(m_Data + m_Length - 1);
		}
		/**
		 * @brief Gets the last item in the dataview.
		 * 
		 * @return reference The last item in the dataview.
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		ALWAYS_INLINE constexpr reference Back() noexcept
		{
			return *(m_Data + m_Length - 1);
		}
		/**
		 * @brief Get a pointer to the first item in the dataview.
		 * 
		 * @return const_pointer A pointer to the first item in the dataview.
		 */
		ALWAYS_INLINE constexpr const_pointer Data() const noexcept
		{
			return m_Data;
		}
		/**
		 * @brief Get a pointer to the first item in the dataview.
		 * 
		 * @return const_pointer A pointer to the first item in the dataview.
		 */
		template <bool B = !ReadOnly, std::enable_if_t<B, int> = 0>
		ALWAYS_INLINE constexpr pointer Data() noexcept
		{
			return m_Data;
		}
		/**
		 * @brief Checks whether the dataview is null (data pointer is nullptr)
		 * 
		 * @return true The data is null.
		 * @return false The data is not null.
		 */
		ALWAYS_INLINE constexpr bool IsNull() noexcept
		{
			return m_Data == nullptr;
		}
		/**
		 * @brief Increment the pointer at the start of the dataview by the count.
		 * 
		 * @note This will not increment beyond the end of the view. 
		 * If an attempt is made to pop more items than exist, it will result in an empty dataview.
		 * 
		 * @param count The number of items to increment the front. 
		 * @return DataView& For chained operations.
		 */
		constexpr DataView & PopFront(size_type count = 1) noexcept
		{
			auto c{ std::min(Size(), count) };
			m_Data      += c;
			m_Length    -= c;
			return *this;
		}
		/**
		 * @brief Decrement the pointer at the end of the dataview by the count.
		 * 
		 * @note This will not decrement beyond the start of the view. 
		 * If an attempt is made to pop more items than exist, it will result in an empty dataview.
		 * 
		 * @param count The number of items to decrement the end. 
		 * @return DataView& For chained operations.
		 */
		constexpr DataView & PopBack(size_type count = 1) noexcept
		{
			m_Length -= std::min( Size(), count );
			return *this;
		}
		/**
		 * @brief Swap the referenced items in two dataviews
		 * 
		 * @note This doesn't change the underlying data in either view, nor does it move the data.
		 * 
		 * @param input The data to swap with.
		 */
		constexpr void Swap(DataView & input) noexcept
		{
			auto temp{ *this };
			*this = input;
			input = temp;
		}
		/**
		 * @brief Extracts a subview of the current dataview. This is similar to substring.
		 * 
		 * @note This will not extract more items than exist, if a view larger than the parent view is requested
		 * the size will be limited to the number of items avaliable relative to the start index.
		 * 
		 * @param index The index to start the subview.
		 * @param length The number of items in the subview.
		 * @return DataView The new subview. 
		 */
		constexpr DataView Subview( size_type index, size_type length = npos ) const noexcept
		{
			auto const sta = std::min( index,	Size() );
			auto const len = std::min( length,	Size() );
			return { m_Data + sta, len };
		}
		/**
		 * @brief Extracts a subview from the start of this DataView.
		 * 
		 * @param index The index to split the Dataview.
		 * @return DataView A dataview starting at the same point as this, with index number of items.
		 */
		ALWAYS_INLINE constexpr DataView Consume( std::size_t index ) const noexcept
		{
			return { m_Data, std::min( index, Size() ) };
		}
		/**
		 * @brief Splits the dataview into a pair of dataviews.
		 * 
		 * Example:
		 * 
		 * If a dataview was viewing the string:
		 * "hello, world"
		 *  012345678901
		 * 
		 * If the pair is defined as std::pair<lhs, rhs> then:
		 * lhs = "hello,"
		 * rhs = " world"
		 * 
		 * @param index The splitting index
		 * @return std::pair<DataView, DataView> The two views.
		 */
		ALWAYS_INLINE constexpr std::pair<DataView, DataView> Split( std::size_t index ) const noexcept
		{
			auto const i = std::min( index, Size() );
			return { { m_Data, i }, { m_Data + i, Size() - i } };
		}
		/**
		 * @brief Determines whether the dataview items are const.
		 */
		static constexpr bool IsReadOnly_v = ReadOnly;
	};
	//
	template <typename T>
	DataView(T, std::size_t)->DataView<T>;
	template <typename C>
	DataView(C)->DataView<typename C::value_type>;
	//
	using StringIO = DataView<char, false>;
	using StringView = DataView<char, true>;
}