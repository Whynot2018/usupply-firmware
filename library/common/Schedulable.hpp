#pragma once

#include <array>
#include <type_traits>
#include <utility>

namespace System
{
	/**
	 * @brief A callback which has the Run function.
	 * 
	 */
	struct GenericCallback
	{
		virtual ~GenericCallback() = default;
		virtual void Run() = 0;
	};
	/**
	 * @brief This is a type of callback
	 * 
	 * @tparam Tag 
	 * @tparam Bytes 
	 */
	template <typename Tag, std::size_t Bytes>
	struct Schedulable
	{
		/**
		 * @brief A wrapper for the function, so that it can called with Run.
		 * 
		 * @tparam F 
		 */
		template <typename F>
		struct Callable : GenericCallback
		{
			F m_Function;
			/**
			 * @brief Construct a new Callable object
			 * 
			 * @param function 
			 */
			template <typename C>
			Callable(C && function) noexcept:
				m_Function{ std::forward<C>( function ) }
			{}
			/**
			 * @brief Runs the function.
			 */
			void Run() override
			{
				m_Function();
			}
			virtual ~Callable() = default;
		};
		//
		inline static std::array<uint8_t, Bytes> m_Data;
		inline static GenericCallback * volatile m_Ptr = nullptr;
		/**
		 * @brief Construct the function (perhaps a function object)
		 * 
		 * @tparam F The function type.
		 * @param function The function.
		 * @return true The construction succeeded.
		 * @return false The construction failed.
		 */
		template <typename F>
		static bool Construct(F && function) noexcept
		{
			static_assert(sizeof(std::decay_t<F>) <= Bytes, "Function does not fit into storage.");
			//Type erasure to allow generic storage of callback
			if (m_Ptr == nullptr)
			{
				using C = std::decay_t<F>;
				m_Ptr = new (m_Data.data()) Callable<C>{ std::forward<C>(function) };
				return true;
			}
			return false;
		}
		/**
		 * @brief Destruct the function object.
		 * 
		 */
		static void Destruct() noexcept
		{
			if (m_Ptr != nullptr)
			{
				m_Ptr->~GenericCallback();
				m_Ptr = nullptr;
			}
		}
		/**
		 * @brief Runs the function and then destroys the callback.
		 * 
		 * @return true A function ran.
		 * @return false No function ran.
		 */
		static bool Run()
		{
			if (m_Ptr != nullptr)
			{
				/**
				 * Copy then clear the function pointer
				 */
				auto ptr{ m_Ptr };
				m_Ptr = nullptr;
				/**
				 * Run then destruct the scheduled action
				 */
				ptr->Run();
				ptr->~GenericCallback();
				/**
				 * Only fails when no function exists
				 *  Typically used with void return,
				 * 	so this might be irrelevant.
				 */
				return true;
			}
			return false;
		}
	};
}