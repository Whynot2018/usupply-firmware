#pragma once
#include <utility>

namespace General
{
    /**
     * @brief Converts a pair of functions into an RAII constructs.
     * 
     * @tparam Destruct The type of the destructor function.
     */
    template <typename Destruct>
    class RAII
    {
    private:
        Destruct m_Destruct;
    public:
        /**
         * @brief 
         * 
         * @tparam C The type of the constructor.
         * @tparam D The type of the destructor.
         * @param construct the constructor function.
         * @param destruct the destructor function.
         */
        template <typename C, typename D>
        constexpr RAII(C && construct, D && destruct) noexcept : 
            m_Destruct{ std::forward<D>(destruct) }
        {
            std::forward<C>(construct)();
        }
        /**
         * @brief Destroy the RAII object
         * 
         * Runs the destructor function.
         */
        ~RAII() noexcept
        {
            m_Destruct();
        }
    };
    //
    template <typename C, typename D>
    RAII(C, D) -> RAII<D>;
}