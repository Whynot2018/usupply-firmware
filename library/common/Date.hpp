#pragma once

#include "IntegerConvert.hpp"
#include <cassert>

namespace General
{
	/**
	 * @brief An enum of the avaliable weekdays.
	 */
	enum class Weekday : std::int8_t
	{
		Monday    = 1,
		Tuesday   = 2,
		Wednesday = 3,
		Thursday  = 4,
		Friday    = 5,
		Saturday  = 6,
		Sunday    = 7,
		Today
	};
	/**
	 * @brief An enum of the avalaibe months
	 */
	enum class Month : std::int8_t
	{
		January   = 1,
		February  = 2,
		March     = 3,
		April     = 4,
		May       = 5,
		June      = 6,
		July      = 7,
		August    = 8,
		September = 9,
		October   = 10,
		November  = 11,
		December  = 12,
	};
	/**
	 * @brief An enum for the date order.
	 */
	enum class DateOrder
	{
		YMD,
		DMY,
		MDY
	};
	/**
	 * @brief An object that deals with an assignable year object that supports chained date operations.
	 * 
	 * The eventual purpose of this is to provide to and from string capibilities.
	 */
	template <typename Base>
	struct DateYear : Base
	{
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateYear() = delete;
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateYear( const DateYear & ) = delete;
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateYear &operator=( const DateYear & ) = delete;
		/**
		 * @brief Assigns a value to the component of the the underlying date object
		 * 
		 * @param value The value to assign.
		 * @return Date& The underlying date object.
		 */
		Base &operator=( std::int8_t value ) noexcept
		{
			Base::m_Year = value;
			return *this;
		}
		/**
		 * @brief Implicity converts to the year as a int8_t (2 digit year)
		 * 
		 * @return std::int8_t The resulting year.
		 */
		operator std::int8_t() const
		{
			return Base::m_Year;
		}
	};
	/**
	 * @brief An object that deals with an assignable month object that supports chained date operations.
	 * 
	 * The eventual purpose of this is to provide to and from string capibilities.
	 */
	template <typename Base>
	struct DateMonth : Base
	{
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateMonth() = delete;
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateMonth( const DateMonth & ) = delete;
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateMonth &operator=( const DateMonth & ) = delete;
		/**
		 * @brief Assigns a value to the component of the the underlying date object
		 * 
		 * @param value The value to assign.
		 * @return Date& The underlying date object.
		 */
		Base &operator=( Month const &value ) noexcept
		{
			Base::m_Month = (std::int8_t)value;
			return *this;
		}
		/**
		 * @brief Converts the month to a month enum.
		 * 
		 * @return General::Month The month as an enum.
		 */
		operator General::Month() const noexcept
		{
			return (General::Month)Base::m_Month;
		}
		/**
		 * @brief Assigns a value to the component of the the underlying date object
		 * 
		 * @param input The value to assign.
		 * @return Date& The underlying date object.
		 */
		Base &operator=( std::int8_t input ) noexcept
		{
			Base::m_Month = (std::int8_t)input;
			return *this;
		}
		/**
		 * @brief Implicitly converts the Month to a int8_t.
		 * 
		 * @return int8_t The month as a int8_t where January = 1.
		 */
		operator int8_t() const noexcept
		{
			return Base::m_Month;
		}
	};
	/**
	 * @brief An object that deals with an assignable day object that supports chained date operations.
	 * 
	 * The eventual purpose of this is to provide to and from string capibilities.
	 */
	template <typename Base>
	struct DateDay : Base
	{
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateDay() = delete;
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateDay( const DateDay & ) = delete;
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateDay &operator=( const DateDay & ) = delete;
		/**
		 * @brief Assign a weekday to the date.
		 * 
		 * @param input The day to assign
		 * @return Date& The underlying date object.
		 */
		Base &operator=( Weekday input ) noexcept
		{
			Base::m_Month = (std::int8_t)input;
			return *this;
		}
		/**
		 * @brief Converts the day to a Weekday enum.
		 * 
		 * @return Weekday The weekday as an enum.
		 */
		operator Weekday() const noexcept
		{
			return (Weekday)Base::m_Day;
		}
		/**
		 * @brief Assigns a value to the component of the the underlying date object
		 * 
		 * @param input The value to assign.
		 * @return Date& The underlying date object.
		 */
		Base &operator=( std::int8_t const &input ) noexcept
		{
			Base::m_Day = input;
			return *this;
		}
		/**
		 * @brief Implicitly converts the Day to a int8_t.
		 * 
		 * @return int8_t The month as a int8_t where Monday = 1.
		 */
		operator std::int8_t() const noexcept
		{
			return Base::m_Day;
		}
	};
	/**
	 * @brief A data structure that container day, month and year.
	 * 
	 */
	class Date
	{
	protected:
		std::int8_t m_Year, m_Month, m_Day;

	public:
		/**
		 * @brief Construct a new Date object
		 * 
		 * @param year The year.
		 * @param month The month.
		 * @param day The day.
		 */
		Date( std::int8_t const &year, std::int8_t const &month, std::int8_t const &day ) noexcept:
		    m_Year( year ),
		    m_Month( month ),
		    m_Day( day )
		{}

		using DateYear_t = DateYear<Date>;
		using DateMonth_t = DateMonth<Date>;
		using DateDay_t = DateDay<Date>;
		/**
		 * @brief Returns a DateYear object, which inherits from Date.
		 * 
		 * The result has the same underlying storage as the Date object.
		 * 
		 * @return DateYear_t& The Date object
		 */
		DateYear_t & Year() noexcept
		{
			return static_cast<DateYear_t&>( *this );
		}
		/**
		 * @brief Returns a DateMonth object, which inherits from Date.
		 * 
		 * The result has the same underlying storage as the Date object.
		 * 
		 * @return DateMonth_t& The Date object
		 */
		DateMonth_t & Month() noexcept
		{
			return static_cast<DateMonth_t &>( *this );
		}
		/**
		 * @brief Returns a DateDay object, which inherits from Date.
		 * 
		 * The result has the same underlying storage as the Date object.
		 * 
		 * @return DateDay& The Date object
		 */
		DateDay_t & Day() noexcept
		{
			return static_cast<DateDay_t &>( *this );
		}
		/**
		 * @brief Returns a DateYear object, which inherits from Date.
		 * 
		 * The result has the same underlying storage as the Date object.
		 * 
		 * @return DateYear& The Date object
		 */
		DateYear_t const & Year() const noexcept
		{
			return static_cast<DateYear_t const &>( *this );
		}
		/**
		 * @brief Returns a DateDay object, which inherits from Date.
		 * 
		 * The result has the same underlying storage as the Date object.
		 * 
		 * @return DateDay& The Date object
		 */
		DateMonth_t const & Month() const noexcept
		{
			return static_cast<DateMonth_t const &>( *this );
		}
		/**
		 * @brief Returns a DateDay object, which inherits from Date.
		 * 
		 * The result has the same underlying storage as the Date object.
		 * 
		 * @return DateDay& The Date object
		 */
		DateDay_t const & Day() const noexcept
		{
			return static_cast<DateDay_t const &>( *this );
		}
		/**
		 * @brief Converts the date to a string, which is returned as a constant sized string.
		 * 
		 * @tparam DateOrder::YMD The order to output the date.
		 * @tparam NullTerminated Whether to ensure a null termination at the end of the string.
		 * @return auto The output string (in the form of a character array)
		 */
		template <DateOrder Order = DateOrder::YMD, bool NullTerminated = false>
		auto ToString() noexcept
		{
			static constexpr auto L = 2u;
			static constexpr auto N = 8u + (unsigned)NullTerminated;
			static constexpr auto S = '/';
			std::array<char, N> output{};
			{
				auto length = 0u;
				if constexpr ( Order == DateOrder::YMD )
				{
					length += General::fixed_utoa<L>( (std::int8_t)m_Year, output, length, '0' );
					if ( length < N ) output[ length++ ] = S;
					length += General::fixed_utoa<L>( (std::int8_t)m_Month, output, length, '0' );
					if ( length < N ) output[ length++ ] = S;
					length += General::fixed_utoa<L>( (std::int8_t)m_Day, output, length, '0' );
				}
				if constexpr ( Order == DateOrder::MDY )
				{
					length += General::fixed_utoa<L>( (std::int8_t)m_Month, output, length, '0' );
					if ( length < N ) output[ length++ ] = S;
					length += General::fixed_utoa<L>( (std::int8_t)m_Day, output, length, '0' );
					if ( length < N ) output[ length++ ] = S;
					length += General::fixed_utoa<L>( (std::int8_t)m_Year, output, length, '0' );
				}
				if constexpr ( Order == DateOrder::DMY )
				{
					length += General::fixed_utoa<L>( (std::int8_t)m_Day, output, length, '0' );
					if ( length < N ) output[ length++ ] = S;
					length += General::fixed_utoa<L>( (std::int8_t)m_Month, output, length, '0' );
					if ( length < N ) output[ length++ ] = S;
					length += General::fixed_utoa<L>( (std::int8_t)m_Year, output, length, '0' );
				}
				if constexpr ( NullTerminated )
					output[ N - 1 ] = '\0';
			}
			return output;
		}
	};
}