#pragma once

#include "Math.hpp"
#include <cstdint>

namespace General
{
    /**
     * @brief A class used to divide a steady tick to a different frequency.
     * 
     * When the desired frequency ticks a callback is called, emulating a timer interrupt. 
     */
    class TimerDivider
    {
    private:
        std::uint32_t const m_Reset = 0;
        std::uint32_t mutable m_Count = 0;
    public:
        /**
         * @brief Setup the timer divider with the input timer frequency and the desired output frequency.
         * 
         * @param timer_frequency The frequency that the Run function is called.
         * @param desired_frequency The frequency that the callback should be called (ideally).
         */
        constexpr TimerDivider(std::uint32_t timer_frequency, std::uint32_t desired_frequency) noexcept :
            m_Reset{ General::RoundedDivide( timer_frequency, desired_frequency) },
            m_Count{ m_Reset }
        {}
        /**
         * @brief Increments the internal timer counter and determine whether the callback should be called.
         * 
         * @tparam Callback The callback type.
         * @param callback The callback to call each period of the desired frequency.
         */
        template <typename Callback>
        constexpr void Run(Callback && callback) noexcept
        {
			if (!(m_Count--))
            {
                m_Count = m_Reset;
                std::forward<Callback>(callback)();
            }
        }
    };
}