#pragma once
#include "USB_StandardInterfaceDescriptor.hpp"
#include "USB_StandardEndpointDescriptor.hpp"
#include "PackedTuple.hpp"

namespace USB
{
    #pragma pack(push, 1)
    /**
     * @brief Sets up a configuration descriptor from a set of different interface descriptors.
     * 
     * MUST have EMPTY base optimisation, this is why this isn't composition.
     * 
     * @tparam Interfaces The interface descriptors.
     */
    template <typename ... Interfaces>
    struct PackagedConfiguration : StandardConfigurationDescriptor, PackedTuple<Interfaces ...> 
    {
        constexpr static std::size_t Count = sizeof...(Interfaces);
        /**
         * @brief Constructs a PackagedConfiguration descriptor
         * 
         * This uses a configuration descriptor and a set of interfaces and fills 
         * in the various fields in the decriptor that depend on the total size and count of the config 
         * and interface descriptors. 
         * 
         * @param config The configuration descriptor.
         * @param interfaces The interface descriptors.
         */
        constexpr PackagedConfiguration( AutoConfiguration config, Interfaces ... interfaces ) noexcept :
            StandardConfigurationDescriptor
            {
                {},
                sizeof( PackagedConfiguration ),
                sizeof...( Interfaces ),
                config.bConfigurationValue,
                config.iConfiguration,
                config.bmAttributes,
                config.bMaxPower
            },
            PackedTuple<Interfaces ...>
            {
                interfaces ...
            }
        {}
    };
    #pragma pack(pop)
}