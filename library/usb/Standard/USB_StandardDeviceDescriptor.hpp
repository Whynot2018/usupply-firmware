#pragma once
#include "Version.hpp"
#include "USB_Types.hpp"

namespace USB
{
    #pragma pack(push, 1)
    /**
     * @brief A standard device descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct StandardDeviceDescriptor : StandardHeader<StandardDeviceDescriptor, DescriptorTypes::DEVICE>
    {
        Version     bcdUSB;             ///< Binary coded decimal of the usb version.
        U<1>        bDeviceClass;       ///< The usb device class.
        U<1>        bDeviceSubClass;    ///< The usb device sub class.
        U<1>        bDeviceProtocol;    ///< The device protocol (usually not used).
        U<1>        bMaxPacketSize0;    ///< The maximum packet size for transactions.
        U<2>        idVendor;           ///< The vendor ID.
        U<2>        idProduct;          ///< The product ID.
        Version     bcdDevice;          ///< The binary coded decimal version of the device.
        U<1>        iManufacturer;      ///< The manufacturer string index.
        U<1>        iProduct;           ///< The product string index.
        U<1>        iSerialNumber;      ///< The product serial number index.
        U<1>        bNumConfigurations; ///< The number of configurations.
    };
    #pragma pack(pop)
}