#pragma once

#include "PinDefinitions.hpp"
#include "DataView.hpp"
#include "I2C.hpp"
#include <array>
#include <optional>

namespace Parts::AT34C02D
{
	/**
	 * @brief A AT34C02D implementation kernal.
	 * 
	 * This is used to tell the AT3402D class 
	 * 
	 * @tparam Address0 Bit 0 of the I2C address of the eeprom module.
	 * @tparam Address1 Bit 1 of the I2C address of the eeprom module.
	 * @tparam Address2 Bit 2 of the I2C address of the eeprom module. 
	 */
	template <bool Address0, bool Address1, bool Address2>
	struct Kernal
	{
		static constexpr std::uint8_t A0 = Address0;
		static constexpr std::uint8_t A1 = Address1;
		static constexpr std::uint8_t A2 = Address2;
		static constexpr std::uint8_t Address = 0xA0 | (A0 << 1 | A1 << 2 | A2 << 3);
		/**
		 * @brief The I2C kernal used by the AT34C02D.
		 */
		struct I2C_Kernal
		{
			using role_t = ::Peripherals::I2CRole;
			//
			static constexpr role_t 	Role 			= role_t::Master;
			static constexpr unsigned 	Channel 		= 1;
			static constexpr unsigned	AddressBits 	= 7;
			static constexpr unsigned 	FilterCount 	= 0;
			static constexpr unsigned 	AnalogFilter 	= true;
			static constexpr size_t		BufferSize 		= 32;
			//
			static constexpr float 		DataHoldTime 	= 125e-9;	// in seconds
			static constexpr float 		DataSetupTime 	= 500e-9;	// in seconds
			static constexpr float 		HighTime 		= 500e-9;	// in seconds
			static constexpr float 		LowTime 		= 1250e-9;	// in seconds
		};
		/**
		 * @brief The I2C pins used by the AT34C02D.
		 */
		using I2C_SCL = System::I2C_SCL;
		using I2C_SDA = System::I2C_SDA;
	};
	/**
	 * @brief The state of the AT34C02D state machine.
	 */
	enum State
	{
		Idle,
		ReadWriteAddress,
		ReadData,
		Write
	};
	
	using Addresses_t = Containers::FIFO<std::uint8_t, 32>;
	/**
	 * @brief A manager class for the AT34C02D memory.
	 * 
	 * @tparam Kernal The configuration of the class.
	 * This should be a class build from the Parts::AT34C02D::Kernal class.
	 * 
	 * @tparam I2CModule The underlying I2C module implementation.
	 */
	template <typename Kernal, typename I2CModule>
	class AT34C02D
	{
	private:
		using WriteView_t = Containers::DataView<std::uint8_t, true>;
		using AddressView_t = Containers::DataView<std::uint8_t, true>;
		//
		static constexpr std::uint8_t PageSize = 0x10;
		static constexpr std::uint8_t PageMask = 0x0F;
		//
		volatile State 	m_State = Idle;
		Addresses_t 	m_Addresses;
		I2CModule 		m_I2C;
	public:
		/**
		 * @brief The generator object MUST return an instance of I2C module.
		 * 
		 * @note This pattern is used to enable maximum flexibility without overhead.
		 * 
		 * 	The generator object will be called with a single parameter,
		 * 	for example : generator( m_State )
		 */
		template <typename I2CGenerator>
		AT34C02D( Kernal, I2CGenerator && generator ) noexcept :
			m_I2C{ std::forward<I2CGenerator>( generator )( m_State, m_Addresses ) }
		{}
		/**
		 * @brief This sets up a read transaction at a random address (Random access).
		 *  
		 * @warning This function blocks until the AT34C02D is idle.
		 * @note The read must be within range of the memory.
		 * @note The address must be within the range of the memory.
		 * 
		 * @return true when a read is initiated correctly.
		 * @return false when address is out of range or address + length is out of range.
		 */
		bool Read(std::uint8_t address, Containers::DataView<std::uint8_t, false> data) noexcept
		{
			while(m_State != Idle);
			m_State = State::ReadWriteAddress;
			//
			return
				!!m_Addresses.Push	( address ) &&
				m_I2C.Write			( Kernal::Address, false, AddressView_t{ &m_Addresses.Back(), 1u } ) && 
				m_I2C.Read			( Kernal::Address, true, data );
		}
		/**
		 * @brief This sets up a write transaction at a random address.
		 * 
		 * @warning This function blocks until the AT34C02D is idle.
		 * @note The write must be within range of the memory.
		 * @note The address must be within the range of the memory.
		 * 
		 * @return true when a write is initiated correctly.
		 * @return false when address is out of range or address + length is out of range.
		 */
		bool Write( std::uint8_t address, WriteView_t data ) noexcept
		{
			while ( data.Size() )
			{
				while( m_State != Idle );
				m_State = State::Write;
				//
				std::uint8_t data_length{ std::min<std::uint8_t>( PageSize, PageSize - ( address & PageMask ) ) };
				//
				// Needs to be an exact type language
				decltype(data) write_temp;
				std::tie( write_temp, data ) = data.Split( data_length );
				//
				if ( data_length )
				{
					while ( !m_Addresses.Push( address ) );
					//
					AddressView_t view{ &m_Addresses.Back(), 1u };
					if ( m_I2C.Write( Kernal::Address, true, view, write_temp ) )
						address += data_length;
					else
						return false;
				}
			}
			return true;
		}
		/**
		 * @brief Checks whether the AT34C02D class is busy.
		 * 
		 * @return true state is not idle
		 * @return false state is idle
		 */
		bool Busy() const noexcept
		{
			return (m_State != State::Idle);
		}
	};
	//
	//
	template <typename Kernal, typename I2CGenerator>
	AT34C02D(Kernal, I2CGenerator generator) -> AT34C02D<Kernal, decltype( generator( std::declval<State&>(), std::declval<Addresses_t&>() ) )>;
	//
	// This is the device specific function, this function could be adapted for any number of flash/eeprom chips with the same interface.
	template <typename Kernal, typename ReadComplete, typename WriteComplete>
	auto EEPROM( ReadComplete && rc, WriteComplete && wc )
	{
		return AT34C02D
		{
			Kernal{},
			[ read_complete{ std::forward<ReadComplete>(rc) }, write_complete{ std::forward<WriteComplete>(wc) } ]( State volatile & state, Addresses_t & addresses )
			{
				return Peripherals::I2CModule
				{
					typename Kernal::I2C_Kernal{},
					typename Kernal::I2C_SDA{},
					typename Kernal::I2C_SCL{},
					[&]( Peripherals::I2CEvent e )
					{
						auto complete = [&]{ addresses.Pop(); state = Idle; };
						//
						switch (e)
						{
						case Peripherals::I2CEvent::WriteComplete:
							//
							switch ( state )
							{
							case Write:
								write_complete();
								complete();
								break;
							case ReadWriteAddress:
								state = ReadData;
								break;
							default:
								complete();
								break;
							};
							//
							break;
						case Peripherals::I2CEvent::ReadComplete:
							//
							if ( state == ReadData )
							{
								read_complete();
								complete();
							}
							//
							break;
						default:
							complete();
							break;
						};
					}
				};
			}
		};
	}
}