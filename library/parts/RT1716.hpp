#pragma once
#include "Math.hpp"
#include "Power.hpp"
#include "Interrupt.hpp"
#include "USB_Types.hpp"
#include "USB_HIDCommon.hpp"
#include "InterruptRegisters.hpp"
#include "RegistersTCPC.hpp"

namespace Parts::RT1716
{
    /**
     * @brief The vendor defined registers on the RT1716.
     * 
     * These are extensions to the standard TCPC registers.
     */
    enum class VendorRegisters : uint8_t
    {
        RTCTRL0	    = 0x90,
        RTCTRL3     = 0x93,
        RT_ST	    = 0x97,
        RT_INT	    = 0x98,
        RT_MASK	    = 0x99,
        RTCTRL8     = 0x9B,
        RTCTRL9     = 0x9C,
        RTCTRL11    = 0x9E,
        RTCTRL12    = 0x9F,
        RTCTRL13    = 0xA0,
        RTCTRL14    = 0xA1,
        RTCTRL15    = 0xA2,
        RTCTRL16_17 = 0xA3
    };
    struct RTCTRL0
    {
        /**
         * @details
         * 24M oscillator for BMC communication 
         * 0b : Disable 24M oscillator 
         * 1b : Enable 24M oscillator (default) 
         * Note : 24M oscillator will be enabled automatically when INT occur.
         */
        uint8_t BMCIO_OSC_EN        : 1;
        /**
         * @details
         * VBUS detection enable 
         * 0b : Measure off 
         * 1b : Operation (default)
         */
        uint8_t VBUS_DETEN          : 1;
        /**
         * @details
         * BMCIO BandGap enable 
         * 0b : BandGap off  CC pin function disable 
         * 1b : BandGap on (default) CC pin function enable
         */
        uint8_t BMCIO_BG_EN         : 1;
        /**
         * @details 
         * Low power mode enable 
         * 0b : Standby mode (default) 
         * 1b : Low power
         */
        uint8_t BMCIO_LPEN          : 1;
        /**
         * @details
         * Low power mode enable 
         * 0b : Low power mode RD (default) 
         * 1b : Low power mode RP
         */
        uint8_t BMCIO_LPRPRD        : 1;
        /**
         * @details
         * VCONN OVP occurs and discharge path turn-on 
         * 0b : No discharge (default) 
         * 1b : Discharge
         */
        uint8_t VCONN_DISCHARGE_EN  : 1;
        uint8_t Reserved            : 2;
    };
    struct RTCTRL3
    {
        uint8_t Reserved            : 5;
        /**
         * @details
         * Over-current control selection
         * 3'b000 -> Current level=200mA
         * 3'b001 -> Current level=300mA
         * 3'b010 -> Current level=400mA
         * 3'b011 -> Current level=500mA
         * 3'b100 -> Current level=600mA
         * 3'b101 -> Current level=700mA
         * 3'b110 -> Current level=800mA
         * 3'b111 -> Current level=VCONN source limiting (Un-bounded)
         */
        uint8_t BMCIO_VCONOCP       : 3;
    };
    //
    //
    struct RT_ST
    {
        uint8_t Reserved0           : 1;
        uint8_t VBUS_80             : 1; ///< 0b: VBUS over 0.8V, 1b: VBUS under 0.8V
        uint8_t Reserved1           : 6;
    };
    struct RT_INT
    {
        uint8_t INT_WAKEUP      : 1; ///< 0b: Cleared, 1b: Sleep Mode Exited
        uint8_t INT_VBUS_80     : 1; ///< 1b: VBUS under 0.8V 
        uint8_t Reserved0       : 3;
        uint8_t INT_RA_DETACH   : 1; ///< 0b: Cleared, 1b: Ra detach.
        uint8_t Reserved1       : 2;
    };
    struct RT_MASK
    {
        uint8_t M_WAKEUP    : 1; ///< 0b: Interrupt masked, 1b: Interrupt unmasked
        uint8_t M_VBUS_80   : 1; ///< 0b: Interrupt masked, 1b: Interrupt unmasked
        //
        //
        uint8_t Reserved0   : 3;
        //
        //
        uint8_t M_RA_DETACH : 1;
        //
        //
        uint8_t Reserved1   : 2;
    };
    //
    //
    struct RTCTRL8
    {
        /**
         * @details 
         * Enter idle mode timeout time = (AUTOIDLE_TIMEOUT*2+1)*6.4ms
         */
        uint8_t AUTOIDLE_TIMEOUT    : 3; // 0b010
        /**
         * @details 
         * 1 : Auto enter idle mode enable
         * 0 : Auto enter idle mode disable
         */
        uint8_t AUTOIDLE_EN         : 1; // 1
        /**
         * @details 
         * 0 : Disable PD3.0 Extended message (default) 
         * 1 : Enable PD3.0 Extended message affect GoodCRC receive detect between PD2.0 and PD3.0
         */
        uint8_t ENEXTMSG            : 1; // 0
        /**
         * @details
         * 0 : Shutdown mode (default) 
         * 1 : Non-Shutdown mode
         */
        uint8_t Shutdown_OFF        : 1; // 1
        uint8_t Reserved            : 1; // 0
        /**
         * @details
         * 0b : Clock_320K from Clock_320K 
         * 1b : Clock_300K divided from Clock_24M (default)
         */
        uint8_t CK_300K_SEL         : 1; // 0
    };
    struct RTCTRL9
    {
        /**
         * @details
         * 1: Enable the INT timeout reset.
         * 0: Disable the INT timeout reset.
         */
        uint8_t INT_RST_SEL         : 2;
        uint8_t Reserved            : 4;
        /**
         * @details
         * INT timeout time = (INT_RST_SEL+1) * 0.2 sec
         */
        uint8_t INT_RST_EN          : 2;
    };
    struct RTCTRL11
    {
        uint8_t I2C_TO_RST_SEL      : 4; ///< timeout time = (I2C_TO_RST_SEL+1) * 12.5ms
        uint8_t Reserved            : 3;
        uint8_t I2C_TO_RST_EN       : 1; ///< 1: Enable the I2C timeout reset function.(SCL and SDA keep low)
    };
    struct RTCTRL12
    {
        uint8_t Reserved0           : 7;
        /**
         * @details 
         * 0: Wakeup function disable.
         * 1: Wakeup function enable.
         */
        uint8_t WAKEUP_EN           : 1;
    };
    struct RTCTRL13
    {
        /**
         * @details 
         * Write 1 to trigger software reset.
         */
        uint8_t SOFT_RESET          : 1;
        uint8_t Reserved0           : 7;
    };
    struct RTCTRL14
    {
        /**
         * @details 
         * tTCPCfilter time = TTCPCFILTER * 26.7us
         */
        uint8_t TTCPCFILTER         : 4;
        uint8_t Reserved0           : 4;
    };
    struct RTCTRL15
    {
        /**
         * @details 
         * Source to Sink and back 
         * advertisement. (Period = TDRP * 6.4 
         * + 51.2ms) 
         * 0000 : 51.2ms 
         * 0001 : 57.6ms 
         * 0010 : 64ms 
         * 0011 : 70.4ms (default) 
         * … 
         * 1110 : 140.8ms 
         * 1111 : 147.2ms
         */
        uint8_t TDRP                : 4;
        uint8_t Reserved0           : 4;
    };
    struct RTCTRL16_17
    {
        /**
         * @details
         * The percent of time that a DRP will 
         * advertise Source during tDRP. 
         * (DUTY = (DCSRCDRP[9:0] + 1) / 
         * 1024) 
         * 0000000000 : 1/1024 
         * 0000000001 : 2/1024 
         * … 
         * 0101000111 : 328/1024 (default) 
         * … 
         * 1111111110 : 1023/1024 
         * 1111111111 : 1024/1024 
         * Note : Setting with 0xA4[9:8]
         */
        uint16_t DCSRCDRP : 10;
        uint8_t Reserved : 6;
    };
    //
    //
    template <typename INT_N>
    struct RT1716Power
    {
        // Setup the external interrupt on the correct channel
        using isr = System::InterruptGeneral::EXTIChannel<INT_N::PortIndex>;
        //
        static void Construct() noexcept
        {
            //
            // Enable the external interrupt
            isr{}.IM() = 1;
            //
            // Don't interrupt on rising edge
            isr{}.RT() = 0; 
            //
            // Interrupt on falling edge
            isr{}.FT() = 1;
            //
            // Clear the pending bit
            isr{}.PIF() = 1;
        }
        static void Destruct() noexcept
        {
            //
            // Enable the external interrupt
            isr{}.IM() = 0;
            //
            // Clear the pending bit
            isr{}.PIF() = 1;
        }
    };
    //
    //
    static constexpr uint8_t RT1716Address = 0b1001110;
    //
    //
    template <typename INT_N, typename I2C>
    class RT1716 :
        INT_N,
        General::ModulePower<RT1716Power<INT_N>>,
        public USB::PD::TCPM<I2C, RT1716Address>, 
        System::Interrupt<RT1716<INT_N, I2C>,System::ExternalInterrupt<INT_N::PinIndex>(),3>
    {
    private:
        using function_t = void (*)(USB::PD::TCPCMessage); 
		using callback_t = General::StaticLambdaWrapper<function_t>;
        using Base = USB::PD::TCPM<I2C, RT1716Address>;
        /**
         */
        static constexpr uint16_t RequiredProductID = 0x1711;
        static constexpr uint16_t RequiredVendorID  = 0x29CF;
        static constexpr uint16_t RequiredDeviceID  = 0x2173;
        /**
         */
        #define READ_REG(x) []() -> x{ return Base::template ReceiveRegister<VendorRegisters, x>( VendorRegisters::x ); }()
        #define WRITE_REG(r, x) [](r write_reg_input) -> bool { return Base::WriteRegister(VendorRegisters::r, write_reg_input); }(x)
        /**
         * @brief This is a message callback, used to move the TCPM state machine.
         */
        callback_t m_Callback;
    public:
        RT1716() = default;
        /**
         * @brief construct the class with a event callback.
         * The constructor only resquires the I2C address (for RT1716 i believe this is constant)
         *  This is provided because the datasheet implies other addresses are possible (I don't know how...)
         * 
         * @tparam C The type of the callback.
         * @param callback The callback.
         */
        template <typename C>
        RT1716( C && callback ) noexcept :
            INT_N{ IO::Mode::Input, IO::Interrupt::Enabled, IO::InterruptEdge::FallingEdge, IO::Resistors::PullUp },
            m_Callback{ std::forward<C>(callback) }
        {}
        /**
         * @brief Basic storage of TCPC properties
         * 
         * This is not needed outside of debugging.
         * 
         * @param i2c_clock_stretching Whether (true) or not (false) clock stretching is enabled.
         * @param vconn_target The target VCONN, either CC1 or CC2.
         * @return true The control register was assigned.
         * @return false The transation failed.
         */
        static bool SendTCPControl( bool i2c_clock_stretching, USB::PD::VCONNTarget vconn_target ) noexcept
        {
            /**
             * This is the configuration of type-c port controller, this is done ONLY by the TCPM.
             */
            USB::PD::TCPC_CONTROL tcpc_control;
            tcpc_control.BIST_TEST_MODE = 0;                                        ///< We are not trying to get USB compliance, we cannot claim full USB-PD support.
            tcpc_control.I2C_CK_STRETCH = (i2c_clock_stretching) ? 0b11 : 0b00;     ///< Clock stretching is enabled in bitbanging library, why wouldn't that be fine?
            tcpc_control.PLUG_ORIENT    = General::UnderlyingValue(vconn_target);   ///< @todo Seems like we need to use orientation detect to set this bit.
            /**
             */
            return Base::TCPCControl( tcpc_control );
        }
        /**
         * @brief 
         * 
         * @param data_role 
         * @param current_set_cc_resistor 
         * @param is_dual_role_port 
         * @return true 
         * @return false 
         */
        static bool SendRoleControl( USB::PD::PortDataRole data_role, USB::PD::RpValue current_set_cc_resistor = USB::PD::RpValue::Rp_Default, bool is_dual_role_port = false ) noexcept
        {
            /**
             * This configures the role for the PD device.
             */
            USB::PD::ROLE_CONTROL role_control{};
            role_control.RP_VALUE   = current_set_cc_resistor;  // Needs to be configured as-per current setpoint.
            role_control.DRP        = General::TrueIsOne( is_dual_role_port );              // This class never supports dual role ports.
            /**
             * Configure the CC resistors as per the data role
             */
            auto role = []( USB::PD::PortDataRole port_data_role )
            {
                switch ( port_data_role )
                {
                case USB::PD::PortDataRole::UFP:     return USB::PD::CCResistor::Rd;
                case USB::PD::PortDataRole::DFP:     return USB::PD::CCResistor::Ra;
                }
                return USB::PD::CCResistor::Open;
            }( data_role );
            /**
             * Always the same, so configure them akin.
             */
            role_control.CC1 = role;
            role_control.CC2 = role;
            /**
             */
            return Base::RoleControl( role_control );
        }
        /**
         * @brief 
         * 
         * @param enable_ovp_circuit 
         * @param enable_ocp_circuit 
         * @param enable_discharge_fault_detection_timer 
         * @param block_standard_input_signal 
         * @param discharge_fault_ov_vcon 
         * @return true 
         * @return false 
         */
        static bool SendFaultControl(bool enable_ovp_circuit, bool enable_ocp_circuit, bool enable_discharge_fault_detection_timer, bool block_standard_input_signal, bool discharge_fault_ov_vcon) noexcept
        {
            /**
             * This setups up the fault control
             */
            USB::PD::FAULT_CONTROL fault_control{};
            fault_control.DIS_VBUS_OC               =   enable_ocp_circuit;
            fault_control.DIS_VBUS_OV               =   enable_ovp_circuit;
            fault_control.DIS_VBUS_DISC_FAULT_TIMER =   enable_discharge_fault_detection_timer;
            fault_control.DIS_FORCE_OFF_VBUS        =   block_standard_input_signal;
            fault_control.DIS_VCON_OC               =   discharge_fault_ov_vcon;
            fault_control.DIS_VCON_OV               =   discharge_fault_ov_vcon;
            /**
             */
            return Base::FaultControl( fault_control );
        }
        /**
         * @brief Sets the power control register of the RT1716.
         * 
         * @param enable_vcon Enables the VCon pin power.
         * @param conn_power_supported The power supported by the VConnPower.
         * @return true The power control register is enabled.
         * @return false The power control register is not enabled.
         */
        static bool SendPowerControl(bool enable_vcon, USB::PD::VConnPowerSupported conn_power_supported) noexcept
        {
            /**
             * This sets up the power control
             */
            USB::PD::POWER_CONTROL power_control{};
            power_control.VCONN_POWER_SPT   =   General::UnderlyingValue( conn_power_supported );
            power_control.EN_VCONN          =   (enable_vcon) ? 1 : 0;
            /**
             */
            return Base::PowerControl( power_control );
        }
        /**
         * @brief Sends the fault status register.
         * 
         * @note this assigns 1's to all bits in the FAULT_STATUS.
         * 0's do nothing if written to a register.
         * 
         * @return true The fault status register was set.
         * @return false The fault status register was not set.
         */
        static bool SendFaultStatus() noexcept
        {
            /**
             * Handle the fault status control
             *  Write 1 to clear, 0 does nothing...
             */
            USB::PD::FAULT_STATUS fault_status{};
            fault_status.I2C_ERROR       = 1;
            fault_status.VCON_OC         = 1;
            fault_status.VBUS_OV         = 1;
            fault_status.VBUS_OC         = 1;
            fault_status.FORCE_DISC_FAIL = 1;
            fault_status.AUTO_DISC_FAIL  = 1;
            fault_status.FORCE_OFF_VBUS  = 1;
            fault_status.VCON_OV         = 1;
            /**
             * 
             */
            return Base::FaultStatus( fault_status );
        }
        /**
         * @brief Sets the receive detect register.
         * 
         * @param cable_reset detect cable resets.
         * @param hard_reset detect hard resets.
         * @param sop_messages detect start of packet messages.
         * @param sop_cable_debug detect start of packet debug messages.
         * @param sop_cable_debug_messages start of packet cable debug messages.
         * @return true The receive detect was successful.
         * @return false The receive detect setting failed.
         */
        static bool SendReceiveDetect(bool cable_reset = false, bool hard_reset = false, bool sop_messages = false, bool sop_cable_debug = false, bool sop_cable_debug_messages = false) noexcept
        {
            USB::PD::RECEIVE_DETECT receive_detect{};
            receive_detect.EN_CABLE_RST = General::TrueIsOne(cable_reset);              // Doesn't detect cable reset
            receive_detect.EN_HARD_RST  = General::TrueIsOne(hard_reset);               // Doesn't detect hard reset signaling.
            receive_detect.EN_SOP1      = General::TrueIsOne(sop_cable_debug);          // Doesn't detect SOP single prime cable messages
            receive_detect.EN_SOP1DB    = General::TrueIsOne(sop_cable_debug_messages); // Doesn't detect SOP single prime cable debug messages
            receive_detect.EN_SOP2      = General::TrueIsOne(sop_cable_debug);          // Doesn't detect SOP double prime cable messages
            receive_detect.EN_SOP2DB    = General::TrueIsOne(sop_cable_debug_messages); // Doesn't detect SOP double prime cable debug messages
            receive_detect.EN_SOP       = General::TrueIsOne(sop_messages);             // Detects SOP messages
            /**
             */
            return Base::ReceiveDetect( receive_detect );
        }
        /**
         * @brief Sets the message header info on the RT1716.
         * 
         * @param power_role The power role to assign.
         * @param data_role The data role to assign.
         * @param source_sink_or_drp Whether the device is a source, sink or dual role port.
         * @param specification_revision The USB PD revision.
         * @return true The message header was sent to the RT1716.
         * @return false The transaction failed.
         */
        static bool SendMessageHeaderInfo(USB::PD::PortPowerRole power_role, USB::PD::PortDataRole data_role, USB::PD::CablePlug source_sink_or_drp, USB::PD::SpecificationRevision specification_revision) noexcept
        {
            USB::PD::MESSAGE_HEADER_INFO message_info{};
            message_info.POWER_ROLE     = General::UnderlyingValue( power_role );
            message_info.DATA_ROLE      = General::UnderlyingValue( data_role );
            message_info.CABLE_PLUG     = General::UnderlyingValue( source_sink_or_drp );
            message_info.USBPD_SPECREV  = General::UnderlyingValue( specification_revision );
            /**
             */
            return Base::MessageHeaderInformation( message_info );
        }
        /**
         * @brief Retrieves the source capibilities from the Source.
         * 
         * This function ALWAYS waits for the period of time required before requesting the 
         * capibilties message. This is definded as tTypeCSinkWaitCap in the standard.
         * 
         * @param the time to wait before retrying a request. (default is from standard)
         * @return true 
         * @return false 
         */
        static bool SourceCapibilities( Milliseconds sink_wait = 620, bool requires_critical = false, unsigned attempts = 16 ) noexcept
        {
            /**
             * If the Sink does not receive a Source_Capabilities Message within 
             * tSinkWaitCap/tTypeCSinkWaitCap then it issues Hard Reset Signalling 
             * in order to cause the Source Port to send a Source_Capabilities Message
             * if the Source Port is PD capable.
             */
            while ( attempts-- )
            {
                if ( WaitUntil
                (
                    [] () noexcept
                    {
                        
                    },
                    sink_wait,
                    requires_critical
                ) )
                    return true;
                //
                // This resets the TCPM, it should send a source capibilities as one of its first messages.
                Base::HardwareReset();
            }
            return false;
        }
        /**
         * @brief Ensures the vendor, product and device ID are correct.
         * 
         * @return true When check was successful and chip was detected.
         * @return false When check failed or the wrong chip was detected.
         */
        static bool Check() noexcept
        {
            return
                ( Base::ProductID() == RequiredProductID ) and
                ( Base::VendorID() == RequiredVendorID ) and
                ( Base::DeviceID() == RequiredDeviceID );
        }
        /**
         * @brief Perform a read modify write operation.
         * 
         * This function performs read-modify-write, it also verifies the write and attempts if the write was not successful.
         * If not retry behaviour is needed, set the attempts parameter to 1 (A single attempt).
         * 
         * @tparam R Any type that has the following format T(#)(). # is any pointer or reference type.
         * @tparam M Any type that has the following format T(#)(T). # is any pointer or reference type.
         * @tparam W Any type that has the following format bool(#)(T), should return true when the write function is successful. # is any pointer or reference type.
         * @param read The callback for the read function.
         * @param modify The callback for the modify function.
         * @param write The callback for the write function.
         * @param attempts The number of attempts to write the data ( in total ).
         * @return true The ReadModifyWrite function succeeded on one of its attempts.
         * @return false None of the attempts succeeded.
         */
        template <typename R, typename M, typename W>
        ALWAYS_INLINE static bool ReadModifyWrite( R && read, M && modify, W && write, int attempts = 512 ) noexcept
        {
            auto reader = std::forward<R>( read );
            //
            while ( attempts-- )
            {
                if ( auto value = std::forward<M>( modify )( reader() ); std::forward<W>( write )( value ) )
                {
                    auto result = reader();
                    //
                    if ( memcmp( &value, &result, sizeof( value ) ) == 0 )
                    {
                        return true;
                    }
                }
            }
            //
            return false;
        }
        /**
         * @brief Performs a read then checks whether the value matches.
         * 
         * @tparam R Any type that has the following format T(#)().         # is any pointer or reference type.
         * @tparam C Any type that has the following format bool(#)(T).     # is any pointer or reference type.
         * @param read A function that reads the value.
         * @param check A function that checks against a predicate.
         * @param attempts The number of attempts to write the data ( in total ).
         * @return true When one of the reads was successful.
         * @return false When the read failed.
         */
        template <typename R, typename C>
        ALWAYS_INLINE static bool ReadCheck( R && read, C && check, int attempts = 512 ) noexcept
        {
            while ( attempts-- and not check( std::forward<R>( read )() ) );
            return ( attempts != 0 );
        }
        /**
         * @brief Wait for the power status register to indicate a ready state.
         * 
         * @return true A ready state has been detected.
         * @return false No ready state was detected.
         */
        static bool WaitReady( int attempts = 512 ) noexcept
        {
            return ReadCheck
            (
                []{ return Base::PowerStatus(); },
                []( USB::PD::POWER_STATUS value )
                {
                    return value.TCPC_INITIAL == 0;
                },
                attempts
            );  
        }
        /**
         * @brief Performs a soft reset.
         * 
         * @return true Write was successful.
         * @return false Write failed.
         */
        static bool SoftwareReset() noexcept
        {
            return Ctrl13
            (
                []() -> RTCTRL13
                {
                    RTCTRL13 output{};
                    output.SOFT_RESET = 1;
                    return output;
                }()
            );
        }
        /**
         * @brief Configures the RT1716 and gets it ready for receiving data.
         * 
         * @param source_to_sink_back_advertisement The timing of the back advertisement.
         * @param tcpc_filter_time The timing for the filter.
         * @param i2c_reset_timeout The reset timeout.
         * @param autoidle_timeout The time before the RT1716 goes into idle mode.
         * @param source_advertise_duty_percent The period for the source advertisement.
         * @return true The config was sent to the chip.
         * @return false The config couldn't be sent.
         */
        static bool ConfigureRegisters
        (
            bool            active_mode                         = true,
            Milliseconds    source_to_sink_back_advertisement   = Milliseconds  { 128 },
            Microseconds    tcpc_filter_time                    = Microseconds  { 27 },
            Milliseconds    i2c_reset_timeout                   = Milliseconds  { 15 },
            Milliseconds    autoidle_timeout                    = Milliseconds  { 32 },
            uint32_t        source_advertise_duty_percent       = 33
        ) noexcept
        {
            return
            /**
             * @brief Performs the following
             * -    autoidle timeout
             * -    disable shutdown mode
             * -    enable autoidle if the timeout isn't configured as zero
             */
            ReadModifyWrite
            (
                []{ return Ctrl8(); }, 
                [&]( RTCTRL8 value )
                {

                    value.AUTOIDLE_TIMEOUT = General::RoundedDivide( 5 * autoidle_timeout - 32, 64 );;//0b010
                    //
                    // 1 : Auto enter idle mode enable
                    // 0 : Auto enter idle mode disable
                    value. AUTOIDLE_EN = autoidle_timeout != 0;
                    //
                    // 0 : Disable PD3.0 Extended message (default) 
                    // 1 : Enable PD3.0 Extended message affect GoodCRC receive detect between PD2.0 and PD3.0
                    value. ENEXTMSG = 0;
                    //
                    // 0 : Shutdown mode (default) 
                    // 1 : Non-Shutdown mode
                    value. Shutdown_OFF = General::TrueIsOne(active_mode);
                    value. Reserved = 0;
                    //
                    // 0b : Clock_320K from Clock_320K 
                    // 1b : Clock_300K divided from Clock_24M (default)
                    value. CK_300K_SEL = 0;
                    return value;
                },
                []( RTCTRL8 value ) { return Ctrl8( value ); } 
            )
            and
            Base::ReceiveDetect
            (
                USB::PD::RECEIVE_DETECT
                {
                    1, ///< EN_SOP : 1;
                    0, ///< EN_SOP1 : 1;
                    0, ///< EN_SOP2 : 1;
                    0, ///< EN_SOP1DB : 1;
                    0, ///< EN_SOP2DB : 1;
                    1, ///< EN_HARD_RST : 1;
                    0, ///< EN_CABLE_RST : 1;
                    0  ///< Reserved : 1;
                }
            );
        }
        /**
         * @brief This performs initalisation of the RT1716
         * 
         * This function checks that the chip is present and correct, 
         *  it then triggers a soft reset and waits for the device to become ready.
         *  once ready the device configures the internal registers.
         * 
         * @return true When startup was successful
         * @return false When startup failed
         */
        static bool Init() noexcept
        {
            return
            {
                Base::HardReset()
                and
                WaitReady(-1)
                and
                ConfigureRegisters()
            };
        }
        /**
         * @brief Transmits bytes using the TCPC
         *
         * @todo Route to specific overloads of USB::PD::PDDataMessage
         *          and USB::PD::PDControlMessage.
         * 
         * @return true When the transmission was successfully initiated.
         * @return false When the transmission failed. 
         */
        template <typename Header, typename ... Data>
        static bool Transmit( USB::PD::TxFrame frame, USB::HID::Header header ) noexcept
        {
            constexpr auto header_length = sizeof(Header);
            static_assert(header_length == 2, "Header must contain 2 bytes (the message header)");
            //
            return ReadModifyWrite
            (
                [] { return Base::WriteByteCount(); },
                [] ( USB::PD::TX_BYTE_COUNT value )
                {
                    value.TX_BYTE_COUNT = header_length;
                    return value;
                },
                []( USB::PD::TX_BYTE_COUNT value )
                {
                    return Base::WriteByteCount( value );
                }
            )
            //
            and
            //
            Base::TransmitBuffer( USB::Bytes( header ) )
            //
            and
            //
            Base::WriteFrameType( frame );
        }
        /**
         * @brief Set the Interrupts object
         * 
         * @param on Enable (true) or disable (false) the RT1716 interrupts.
         * @return true Interrupts were enabled.
         * @return false Interrupts couldn't be enabled.
         */
        static bool SetInterrupts( bool on = true ) noexcept
        {
            return ReadModifyWrite
            (
                []{ return Base::AlertMask(); },
                [&](USB::PD::ALERT_MASK mask)
                {
                    auto v = General::TrueIsOne(on);
                    mask.M_RX_HARD_RESET     = v;
                    mask.M_TX_SUCCESS        = v;
                    mask.M_TX_FAIL           = v;
                    mask.M_TX_SUCCESS        = v;
                    mask.M_TX_DISCARD        = v;
                    mask.M_RX_SOP_MSG_STATUS = v;
                    return mask;
                },
                [](USB::PD::ALERT_MASK mask){ return Base::AlertMask( mask ); }
            );
        }
        /**
         * @brief A getter and setter function for the vendor defined RTCTRL0 register.
         */
        GENERATE_GETTER_SETTER_PAIR( RTCTRL0,       Ctrl0 )
        /**
         * @brief A getter and setter function for the vendor defined RTCTRL3 register.
         */
        GENERATE_GETTER_SETTER_PAIR( RTCTRL3,       Ctrl3 )
        /**
         * @brief A getter and setter function for the vendor defined RTCTRL8 register.
         */
        GENERATE_GETTER_SETTER_PAIR( RTCTRL8,       Ctrl8 )
        /**
         * @brief A getter and setter function for the vendor defined RTCTRL9 register.
         */
        GENERATE_GETTER_SETTER_PAIR( RTCTRL9,       Ctrl9 )
        /**
         * @brief A getter and setter function for the vendor defined RTCTRL11 register.
         */
        GENERATE_GETTER_SETTER_PAIR( RTCTRL11,      Ctrl11 )
        /**
         * @brief A getter and setter function for the vendor defined RTCTRL12 register.
         */
        GENERATE_GETTER_SETTER_PAIR( RTCTRL12,      Ctrl12 )
        /**
         * @brief A getter and setter function for the vendor defined RTCTRL13 register.
         */
        GENERATE_GETTER_SETTER_PAIR( RTCTRL13,      Ctrl13 )
        /**
         * @brief A getter and setter function for the vendor defined RTCTRL14 register.
         */
        GENERATE_GETTER_SETTER_PAIR( RTCTRL14,      Ctrl14 )
        /**
         * @brief A getter and setter function for the vendor defined RTCTRL15 register.
         */
        GENERATE_GETTER_SETTER_PAIR( RTCTRL15,      Ctrl15 )
        /**
         * @brief A getter and setter function for the vendor defined RT_ST register.
         */
        GENERATE_GETTER_SETTER_PAIR( RT_ST,         RT_State )
        /**
         * @brief A getter and setter function for the vendor defined RT_INT register.
         */
        GENERATE_GETTER_SETTER_PAIR( RT_INT,        RT_Interrupt )
        /**
         * @brief A getter and setter function for the vendor defined RT_MASK register.
         */
        GENERATE_GETTER_SETTER_PAIR( RT_MASK,       RT_InterruptMask )
        /**
         * @brief A getter and setter function for the vendor defined RTCTRL16 and RTCTRL17 registers.
         */
        GENERATE_GETTER_SETTER_PAIR( RTCTRL16_17,   Ctrl16 )
        /**
         * @brief This is not supported by RT1716.
         * 
         * @return true Always.
         */
        static bool ClockGating() noexcept
        {
            return true;
        }
        /**
         * @brief Runs when the CCStatus has changed.
         * 
         * @param flags The CC flags that were set by the RT1716.
         */
        static void Interrupt_CCStatus( USB::PD::CC_STATUS flags ) noexcept
        {
            // This is required because Looking4Connection relies on historical knowledge.
            static USB::PD::CC_STATUS previous{};
            //
            // Check the upstream device, if it is presenting Rp or Rd.
            if ( flags.DRP_RESULT )
            {
                // Other device presenting Rd.
                callback_t::Run( USB::PD::TCPCMessage::ConnectResultRd );
            }
            else
            {
                // Other device presenting Rp.
                callback_t::Run( USB::PD::TCPCMessage::ConnectResultRp );
            }
            //
            // A transition from 1 to 0 of Looking4Connection (DRP_STATUS) indicates connection found.
            if ( previous.DRP_STATUS and not flags.DRP_STATUS  )
            {
                callback_t::Run( USB::PD::TCPCMessage::PotentialConnectionFound );
            }
            previous = flags;
        }
        /**
         * @brief Handles a change in the system power status.
         * 
         * @param flags The flags that were set by the RT1716.
         */
        static void Interrupt_PowerStatus( USB::PD::POWER_STATUS flags ) noexcept
        {
            // This is required because Looking4Connection relies on historical knowledge.
            if ( flags.SINK_VBUS )
            {
                callback_t::Run( USB::PD::TCPCMessage::SinkVBus );
            }
            if ( flags.VCONN_PRESENT )
            {
                callback_t::Run( USB::PD::TCPCMessage::VConnPresent );
            }
            if ( flags.VBUS_PRESENT )
            {
                callback_t::Run( USB::PD::TCPCMessage::VBusPresent );
            }
            if ( flags.VBUS_PRESENT_DETC )
            {
                callback_t::Run( USB::PD::TCPCMessage::VBusPresentDetect );
            }
            if ( flags.SRC_VBUS )
            {
                callback_t::Run( USB::PD::TCPCMessage::SourceVBus );
            }
            if ( flags.SRC_HV )
            {
                callback_t::Run( USB::PD::TCPCMessage::SourceHighVoltage );
            }
            if ( not flags.TCPC_INITIAL )
            {
                callback_t::Run( USB::PD::TCPCMessage::TCPCInitialisation );
            }
            if ( flags.DBG_ACC_CONNECT )
            {
                callback_t::Run( USB::PD::TCPCMessage::DebugAccessoryConnect );
            }
        }
        /**
         * @brief Handles a change in the fault status bits
         * 
         * @todo Implement.
         */
        static void Interrupt_FaultStatus( USB::PD::FAULT_STATUS flags ) noexcept
        {
        }
        /**
         * @brief Handles the receive interrrupt
         * 
         * @todo Implement.
         */
        static void Interrupt_ReceiveStatus( USB::PD::RX_BUF_FRAME_TYPE flags ) noexcept
        {
        }
        /**
         * @brief Routes the interrupt flags to the appropreate handle
         * 
         * @param flags The flags that are set on the RT1716
         */
        static void Interrupt_AlertStatus( USB::PD::ALERT flags ) noexcept
        {
            /**
             * @brief Receive status register changed
             */
            if ( flags.RX_SOP_MSG_STATUS )
            {
                Interrupt_ReceiveStatus( Base::ReceiveFrameType() );
            }
            /**
             * @brief Received hard reset message
             */
            if ( flags.RX_HARD_RESET )
            {
            }
            /**
             * @brief SOP* message transmission not successful, no GoodCRC response received on SOP* message transmission.
             */
            if ( flags.TX_FAIL )
            {
            }
            /**
             * @brief Reset or SOP* message transmission not sent due to incomming receive message.
             */
            if ( flags.TX_DISCARD )
            {
            }
            /**
             * @brief Reset or SOP* message transmission successful.
             */
            if ( flags.TX_SUCCESS )
            {
            }
            /**
             * Handle INT_N event here, need to MUX which events occured
             */
            if ( auto status{General::PunCast<uint8_t>( Base::CCStatus() )}; flags.CC_STATUS and status )
            {
                Interrupt_CCStatus( General::PunCast<USB::PD::CC_STATUS>( status ) );
            }
            /**
             * Handle the power status events here
             */
            if ( auto status
            {
                General::PunCast<uint8_t>( Base::PowerStatus() ) &
                General::PunCast<uint8_t>( Base::PowerStatusMask() )
            }; flags.POWER_STATUS and status )
            {
                Interrupt_PowerStatus( General::PunCast<USB::PD::POWER_STATUS>( status ) );
            }
            /**
             * Handle the fault status events here
             */
            if ( auto status
            {
                General::PunCast<uint8_t>( Base::FaultStatus() ) &
                General::PunCast<uint8_t>( Base::FaultStatusMask() )
            }; flags.FAULT and status )
            {
                Interrupt_FaultStatus( General::PunCast<USB::PD::FAULT_STATUS>( status ) );
            }
            /**
             * Clear the alert bits
             *  Because write 1 to clear is active, 
             *  just write back the existing value unchanged.
             * 
             * NOTE : No way to handle a failed write at this time?
             */
            Base::Alert( flags );
        }
        /**
         * @brief This is the interrupt called by the GPIO interrupt occured.
         */
        static void Interrupt() noexcept
        {
            if ( INT_N::PendingInterruptFlag() )
            {
                INT_N::ClearInterruptFlag();
                /**
                 * Handle the alert low events
                 */
                Interrupt_AlertStatus( General::PunCast<USB::PD::ALERT>
                (
                    General::PunCast<uint16_t>( Base::Alert() ) & 
                    General::PunCast<uint16_t>( Base::AlertMask() )
                ) );
                /**
                 * Clear the alert flags
                 */
                Base::Alert( USB::PD::ALERT{} );
            }
        }
        #undef READ_REG
        #undef WRITE_REG
    };
}