#include "Timer.hpp"
#include "System.hpp"
#include "Numpad.hpp"
#include "Menu.hpp"
#include "TimerDivider.hpp"
#include "Benchmark.hpp"
#include "PinDefinitions.hpp"
//
using namespace General;
using namespace Peripherals;
//
//
using Parser::SCPI::test_schedulable_t;
using System::save_schedulable_t;
//
// This represents all internal system state (no IO)
static auto usupply = System::CreateSystem();
using usupply_t = decltype(usupply);

#ifndef MINIMUM
//
// Device navigation and user interface
static auto menu 	= System::Menu<usupply_t>();
static auto numpad 	= System::Numpad
(
    []( IO::StateEvent type, System::KeyCodes codes )
    {
        menu.Navigate
        (
            ( (type == IO::StateEvent::LongHigh) && (codes == System::KeyCodes::Presets) ) ?
                System::KeyCodes::PresetsLong :
                codes
        );
        //
        // Setpoints could have changed
        usupply.Screen().UpdateSetpoints();
        usupply.Screen().UpdateMenu<usupply_t>( menu );
    }
);
//
// Used to divide the timers frequency, no need to use multiple timers.
static TimerDivider FsDivider{ System::SampleFrequency, System::UpdateFrequency };
#endif
//
int main( int argc, char * argv[] )
{
	System::LED = IO::State::High;
#ifndef MINIMUM
	//
	// Display the initial sate of the menu on screen
	usupply.Screen().UpdateMenu<usupply_t>( menu );
	//
	// A periodic update timer, samples ADC and updates measurements
	BasicTimer sampler
	{
		TimerConfig<7>{},
		System::SampleFrequency,
		[]() noexcept
		{
			usupply.Measure().Update();
			FsDivider.Run([]
			{
				usupply.Screen().UpdateMeasurements( usupply.Setpoint(), usupply.Measure() );
				//
				// Menu runs because some information on it may update with the corresponding measurements
				usupply.Screen().UpdateMenu<usupply_t>(menu);
			});
		}
	};
	//
	// Timer isn't started until here ( not automatic like the peripheral's registers )
	sampler.Start();
	
	// Busy loop, device should be asleep most of the time here
	while(true)
	{
		numpad.Update();
		usupply.Screen().Update();
		//
		// Handle scheduled actions
		//	These are used to flatten stack
		test_schedulable_t::Run();
		save_schedulable_t::Run();
	}
#else
    while(true);
#endif
	//
 	// Stack or program counter corrupted
	// (╯°-°）╯︵ ┻━┻
	return 0;
}