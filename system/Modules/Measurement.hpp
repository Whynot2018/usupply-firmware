#pragma once

#include "ADC.hpp"
#include "RTC.hpp"
#include "Energy.hpp"
#include "SettingsSingleton.hpp"
#include "KalmanFilter.hpp"

namespace System
{
	class Measurement
	{
	private:
		using ADC_t 	= Peripherals::ADCModule<ADC_IN0, ADC_IN1, V_BUCK_MON_5, _15V0_MON_5, TEMP_MEAS>;
		using energy_t	= General::Energy<float>;

		class ADCSample
		{
		private:
			struct RawSample
			{
				std::uint16_t m_VOutput;
				std::uint16_t m_IOutput;
				std::uint16_t m_VIsolated;
				std::uint16_t m_VPreregulator;
				std::uint16_t m_Temperature;

				auto & operator=( RawSample const & pInput ) noexcept
				{
					m_VOutput 		= pInput.m_VOutput;
					m_IOutput 		= pInput.m_IOutput;
					m_VPreregulator = pInput.m_VPreregulator;
					m_VIsolated 	= pInput.m_VIsolated;
					m_Temperature 	= pInput.m_Temperature;
					return *this;
				}
			};

			RawSample m_State{0u, 0u, 0u, 0u, 0u};

			//Used to verify values
			static auto constexpr OutputVoltageScale() noexcept
			{
				auto constexpr scale =
						( (( (float)R8 ) + ( (float)R23 ) ) ) /
						   ( (float)R23 );
				return scale * ADC_t::Scale;
			}
			static auto constexpr OutputCurrentScale() noexcept
			{
				auto constexpr scale =	(float)R17 /
										(float)R32 / RSHUNT;
				return scale * ADC_t::Scale;
			}
			static auto constexpr PreregulatorScale() noexcept
			{
				auto constexpr scale =
						( (( (float)R24) + (float)R34 ) ) /
						   ( (float)R34 );
				return scale * ADC_t::Scale;
			}
			static auto constexpr IsolatedScale() noexcept
			{
				auto constexpr scale =
						( ( (float)R25) + (float)R35 ) /
						  ( (float)R35 );
				return scale * ADC_t::Scale;
			}
		public:
			ADCSample() = default;

			auto & operator = ( ADC_t const & pInput ) noexcept
			{
				m_State = RawSample
				{
					pInput.Get<ADC_IN0>(),
					pInput.Get<ADC_IN1>(),
					pInput.Get<V_BUCK_MON_5>(),
					pInput.Get<_15V0_MON_5>(), 
					pInput.Get<TEMP_MEAS>()
				};
				return *this;
			}

			float Voltage() const noexcept
			{
				float value{ (float)m_State.m_VOutput };
				return value * OutputVoltageScale();
			}

			float Current() const noexcept
			{
				float value{ (float)m_State.m_IOutput };
				return value * OutputCurrentScale();
			}
			float Power() const noexcept
			{
				return Voltage() * Current();
			}
			float Preregulator() const noexcept
			{
				float value{ (float)m_State.m_VPreregulator };
				return value * PreregulatorScale();
			}
			float Isolated() const noexcept
			{
				float value{ (float)m_State.m_VIsolated };
				return value * IsolatedScale();
			}
			//Temperature scale cannot be constexpr as it depends on device specific constants
			float Temperature() const noexcept
			{
				//Factory calibration temperatures
				auto constexpr low_cal_value 	= 30.0f;
				auto constexpr high_cal_value	= 110.0f;

				//Internal stored calibration values
				auto const 	temp110_cal			= (float)TEMP110_CAL;
				auto const 	temp30_cal 			= (float)TEMP30_CAL;

				//Scale calculation (degree C per lsb)
				auto const 	scale 				= (high_cal_value - low_cal_value) / (temp110_cal - temp30_cal);

				//Register value
				float const value				= (float) m_State.m_Temperature;

				//Measured temperature
				return (value - temp30_cal) * scale + low_cal_value;
			}
		};
		//
		ADC_t 		m_ADC;
		ADCSample	m_State;
		float 		m_CalculatedPower;
		energy_t	m_Energy;
		//
		General::KalmanFilter<float>
					m_VoltageFilter{ 1, 	0.5, 	0.01 },
					m_CurrentFilter{ 0.1,	0.5, 	0.01 };
	public:
		Measurement() noexcept: m_ADC
			{
				[&]
				{
					m_State = m_ADC;
					m_CalculatedPower = m_State.Power();
					m_Energy.Sample( m_State.Power() );
				}
			}
		{
			//Updates the energy calculation
			static constexpr unsigned RTC_TIME = 1;

			//Updates once a second, this represents X samples when sampled at Y Hz
            using ::Peripherals::RCCGeneral::RTCClockSource;
			static auto rtc = Peripherals::RTCModule(RTC_TIME, RTCClockSource::LSE, [&]
			{
				m_Energy.Update(RTC_TIME);
			});
		}

		void ResetTime() noexcept
		{
			Peripherals::RTCGeneral::ResetTimeAndDate();
		}
		void PauseTime() noexcept
		{
			Peripherals::RTCGeneral::Pause();
		}
		void ResumeTime() noexcept
		{
			Peripherals::RTCGeneral::Resume();
		}
		void ResetEnergy() noexcept
		{
			m_Energy.Reset();
		}
		void ResetTimeAndEnergy() noexcept
		{
			ResetTime();
			ResetEnergy();
		}

		//Measurement functions, used by SCPI and Screen and Menu
		float Voltage() noexcept
		{
			if (Singleton::Settings().VoltageFilter())
				return m_VoltageFilter.Update(m_State.Voltage());
			else
				return m_State.Voltage();
		}
		float Current() noexcept
		{
			if (Singleton::Settings().CurrentFilter())
				return m_CurrentFilter.Update(m_State.Current());
			else
				return m_State.Current();
		}
		float Isolated() const noexcept
		{
			return m_State.Isolated();
		}
		float Preregulator() const noexcept
		{
			return m_State.Preregulator();
		}

		float Temperature() const noexcept
		{
			return m_State.Temperature();
		}
		//
		void ResetFilters()
		{
			m_VoltageFilter.Reset(Voltage());
			m_CurrentFilter.Reset(Current());
		}

		float Power() noexcept
		{
			return m_CalculatedPower;
		}
		float Energy() noexcept
		{
			return m_Energy.Joules();
		}
		float WattHours() noexcept
		{
			return m_Energy.WattHours();
		}
		void Update() noexcept
		{
			m_ADC.Trigger();
		}

		//Returns a tuple v, i, pre, iso, temp, enables structured bindings
		std::tuple<float, float, float, float, float> Binding () noexcept
		{
			return { Voltage(), Current(), Preregulator(), Isolated(), Temperature() };
		}
	};
}