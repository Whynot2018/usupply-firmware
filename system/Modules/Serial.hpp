#pragma once
#include "Types.hpp"
#include "USART.hpp"
#include "SCPI.hpp"
#include "LockState.hpp"

namespace System
{
	using CommandCompletion = Peripherals::CommandCompletion;
    //
	class USARTKernal
	{
	private:
		UART_RX m_Rx;
		UART_TX m_Tx;
	public:
		using RXPin = UART_RX;
		using TXPin = UART_TX;
		//
		static constexpr std::uint32_t 								Channel 	= 1u;
		static constexpr std::uint32_t 								Baudrate 	= 19200u;
		static constexpr Peripherals::USARTGeneral::Databits 		Databits 	= Peripherals::USARTGeneral::Databits::Bits8;
		static constexpr Peripherals::USARTGeneral::Stopbits 		Stopbits 	= Peripherals::USARTGeneral::Stopbits::One;
		static constexpr Peripherals::USARTGeneral::Parity 			Parity 		= Peripherals::USARTGeneral::Parity::None;
		static constexpr Peripherals::USARTGeneral::FlowControl 	FlowControl = Peripherals::USARTGeneral::FlowControl::None;
		static constexpr std::uint32_t								BufferSize	= 128;
		static constexpr bool 										Echo		= true;
		static constexpr bool 										UseDMA		= false;
		//
		USARTKernal() noexcept:
			m_Rx{ IO::Mode::Alternate, UART_RX::AltFunctions::USART1_RX },
			m_Tx{ IO::Mode::Alternate, UART_TX::AltFunctions::USART1_TX }
		{}
		//
		static CommandCompletion CommandComplete( char input ) noexcept
		{
			switch(input)
			{
			case '\r':
			case '\n':
			case ';':
				return CommandCompletion::Submit;
			case '!':
				return CommandCompletion::Silent;
			default:
				return CommandCompletion::No;
			}
		}
        //
		static bool IsBackspace( char input ) noexcept
		{
			return (General::IsAnyOf(input, (char)127, '\b'));
		}
		//
		template <std::size_t N>
		static bool ShouldPush( char value, Containers::BlindFIFO<char, N> & buffer ) noexcept
		{
			if (value == 27) buffer.Clear();
			//
			buffer.Emplace( value );
			switch( buffer.Count() )
			{
				case 1: 	return !(value == (char)27);
				case 2: 	return !(value == (char)91);
				case 3: 	return !General::IsAnyOf(value, (char)65, (char)66, (char)67, (char)68);
				default: 	return true;
			}
		}
	};
    //
	template <typename Base, typename ChangeF, typename ErrorF>
	auto Serial( ChangeF && change, ErrorF && error ) noexcept
	{
        static auto const scpi = Parser::SCPI::SCPI<Base>();
        //
		return Peripherals::USARTModule
		{
			General::Type<USARTKernal>{},
			[ c{ std::forward<ChangeF>( change ) }, e{ std::forward<ErrorF>( error ) } ] ( CommandCompletion s ) noexcept
			{
				if ( s == CommandCompletion::Silent and not Base::Settings().CommandMode() )
					return;
				else
				{
					auto result = scpi( Peripherals::USARTSender<USARTKernal>::m_RBuffer.AsArray() );
					//
					if ( s != CommandCompletion::Silent )
					{
						if ( result )
						{
							Base::ComputerLock();
							c();
						}
						else e();
					}
				}
			}
		};
	}
}