#pragma once

#include "DAC.hpp"
#include "Constants.hpp"
#include "ControlMode.hpp"
#include "SettingsSingleton.hpp"

namespace System
{
	class LDO
	{
	private:
		using prereg_t 	= Prereg_t;
		using dac_t 	= Peripherals::DACModule<1u, 2u>;
		using ncv_cc_t	= CC_NCV;

		static auto constexpr OutputVoltageScale()
		{
			return 	(float)( R13 ) /
					(float)( R22 + R13 );
		}
		static auto constexpr OutputCurrentScale()
		{
			return RSHUNT;
		}

		General::OnOff 	m_State = General::Off;
		prereg_t 		m_Preregulator;
		dac_t 			m_DAC;
		ncv_cc_t		m_NCV_CC;
		A_PWR_EN	 	m_Power = IO::Mode::Output;
	public:
		LDO() = default;
		//
		static float Voltage()
		{
			return Singleton::Settings().VoltageSetpoint();
		}
		static float Current()
		{
			return Singleton::Settings().CurrentSetpoint();
		}
		static float Dropout()
		{
			return Singleton::Settings().DropoutSetpoint();
		}
		static float PowerLimit()
		{
			return Singleton::Settings().PowerLimit();
		}
		static float VoltageLimit()
		{
			return Singleton::Settings().VoltageLimit();
		}
		static float CurrentLimit()
		{
			return Singleton::Settings().CurrentLimit();
		}

		bool VoltageLimit(float input)
		{
			Singleton::Settings().VoltageLimit(input);
			return Voltage(Voltage());
		}
		bool CurrentLimit(float input)
		{
			Singleton::Settings().CurrentLimit(input);
			return Current(Current());
		}
		bool Dropout(float input)
		{
			auto output = (input < Singleton::Settings().VoltageLimit());
			if (output)
				Singleton::Settings().DropoutSetpoint(input);
			return output;
		}
		bool PowerLimit(float input) noexcept
		{
			Singleton::Settings().PowerLimit(input);
			return Current(Current());
		}
		bool Voltage( float input, bool save = true) noexcept
		{
			auto clamp		= input > Singleton::Settings().VoltageLimit();
			auto voltage 	= ( clamp ) ? Singleton::Settings().VoltageLimit() : input;
			//
			Singleton::Settings().VoltageSetpoint( voltage, save );
			//
			auto current 	= Current(Current());
			Update();
			return current && !(clamp);
		}
		bool Current(float curr, bool save = true) noexcept
		{
			auto clamp	= curr > Singleton::Settings().CurrentLimit();
			auto current  = ( clamp ) ? Singleton::Settings().CurrentLimit() : curr;
			auto voltage 	= Voltage();
			auto limit 	= Singleton::Settings().PowerLimit();
			auto power 	= voltage * current;
			auto overload = power > limit;
			//
			Singleton::Settings().CurrentSetpoint( ( overload ) ? ( limit / voltage ) : current, save );
			//
			Update();
			return !(overload || clamp);
		}
		//
		//
		bool ConstantVoltage() const noexcept
		{
			return (bool)m_NCV_CC;
		}
		bool ConstantCurrent() const noexcept
		{
			return !ConstantVoltage();
		}
		LCD::Block::Control Mode() const noexcept
		{
			if ( IsOn() )
			{
				return ( ConstantCurrent() ) ?
					LCD::Block::Control::CC :
					LCD::Block::Control::CV;
			}
			else
			{
				return LCD::Block::Control::Off;
			}
		}
		//
		//
		void PowerState(General::OnOff input) noexcept
		{
			m_Power = (bool)input;
			m_State = input;
			Update();
		}
		void On()
		{
			PowerState( General::On );
		}
		void Off()
		{
			PowerState( General::Off );
		}
		General::OnOff const & PowerState() const
		{
			return m_State;
		}
		bool IsOn() const noexcept
		{
			return PowerState();
		}
		void Update()
		{
			if ( PowerState() )
			{
				m_Preregulator = Voltage() + Dropout();
				m_DAC.SetVoltage
				(
					( Singleton::Settings().VoltageCalibration() )(Voltage()) * OutputVoltageScale(),
					( Singleton::Settings().CurrentCalibration() )(Current()) * OutputCurrentScale()
				);
			}
			else
			{
				m_Preregulator = Dropout();
				m_DAC.SetVoltage(0, 0);
			}
		}
	};
}