#pragma once

#include "Constants.hpp"
#include "SupplyState.hpp"
#include "Version.hpp"
#include "Flash.hpp"
#include "AT34C02D.hpp"
#include "Schedulable.hpp"
#include <cstdint>

namespace System
{
	static constexpr uint8_t MagicValue = 0b10100111;

	struct NonVolatileSettings
	{
		uint8_t const				m_Magic;
		General::Version			m_Version;							//2 		=	2	bytes
		Setpoint					m_Setpoints;	//Saved to 0		//12 		=	12	bytes
		Limits						m_Limits;							//12 		=	12	bytes	
		CalibrationData				m_Calibration;						//2*12*2	=	48	bytes
		Flags						m_Flags;							//1			=	1	byte
		std::array<Output, 10>		m_Presets;							//	Saved to 0 - 9	
																		// 12 * 9	=	108	bytes
	};
	using save_schedulable_t = Schedulable<NonVolatileSettings, 16>;

	//
	// This is the actual storage of the configuation and settings
	constexpr NonVolatileSettings ConfigDefault
	{
		MagicValue,
		General::Version{ 0u, 		0u 	}, 
		Setpoint		{ 1.0f, 	0.1f, 		2.0f 	}, 
		Limits			{ 12.0f, 	2.0f, 		10.0f 	},
		{},
		{},
		{
			Output{	0.8, 0.2	},
			Output{	1.8, 0.5	},
			Output{	2.2, 0.5	},
			Output{	3.3, 0.25	},
			Output{	4.1, 0.5	},
			Output{	5.0, 0.25	},
			Output{	6.2, 0.2	},
			Output{	7.1, 0.2	},
			Output{	8.0, 0.2	},
			Output{	9.0, 0.2	}
		}
	};
	
	static_assert( sizeof(NonVolatileSettings) < 256, "Config cannot fit in EEPROM." );

	namespace Singleton
	{
		using EEPROM_Kernal = ::Parts::AT34C02D::Kernal<false, false, false>;
		//
		auto & EEPROM()
		{
			static auto eeprom = ::Parts::AT34C02D::EEPROM<EEPROM_Kernal>([](){},[](){});
			return eeprom;
		}
		bool SaveSettings(NonVolatileSettings const & data)
		{
			save_schedulable_t::Destruct();
			return save_schedulable_t::Construct
			(
				[&]
				{
					//
					//	Block until done
					EEPROM().Write( 0x00, { (uint8_t const *)&data, sizeof( NonVolatileSettings ) } );
					while( EEPROM().Busy() );
				}
			);
		}
		//
		//
		NonVolatileSettings LoadSettings()
		{
			using buffer_t = std::uint8_t[ sizeof(NonVolatileSettings) ];
			//
			// Avoid constructing an expensive type
			union
			{
				uint8_t 			magic;
				buffer_t 			buffer;
				NonVolatileSettings output;
			} combine{ 0 };
			//
			auto r{ EEPROM().Read( 0x00, { (uint8_t *)&combine.buffer[0], sizeof( combine ) } ) };
			while( EEPROM().Busy() );
			//
			if (r)
			{
				//
				// Never loaded into EEPROM
				if ( combine.magic != MagicValue )
				{
					SaveSettings( ConfigDefault );
					return ConfigDefault;
				}
				else
				{
				 	if ( !combine.output.m_Flags.SaveLimits )
				 		combine.output.m_Limits = ConfigDefault.m_Limits;
				 	//
				 	if ( !combine.output.m_Flags.SaveSetpoints )
						combine.output.m_Setpoints = ConfigDefault.m_Setpoints;
					//
					return combine.output;
				}
			}
			return ConfigDefault;
		}
		//
		//
		unsigned ToBit( bool v ) noexcept
		{
			return (v) ? 1u : 0u;
		}
		unsigned ToBit( General::OnOff v ) noexcept
		{
			return (v == General::OnOffValue::On) ? 1u : 0u;
		}
		//
		//
		class SettingsSingleton
		{
		protected:
			//
			// Loads on startup and creates a copy which can be worked off
			NonVolatileSettings m_Copy{ LoadSettings() };
			//
			auto & GetLimits()
			{
				return m_Copy.m_Limits;
			}
			auto & GetSetpoint()
			{
				return m_Copy.m_Setpoints;
			}
			//
			//
			template <typename F>
			static void ChangeSetting( General::OnOff const & new_data, General::OnOff const & old_data, NonVolatileSettings const & data, F && function ) noexcept
			{
				if ( (bool)new_data != (bool)old_data )
				{
					std::forward<F>( function )( new_data );
					SaveSettings( data );
				}
			}
			template <typename F>
			static void ChangeSetting( float new_data, float old_data, NonVolatileSettings const & data, F && function, bool save = true ) noexcept
			{
				if ( new_data != old_data )
				{
					std::forward<F>( function )( new_data );
					if (save) SaveSettings( data );
				}
			}
			//
			//
			struct BuzzerState
			{
				NonVolatileSettings & Data;
				//
				BuzzerState & operator = (General::OnOff state) noexcept
				{
					ChangeSetting
					(
						state,
						General::OnOff{!!Data.m_Flags.BuzzerEnabled},
						Data,
						[&](auto v)
						{
							Data.m_Flags.BuzzerEnabled = ToBit(v);
						}
					);
					return *this;
				}
				General::OnOff State() const noexcept
				{
					return (General::OnOff)Data.m_Flags.BuzzerEnabled;
				}
				operator General::OnOff() const noexcept
				{
					return State();
				}
				operator bool () const noexcept
				{
					return (bool)Data.m_Flags.BuzzerEnabled;
				}
			};
			struct ResetEnergyOnPowerState
			{
				NonVolatileSettings & Data;
				ResetEnergyOnPowerState(NonVolatileSettings & data) noexcept : Data{ data } {}
				//
				auto & operator = (General::OnOff state) noexcept
				{
					ChangeSetting
					(
						state,
						General::OnOff{ General::OneIsTrue(Data.m_Flags.ResetEnergyOnPower) }, 
						Data,
						[&](auto v)
						{
							Data.m_Flags.ResetEnergyOnPower = ToBit(v);
						}
					);
					return *this;
				}
				General::OnOff State() const noexcept
				{
					return (General::OnOff)Data.m_Flags.ResetEnergyOnPower;
				}
				operator General::OnOff() const noexcept
				{
					return State();
				}
				operator bool () const noexcept
				{
					return (bool)Data.m_Flags.ResetEnergyOnPower;
				}
			};
			struct VoltageFilterState
			{
				NonVolatileSettings & Data;
				VoltageFilterState(NonVolatileSettings & data) noexcept : Data{ data } {}
				//
				auto & operator = (General::OnOff state) noexcept
				{
					ChangeSetting
					(
						state,
						General::OnOff{ General::OneIsTrue( Data.m_Flags.VoltageFilter ) }, 
						Data,
						[&](auto v)
						{
							Data.m_Flags.VoltageFilter = ToBit(v);
						}
					);
					return *this;
				}
				General::OnOff State() const noexcept
				{
					return (Data.m_Flags.VoltageFilter) ? General::OnOffValue::On : General::OnOffValue::Off;
				}
				operator General::OnOff() const noexcept
				{
					return State();
				}
				operator bool () const noexcept
				{
					return (Data.m_Flags.VoltageFilter) ? General::OnOffValue::On : General::OnOffValue::Off;
				}
			};
			struct CurrentFilterState
			{
				NonVolatileSettings & Data;
				CurrentFilterState(NonVolatileSettings & data) noexcept : Data{ data } {}
				//
				auto & operator = (General::OnOff state) noexcept
				{
					ChangeSetting
					(
						state,
						General::OnOff{!!Data.m_Flags.CurrentFilter}, 
						Data,
						[&](auto v)
						{
							Data.m_Flags.CurrentFilter = ToBit(v);
						}
					);
					return *this;
				}
				General::OnOff State() const noexcept
				{
					return (Data.m_Flags.CurrentFilter) ? General::OnOffValue::On : General::OnOffValue::Off;
				}
				operator General::OnOff() const noexcept
				{
					return State();
				}
				operator bool () const noexcept
				{
					return (Data.m_Flags.CurrentFilter) ? General::OnOffValue::On : General::OnOffValue::Off;
				}
			};
			struct SaveLimitsRef
			{
				NonVolatileSettings & Data;
				//
				auto & operator = (General::OnOff state) noexcept
				{
					ChangeSetting
					(
						state,
						General::OnOff{ General::OneIsTrue( Data.m_Flags.SaveLimits ) }, 
						Data,
						[&](auto v)
						{
							Data.m_Flags.SaveLimits = ToBit(v);
						}
					);
					return *this;
				}
				General::OnOff State() const noexcept
				{
					return (General::OnOff)Data.m_Flags.SaveLimits;
				}
				operator General::OnOff() const noexcept
				{
					return State();
				}
				operator bool () const noexcept
				{
					return (bool)Data.m_Flags.SaveLimits;
				}
			};
			struct CommandModeRef
			{
				NonVolatileSettings & Data;
				//
				CommandModeRef & operator = (General::OnOff state) noexcept
				{
					ChangeSetting
					(
						state,
						General::OnOff{ General::OneIsTrue( Data.m_Flags.CommandMode ) }, 
						Data,
						[&](auto v)
						{
							Data.m_Flags.CommandMode = ToBit(v);
						}
					);
					return *this;
				}
				General::OnOff State() const noexcept
				{
					return (General::OnOff)Data.m_Flags.CommandMode;
				}
				operator General::OnOff() const noexcept
				{
					return State();
				}
				operator bool () const noexcept
				{
					return (bool)Data.m_Flags.CommandMode;
				}
			};
			struct SaveSetpointsRef
			{
				NonVolatileSettings & Data;
				//
				auto & operator = (General::OnOff state) noexcept
				{
					ChangeSetting
					(
						state,
						General::OnOff{ General::OneIsTrue( Data.m_Flags.SaveSetpoints ) }, 
						Data,
						[&](auto v)
						{
							Data.m_Flags.SaveSetpoints = ToBit(v);
						}
					);
					return *this;
				}
				General::OnOff State() const noexcept
				{
					return (General::OnOff)Data.m_Flags.SaveSetpoints;
				}
				operator General::OnOff() const noexcept
				{
					return State();
				}
				operator bool () const noexcept
				{
					return (bool)Data.m_Flags.SaveSetpoints;
				}
			};
			//
			//
			template <bool IsVoltage>
			struct CalibrationRef
			{
				NonVolatileSettings & Data;
				CalibrationRef( NonVolatileSettings & data ) noexcept : Data{ data } {}
				//
				//
				Calibration_t & Ref() noexcept 
				{
					if constexpr (IsVoltage)
						return Data.m_Calibration.Voltage.m_Data;
					//
					if constexpr (!IsVoltage)
						return Data.m_Calibration.Current.m_Data;
				}
				Calibration_t const & Ref() const noexcept 
				{
					if constexpr (IsVoltage)
						return Data.m_Calibration.Voltage.m_Data;
					//
					if constexpr (!IsVoltage)
						return Data.m_Calibration.Current.m_Data;
				}
				//
				//
				bool Clear()
				{
					Ref().Clear();
					return SaveSettings(Data);
				}
				bool Push(General::Sample const & input)
				{
					return Ref().Push( input ) && SaveSettings( Data );
				}
				std::size_t Count() const
				{
					return Ref().Count();
				}
				bool Empty() const
				{
					return Ref().Empty();
				}
				decltype(auto) begin() const noexcept
				{
					return Ref().begin();
				}
				decltype(auto) end() const noexcept
				{
					return Ref().end();
				}
				constexpr float operator()(float value) const
				{
					return Ref()(value);
				}
			};
		public:
			SettingsSingleton() = default;
			//
			// Version cannot be modified
			General::Version Version() const noexcept
			{
				return m_Copy.m_Version;
			}
			//
			// Everything else is mutable
			float VoltageSetpoint() noexcept
			{
				return GetSetpoint().Voltage;
			}
			float CurrentSetpoint() noexcept
			{
				return GetSetpoint().Current;
			}
			float DropoutSetpoint() noexcept
			{
				return GetSetpoint().Dropout;
			}
			float VoltageLimit() noexcept
			{
				return GetLimits().Voltage;
			}
			float CurrentLimit() noexcept
			{
				return GetLimits().Current;
			}
			float PowerLimit() noexcept
			{
				return GetLimits().Power;
			}
			//
			//
			void VoltageSetpoint(float input, bool save = true) noexcept
			{
				ChangeSetting
				(
					input,
					VoltageSetpoint(), 
					m_Copy,
					[&](auto v)
					{
						GetSetpoint().Voltage = v;
					},
					save
				);
			}
			void CurrentSetpoint(float input, bool save = true) noexcept
			{
				ChangeSetting
				(
					input,
					CurrentSetpoint(), 
					m_Copy,
					[&](auto v)
					{
						GetSetpoint().Current = v;
					},
					save
				);
			}
			void DropoutSetpoint(float input) noexcept
			{
				ChangeSetting
				(
					input,
					DropoutSetpoint(),
					m_Copy, 
					[&](auto v)
					{
						GetSetpoint().Dropout = v;
					}
				);
			}
			void VoltageLimit(float input) noexcept
			{
				ChangeSetting
				(
					input,
					VoltageLimit(),
					m_Copy, 
					[&](auto v)
					{
						GetLimits().Voltage = v;
					}
				);
			}
			void CurrentLimit(float input) noexcept
			{
				ChangeSetting
				(
					input,
					CurrentLimit(),
					m_Copy, 
					[&](auto v)
					{
						GetLimits().Current = v;
					}
				);
			}
			void PowerLimit(float input) noexcept
			{
				ChangeSetting
				(
					input,
					PowerLimit(), 
					m_Copy,
					[&](auto v)
					{
						GetLimits().Power = v;
					}
				);
			}
			//
			//
			CalibrationRef<true> VoltageCalibration() noexcept
			{
				return { m_Copy };
			}
			CalibrationRef<false> CurrentCalibration() noexcept
			{
				return { m_Copy };
			}
			//
			//
			Output Preset( std::size_t i ) const noexcept
			{
				if ( i >= m_Copy.m_Presets.size() ) return {};
				return m_Copy.m_Presets[i];
			}
			void Preset( Output setpoint, std::size_t i ) noexcept
			{
				if ( i >= m_Copy.m_Presets.size() ) return;
				m_Copy.m_Presets[i] = setpoint;
				SaveSettings( m_Copy );
			}
			//
			// Bitfields
			ResetEnergyOnPowerState ResetEnergyOnPower() noexcept
			{
				return { m_Copy };
			}
			BuzzerState Buzzer() noexcept
			{
				return { m_Copy };
			}
			SaveLimitsRef SaveLimits() noexcept
			{
				return { m_Copy };
			}
			CommandModeRef CommandMode() noexcept
			{
				return { m_Copy };
			}
			SaveSetpointsRef SaveSetpoints() noexcept
			{
				return { m_Copy };
			}
			VoltageFilterState VoltageFilter() noexcept
			{
				return { m_Copy };
			}
			CurrentFilterState CurrentFilter() noexcept
			{
				return { m_Copy };
			}
		};
		//
		//
		SettingsSingleton & Settings() 
		{
			static SettingsSingleton settings;
			return settings;
		}
	}
}