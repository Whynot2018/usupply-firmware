#pragma once

#include "Constants.hpp"
#include <cstdint>

namespace Electronics::System
{
	template <typename value_t>
	class Feedback
	{
	protected:
		std::uint16_t const m_OutputVoltage;		//V_SENSE
		std::uint16_t const m_OutputCurrent;		//I_SENSE
		std::uint16_t const m_PreregMonitor;		//MON_V_BUCK_5
		std::uint16_t const m_IsolationMonitor;		//MON_15V0_5
		
		static value_t const m_Fullscale;
		
		static auto const ToVoltage(value_t const pInput)
		{
			value_t const Scale = (AnalogReference / m_Fullscale);
			return pInput * Scale;
		}
	public:
		Feedback() : 
			m_OutputVoltage		(0u), 
			m_OutputCurrent		(0u), 
			m_PreregMonitor		(0u), 
			m_IsolationMonitor	(0u) 
		{}
		Feedback( Feedback<value_t> const & pInput ) : 
			m_OutputVoltage		(pInput.m_OutputVoltage), 
			m_OutputCurrent		(pInput.m_OutputCurrent), 
			m_PreregMonitor		(pInput.m_PreregMonitor), 
			m_IsolationMonitor	(pInput.m_IsolationMonitor) 
		{}
		
		value_t const OutputVoltage() const
		{
			return ToVoltage( m_OutputVoltage );
		}
		value_t const OutputCurrent() const
		{
			return ToVoltage( m_OutputCurrent );
		}
		value_t const PreregulatorVoltage() const
		{
			return 5.0 * ToVoltage( m_PreregMonitor );
		}
		value_t const IsolationVoltage() const
		{
			return 5.0 * ToVoltage( m_IsolationMonitor );
		}
	};
	
	template <typename value_t>
	const value_t Feedback<value_t>::m_Fullscale = 4096.0;
}