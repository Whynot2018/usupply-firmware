#pragma once

#include "Boolean.hpp"
#include "Calibration.hpp"

namespace System
{
	using Calibration_t = General::Calibration<6u>;
	//
	//
	struct SingleCalibration
	{
		Calibration_t m_Data;
	};
	struct Setpoint
	{
		float Voltage, Current, Dropout;
	};
	struct Output
	{
		float Voltage, Current;
	};
	struct Limits
	{
		float Voltage, Current, Power;
	};
	struct CalibrationData
	{
		SingleCalibration Voltage, Current;
	};
	//
	//
	struct Flags 
	{
		uint8_t SaveSetpoints 		: 1u;
		uint8_t SaveLimits			: 1u;
		uint8_t BuzzerEnabled		: 1u;
		uint8_t ResetEnergyOnPower 	: 1u;
		uint8_t VoltageFilter		: 1u;
		uint8_t CurrentFilter		: 1u;
		uint8_t CommandMode			: 1u;
		//
		//
		constexpr Flags() : 
			SaveSetpoints		{1},
			SaveLimits			{1},
			BuzzerEnabled		{1},
			ResetEnergyOnPower	{1},
			VoltageFilter		{0},
			CurrentFilter		{0},
			CommandMode			{1}
		{}
	};
	static_assert(sizeof(Flags) == 1, "Flags should only occupy a single byte.");
}