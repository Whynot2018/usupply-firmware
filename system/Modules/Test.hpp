#pragma once

#include "Benchmark.hpp"
#include "Constants.hpp"
#include "Math.hpp"
#include "ArrayConvert.hpp"

namespace System
{
	class TestState
	{
	public:
		//
		enum S : std::size_t
		{
			CurrentLow,
			VoltageLow,
			VoltageHigh,
			CurrentHigh,
			//
			SourceEnd
			//
		};
		//
		enum L : std::size_t
		{
			LoadOpen,
			Load10R,
			Load100R,
			Load100nF,
			Load100uF,
			Load100nH,
			Load100uH,
			//
			LoadEnd
			//
		};
		//
		enum T : std::size_t
		{
			VoltagePrereg,
			VoltageIsolator,
			VoltageOutput,
			CurrentOutput,
			//
			TestEnd
			//
		};
	private:
		//
		class iterator
		{
			using S_t = std::underlying_type_t<S>;
			using T_t = std::underlying_type_t<T>;
			using L_t = std::underlying_type_t<L>;
			//
			union 
			{
				S m_Source = (S)0;
				S_t m_SourceIndex;
			};
			union
			{
				T m_Test = (T)0;
				T_t	m_TestIndex;
			};
			union
			{
				L m_Load = (L)0;
				L_t m_LoadIndex;
			};
			//
			constexpr void ResetSource() noexcept
			{
				m_Source = (S)0;
			}
			constexpr void ResetTest() noexcept
			{
				m_Test = (T)0;
			}
			constexpr void ResetLoad() noexcept
			{
				m_Load = (L)0;
			}
			constexpr bool IncSource() noexcept
			{
				++m_SourceIndex;
				return m_Source != SourceEnd;
			}
			constexpr bool IncTest() noexcept
			{
				++m_TestIndex;
				return m_Test != TestEnd;
			}
			constexpr bool IncLoad() noexcept
			{
				++m_LoadIndex;
				return m_Load != LoadEnd;
			}
		public:
			using  self_type 			= iterator;
			using  value_type 			= self_type;
			using  reference 			= value_type &;
			using  pointer 				= value_type *;
			using  iterator_category 	= std::forward_iterator_tag;
			using  difference_type 		= long;
			//
			constexpr iterator( ) noexcept = default;
			//
			constexpr iterator( iterator && ) 					noexcept = default;
			constexpr iterator( iterator const & ) 				noexcept = default;
			//
			constexpr reference operator = ( iterator && ) 		noexcept = default;
			constexpr reference operator = ( iterator const & ) noexcept = default;
			//
			constexpr iterator( S source, T test, L load ) noexcept :
				m_Source{ source },
				m_Test{ test },
				m_Load{ load }
			{}
			//
			constexpr self_type operator++() noexcept
			{
				if ( !IncSource() )
				{
					if ( IncTest() )
						ResetSource();
					else
					{
						if (IncLoad())
						{
							ResetSource();
							ResetTest();
						}
					}
				}
				return *this;
			}
			constexpr self_type operator++(int) noexcept
			{
				auto output = *this;
				++(*this);
				return output;
			}
			//
			constexpr reference operator*() noexcept
			{
				return *this;
			}
			//
			constexpr pointer operator->() noexcept
			{
				return this;
			}
			//
			constexpr bool operator==(self_type const & rhs) const noexcept
			{
				return (m_Source == rhs.m_Source) && (m_Test == rhs.m_Test) && (m_Load == rhs.m_Load);
			}
			//
			constexpr bool operator!=(self_type const & rhs) const noexcept
			{
				return (m_Source != rhs.m_Source) || (m_Test != rhs.m_Test) || (m_Load != rhs.m_Load);
			}
			//
			S Source() const noexcept
			{
				return m_Source;
			}
			T Test() const noexcept
			{
				return m_Test;
			}
			L Load() const noexcept
			{
				return m_Load;
			}
			//
		};
	public:
		//
		iterator begin() const
		{
			return {};
		}
		//
		iterator end() const
		{
			return {SourceEnd, TestEnd, LoadEnd};
		}
		//
	};

	template <typename Base>
	void Test(float lowV, float highV, float lowI, float highI, float powerlimit)
	{
		float stage_wait = 0.2;
		//
		// Change dropout to be reletive to ground and not output voltage
		auto preregulator = [&] (auto v) { return v + Base::Setpoint().Dropout(); };
		//
		Base::Setpoint().PowerLimit(powerlimit);
		//
		Base::Serial().Send("Self test started, don't unplug device\n");

		auto const state = TestState{};
		float expectedv = 0;
		float expectedi = 0;
		for (auto current : state)
		{
			Base::Wait( stage_wait );
			//
			// Enable the setpoint
			auto is_set = [&] () -> bool
			{
				//
				// Setup the source
				switch( current.Source() )
				{
				case TestState::CurrentLow:	 		Base::Serial().Send(lowI, 	"A, "); return Base::Setpoint().Current( expectedi = lowI );
				case TestState::CurrentHigh: 		Base::Serial().Send(highI, 	"A, "); return Base::Setpoint().Current( expectedi = highI );
				case TestState::VoltageLow:	 		Base::Serial().Send(lowV, 	"V, "); return Base::Setpoint().Voltage( expectedv = lowV );
				case TestState::VoltageHigh: 		Base::Serial().Send(highV, 	"V, "); return Base::Setpoint().Voltage( expectedv = highV );
				default: 					 		Base::Serial().Send("S Error"); 	return false;
				};
			}();
			Base::Wait( stage_wait );
			//Only process if the test is within the power limit
			if ( is_set )
			{
				//
				// Enable the setpoint
				Base::Setpoint().On();
				Base::Setpoint().Update();
				Base::Screen().Update();
				//
				// Wait for source to settle
				Base::Wait( stage_wait );
				//
				// Prompt user for load
				switch( current.Load() )
				{
				case TestState::LoadOpen:			Base::Serial().Send("Open, ");  	break;
				case TestState::Load10R:			Base::Serial().Send("10R, ");  		break;
				case TestState::Load100R:			Base::Serial().Send("100R, ");  	break;
				case TestState::Load100nF:			Base::Serial().Send("100nF, "); 	break;
				case TestState::Load100uF:			Base::Serial().Send("100uF, "); 	break;
				case TestState::Load100nH:			Base::Serial().Send("100nH, "); 	break;
				case TestState::Load100uH:			Base::Serial().Send("100uH, "); 	break;
				default: 							Base::Serial().Send("L Error"); 	return;
				};
				//
				// Wait for source to settle
				// Measure Outputs etc...
				Base::Wait( stage_wait );
				Base::Measure().Update();
				Base::Wait( stage_wait );
				auto [v, i, pre, iso, temp] = Base::Measure().Binding();
				//
				// Perform required test
				// Check specifications
				auto pass_fail = [&](auto value, auto expected, auto tol)
				{
					Base::Serial().Send("[", value, ", ", expected, "],");
					Base::Serial().Send("[", v, ", ", i, ", ", pre, ", ", iso, "] -> ");
					Base::Serial().Send( General::MakeArray( (General::WithinTolerance(value, expected, tol))? "pass\n" : "fail\n" ) );
				};
				switch( current.Test() )
				{
				case TestState::VoltagePrereg:
					Base::Serial().Send("Prereg : ");
					pass_fail(pre, preregulator(expectedv), PreregulatorTolerance);
					break;
				case TestState::VoltageIsolator:
					Base::Serial().Send("Isolator : ");
					pass_fail(iso, IsolatedVoltage, IsolatedTolerance);
					break;
				case TestState::VoltageOutput:
					Base::Serial().Send("Output V : ");
					pass_fail(v, expectedv, OutputVoltageTolerance);
					break;
				case TestState::CurrentOutput:
					Base::Serial().Send("Output I : ");
					pass_fail(i, expectedi, OutputCurrentTolerance);
					break;
				default:
					Base::Serial().Send("T Error");
					return;
				};
				//
				// Disable the setpoint
				Base::Setpoint().Off();
				Base::Setpoint().Update();
				Base::Screen().Update();
			}
			else
			{
				Base::Serial().Send("Test exceeds power limit\n");
			}
		}
		Base::Setpoint().Off();
	}
}