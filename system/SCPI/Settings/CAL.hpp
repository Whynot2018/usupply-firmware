#pragma once

#include "SCPIShared.hpp"
#include "List.hpp"

namespace Parser::SCPI
{
	template <typename Base>
	auto CALIbration() noexcept
	{
		using namespace Helpers;
		return Node
		(
			Keyword(":CALI", "bration"),
			States
			(
				Node
				(
					voltage,
					States
					(
						Command
						(
							rst,
							Blank<0>{},
							None{},
							[&](auto)
							{
								Base::ResetVoltageCalibration();
							}
						),
						Command
						(
							filter,
							Param<General::OnOff>{},
							Blank<0>{},
							[&](auto v)
							{
								if constexpr (IsParam(v))
								{
									//Set state of filter
									Base::Measure().ResetFilters();
									Base::Settings().VoltageFilter() = v;
								}
								else
								{
									//Return state of filter
									Base::Serial().Send( Base::Settings().VoltageFilter().State(), "\n" );
								}
							}
						),
						Command
						(
							Fallback{},
							List
							(
								Param<float>{}, 
								Param<float>{}
							),
							Blank<0>{},
							[&](auto v)
							{
								
								if constexpr (IsParams(v))
								{
									auto const[v1, v2] = v;
									Base::Serial().Send( v1, ", ", v2, " added\n" );
									Base::AddVoltageCalibrationPoint(General::Sample(v1, v2));
								}
								else
								{
									if (Base::Settings().VoltageCalibration().Empty())
										Base::Serial().Send( "None\n" );
									else
									{
										Base::Serial().Send( "Expected, Measured\n" );
										for (auto point : Base::Settings().VoltageCalibration())
											Base::Serial().Send( point, "\n" );
									}
								}
							}
						)
					)
				),
				Node
				(
					current,
					States
					(
						Command
						(
							rst,
							Blank<0>{},
							None{},
							[&](auto)
							{
								Base::ResetCurrentCalibration();
							}
						),
						Command
						(
							filter,
							Param<General::OnOff>{},
							Blank<0>{},
							[&](auto v)
							{
								if constexpr (IsParam(v))
								{
									//Set state of filter
									Base::Measure().ResetFilters();
									Base::Settings().CurrentFilter() = v;
								}
								else
								{
									//Return state of filter
									Base::Serial().Send( Base::Settings().CurrentFilter().State() );
								}
							}
						),
						Command
						(
							Fallback{},
							List
							(
								Param<float>{}, 
								Param<float>{}
							),
							Blank<0>{},
							[&](auto v)
							{
								if constexpr (IsParams(v))
								{
									auto const[v1, v2] = v;
									Base::Serial().Send( v1, ", ", v2, " added\n" );
									Base::AddCurrentCalibrationPoint(General::Sample(v1, v2));
								}
								else
								{
									if (Base::Settings().CurrentCalibration().Empty())
										Base::Serial().Send( "None\n" );
									else
									{
										Base::Serial().Send( "Expected, Measured\n" );
										for (auto point : Base::Settings().CurrentCalibration())
											Base::Serial().Send( point, "\n" );
									}
								}
							}
						)
					)
				)
			)
		);
	}
}