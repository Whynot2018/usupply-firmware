#pragma once

#include "SCPIShared.hpp"

namespace Parser::SCPI
{
	template <typename Base>
	auto FIRMware() noexcept
    {
        return Node
        (
            Keyword(":FIRM","ware"),
            States
            (
                Command
                (
                    Keyword(":BUIL","d"),
                    None{},
                    Blank<0>{},
                    [&](auto){ Base::Serial().Send( "Date = ", __DATE__, ", Time = ", __TIME__, "\n" ); }
                ),
                Command
                (
                    Keyword(":BOOT","loader"),
                    Blank<0>{},
                    None{},
                    [&](auto){ Base::Bootloader(); }
                )
            )
        );
    }
}