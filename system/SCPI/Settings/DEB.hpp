#pragma once

#include "SCPIShared.hpp"

namespace Parser::SCPI
{
	template <typename Base>
	auto DEBug() noexcept
    {
        return Node
        (
            Chain(Keyword(":DEB","ug"), Keyword(":CLOC","k")),
            States
            (
                Command
                (
                    Required(":TICK"),
                    None{},
                    Blank<0>{},
                    [&](auto){ /*Base::Serial().Send( Base::Debug().Stopwatch().Tick() );*/ }
                ),
                Command
                (
                    Keyword(":DURA", "tion"),
                    None{},
                    Blank<0>{},
                    [&](auto){ /*Base::Serial().Send( Base::Debug().Stopwatch().Duration() );*/ }
                ),
                Command
                (
                    Keyword(":STAR", "t"),
                    Blank<0>{},
                    None{},
                    [&](auto){ /*Base::Debug().Stopwatch().Start();*/ }
                ),
                Command
                (
                    Required(":STOP"),
                    Blank<0>{},
                    None{},
                    [&](auto){ /*Base::Debug().Stopwatch().Stop();*/ }
                )
            )
        );
    }
}