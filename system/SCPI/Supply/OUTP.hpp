#pragma once

#include "SCPIShared.hpp"

namespace Parser::SCPI
{
	template <typename Base>
	auto OUTPut() noexcept
    {
        return Node
        (
            Keyword("OUTP", "ut"),
            States
            (
                Command
                (
                    Keyword(":BUZZ", "er"),
                    Param<General::OnOff>{},
                    Blank<0>{},
                    [&](auto v)
                    {
                        if constexpr (IsParam(v))	Base::Screen().BuzzerState(v);
                        else						Base::Serial().Send(Base::Screen().BuzzerState().State(), "\n");
                    }
                ),
                Command
                (
                    Keyword(":STAT", "e"),
                    Param<General::OnOff>{},
                    Blank<0>{},
                    [&](auto v)
                    {
                        if constexpr (IsParam(v))	Base::Setpoint().PowerState(v);
                        else						Base::Serial().Send(Base::Setpoint().PowerState(), "\n");
                    }
                )
            )
        );
    }
}