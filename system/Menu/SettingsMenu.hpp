#pragma once

#include "RawEditable.hpp"
#include "CalibrationMenu.hpp"
#include "CallbackInput.hpp"

#define FWD(x) std::forward<decltype(x)>(x)
namespace System
{
	template 
	<
		unsigned BufferSize, typename T, 
		typename BeepCallback,
		typename AutoResetEnergyCallback,
		typename SaveSetpointCallback,
		typename SaveLimitsCallback,
		typename FactoryResetCallback,
		typename CalibrationResetCallback,
		typename VCalCallback,
		typename ICalCallback
	>
	decltype(auto) SettingsMenu
	(
		General::SizeT<BufferSize>		s,
		General::Type<T>				t,
	    BeepCallback&&					pBeep,
	    AutoResetEnergyCallback &&		pAutoResetEnergy,
		SaveSetpointCallback && 		pSaveSetpoints,
		SaveLimitsCallback && 			pSaveLimits,
	    FactoryResetCallback &&			pFactoryReset,
	    CalibrationResetCallback &&		pCalibrationReset,
	    VCalCallback &&					pAddVoltageCalibration,
	    ICalCallback &&					pAddCurrentCalibration 
	)
	{
		using namespace General;
		using namespace Menu;
		//
		return AutoMenu
		(
			s,
			//////////////////////////////////////////////////////////////////////////////////////////
			//							/**/Format				/**/Labels							/**/Output
			LabelledCallbackInput	(s, General::Type<OnOff>{},	/**/General::MakeArray("bEEp"),		/**/FWD(pBeep)),
			LabelledCallbackInput	(s, General::Type<OnOff>{}, /**/General::MakeArray("rSt.En"),	/**/FWD(pAutoResetEnergy)),
			LabelledCallbackInput 	(s, General::Type<OnOff>{},	/**/General::MakeArray("St.SEt"),	/**/FWD(pSaveSetpoints)),
			LabelledCallbackInput 	(s, General::Type<OnOff>{},	/**/General::MakeArray("St.LIt"),	/**/FWD(pSaveLimits)),
			Label					(s,	/**/					/**/General::MakeArray("F:1.00")	/**/),
			Action					(s, /**/					/**/General::MakeArray("F rSt"),	/**/FWD(pFactoryReset)),
			//////////////////////////////////////////////////////////////////////////////////////////
			CalibrationMenu			(s, FWD(pCalibrationReset), FWD(pAddVoltageCalibration), FWD(pAddCurrentCalibration))
			//////////////////////////////////////////////////////////////////////////////////////////
		);
	}
}
#undef FWD