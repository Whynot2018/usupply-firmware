#pragma once

#include "ArrayConvert.hpp"
#include "Meta.hpp"
#include "MultipurposeUnits.hpp"
#include "SettingsMenu.hpp"
#include "Shortcut.hpp"
#include "Hotkey.hpp"

namespace System
{
#define FWD( x ) std::forward<decltype( x )>( x )
	using LCD::Block::MultipurposeUnit;
	template
	<
		unsigned BufferSize, typename T,

		typename BeepCallback,
		typename AutoResetEnergyCallback,
		typename SaveSetpointCallback,
		typename SaveLimitsCallback,
		typename UnitCallback,
		typename PresetsCallback,
		typename PresetsLoadCallback,
	    typename ResetSysFunction,
	    typename ResetCalFunction,
	    typename VoltageCalFunction,
	    typename CurrentCalFunction
	>
	auto HomeMenu
	( 
		General::SizeT<BufferSize> s, General::Type<T> t,
		BeepCallback&&				pBeep,
		AutoResetEnergyCallback&&	pAutoResetEnergyCallback,
		SaveSetpointCallback&& 		pSaveSetpoints,
		SaveLimitsCallback&& 		pSaveLimits,
		UnitCallback&&				pUnits,
		PresetsCallback&&			pPresetsCallback,
		PresetsLoadCallback&&		pPresetsLoadCallback,
		ResetSysFunction&&			pRSF,
		ResetCalFunction&&			pRCF,
		VoltageCalFunction&&		pVCF,
		CurrentCalFunction&&		pCCF
	)
	{
		using namespace Menu;
		return Shortcut
		(
		    s, Label(s, General::MakeArray("ShiFt")),
		    BasicHotkey
			(
		        Menu::KeyMatchKernal<KeyCodes::Config, true>{},
		        SettingsMenu
				(
		            s, t,
		            FWD( pBeep ),
					FWD( pAutoResetEnergyCallback ),
					FWD( pSaveSetpoints ),
					FWD( pSaveLimits ),
		            FWD( pRSF ),
		            FWD( pRCF ),
		            FWD( pVCF ),
		            FWD( pCCF ) 
				) 
			),
		    BasicHotkey
			(
		        KeyMatchKernal<KeyCodes::Shift, true>{},
		        Shortcut
				(
		            s, Label(s, General::MakeArray("Fn")),
		            ActionHotkey( BlankHotkey( s, KeyMatchKernal<KeyCodes::Num7>{} ),[&](auto){std::forward<UnitCallback>(pUnits)( MultipurposeUnit::W	);}),
		            ActionHotkey( BlankHotkey( s, KeyMatchKernal<KeyCodes::Num9>{} ),[&](auto){std::forward<UnitCallback>(pUnits)( MultipurposeUnit::J	);}),
		            ActionHotkey( BlankHotkey( s, KeyMatchKernal<KeyCodes::Num1>{} ),[&](auto){std::forward<UnitCallback>(pUnits)( MultipurposeUnit::Wh	);}),
		            ActionHotkey( BlankHotkey( s, KeyMatchKernal<KeyCodes::Num3>{} ),[&](auto){std::forward<UnitCallback>(pUnits)( MultipurposeUnit::h	);}) 
				)
			),
		    BasicHotkey
			(
		        KeyMatchKernal<KeyCodes::Presets, true>{},
		        Shortcut
				(
		            s, Label(s, General::MakeArray("LoAd")),
		            ActionHotkey( BlankHotkey( s, NumberMatchKernal{} ),[&](auto v){ std::forward<PresetsCallback>(pPresetsCallback)(v); })
				)
			),
		    BasicHotkey
			(
		        KeyMatchKernal<KeyCodes::PresetsLong, true>{},
		        Shortcut
				(
		            s, Label(s, General::MakeArray("SAvE")),
		            ActionHotkey( BlankHotkey( s, NumberMatchKernal{} ),[&](auto v){ std::forward<PresetsLoadCallback>(pPresetsLoadCallback)(v); })
				) 
			) 
		);
	}
#undef FWD
} // namespace General::Controls
